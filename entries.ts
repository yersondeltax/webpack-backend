
const entriesBasicTransportUnit = { ["createBasicTransportUnit/app"]: './src/handlers/basicTransportUnit/createBasicTransportUnit/app.js',
["enableDisableBasicTransportUnit/app"]: './src/handlers/basicTransportUnit/enableDisableBasicTransportUnit/app.js',
["getAllBasicTransportUnit/app"]: './src/handlers/basicTransportUnit/getAllBasicTransportUnit/app.js',
["getBasicTransportUnitId/app"]: './src/handlers/basicTransportUnit/getBasicTransportUnitId/app.js',
["updateBasicTransportUnit/app"]: './src/handlers/basicTransportUnit/updateBasicTransportUnit/app.js'}

const entriesBenefits = { ["createBenefits/app"]: './src/handlers/benefits/createBenefits/app.js',
["deleteBenefitsById/app"]: './src/handlers/benefits/deleteBenefitsById/app.js',
["enableDisableBenefits/app"]: './src/handlers/benefits/enableDisableBenefits/app.js',
["getAllBenefits/app"]: './src/handlers/benefits/getAllBenefits/app.js',
["getBenefitsId/app"]: './src/handlers/benefits/getBenefitsId/app.js',
["updateBenefits/app"]: './src/handlers/benefits/updateBenefits/app.js'}

const entriesBoardingMode = { ["createBoardingMode/app"]: './src/handlers/boardingMode/createBoardingMode/app.js',
["enableDisableBoardingMode/app"]: './src/handlers/boardingMode/enableDisableBoardingMode/app.js',
["getAllBoardingMode/app"]: './src/handlers/boardingMode/getAllBoardingMode/app.js',
["getBoardingModeById/app"]: './src/handlers/boardingMode/getBoardingModeById/app.js',
["updateBoardingMode/app"]: './src/handlers/boardingMode/updateBoardingMode/app.js'}

const entriesBrand = { ["createBrand/app"]: './src/handlers/brand/createBrand/app.js',
["enableDisableBrand/app"]: './src/handlers/brand/enableDisableBrand/app.js',
["getAllBrand/app"]: './src/handlers/brand/getAllBrand/app.js',
["getBrandById/app"]: './src/handlers/brand/getBrandById/app.js',
["updateBrand/app"]: './src/handlers/brand/updateBrand/app.js',}

const entriesCategory = { ["createCategory/app"]: './src/handlers/category/createCategory/app.js',
["enableDisableCategory/app"]: './src/handlers/category/enableDisableCategory/app.js',
["getAllCategory/app"]: './src/handlers/category/getAllCategory/app.js',
["getCategoryById/app"]: './src/handlers/category/getCategoryById/app.js',
["updateCategory/app"]: './src/handlers/category/updateCategory/app.js'}

const entriesCheckPoints = { ["createCheckPoints/app"]: './src/handlers/checkPoints/createCheckPoints/app.js',
["createCheckPointsNotifyOperator/app"]: './src/handlers/checkPoints/createCheckPointsNotifyOperator/app.js',
["getAllCheckPoints/app"]: './src/handlers/checkPoints/getAllCheckPoints/app.js',
["getAllCheckPointsByLoadingOrder/app"]: './src/handlers/checkPoints/getAllCheckPointsByLoadingOrder/app.js'}

const entriesCities = { ["createCity/app"]: './src/handlers/cities/createCity/app.js',
["enableDisableCity/app"]: './src/handlers/cities/enableDisableCity/app.js',
["getAllCities/app"]: './src/handlers/cities/getAllCities/app.js',
["getCityById/app"]: './src/handlers/cities/getCityById/app.js',
["searchCities/app"]: './src/handlers/cities/searchCities/app.js',
["updateCity/app"]: './src/handlers/cities/updateCity/app.js'}

const entriesClient = { ["getAllClient/app"]: './src/handlers/client/getAllClient/app.js',
["searchPlate/app"]: './src/handlers/client/searchPlate/app.js',
["searshUser/app"]: './src/handlers/client/searshUser/app.js'}

const entriesCompany = { ["createCompany/app"]: './src/handlers/company/createCompany/app.js',
["enableDisableCompany/app"]: './src/handlers/company/enableDisableCompany/app.js',
["getAllCompany/app"]: './src/handlers/company/getAllCompany/app.js',
["getAllCompanyFms/app"]: './src/handlers/company/getAllCompanyFms/app.js',
["getAllModuleByCompany/app"]: './src/handlers/company/getAllModuleByCompany/app.js',
["getCompanyById/app"]: './src/handlers/company/getCompanyById/app.js',
["updateCompany/app"]: './src/handlers/company/updateCompany/app.js'}

const entriesCompanyClient = { ["createCompanyClient/app"]: './src/handlers/companyClient/createCompanyClient/app.js',
["enableDisableCompanyClient/app"]: './src/handlers/companyClient/enableDisableCompanyClient/app.js',
["getAllCompanyClient/app"]: './src/handlers/companyClient/getAllCompanyClient/app.js',
["getAllCompanyClientPrueba/app"]: './src/handlers/companyClient/getAllCompanyClientPrueba/app.js',
["getCompanyClientById/app"]: './src/handlers/companyClient/getCompanyClientById/app.js',
["updateCompanyClient/app"]: './src/handlers/companyClient/updateCompanyClient/app.js'}

const entriesDashboard = { ["dashboard/app"]: './src/handlers/dashboard/dashboard/app.js',
["dashboardTMS/app"]: './src/handlers/dashboard/dashboardTMS/app.js'}

const entriesDispatchType = { ["createDispatchType/app"]: './src/handlers/dispatchType/createDispatchType/app.js',
["enableDisableDispatchType/app"]: './src/handlers/dispatchType/enableDisableDispatchType/app.js',
["getAllDispatchType/app"]: './src/handlers/dispatchType/getAllDispatchType/app.js',
["getdispatchTypeById/app"]: './src/handlers/dispatchType/getdispatchTypeById/app.js',
["updatedispatchType/app"]: './src/handlers/dispatchType/updatedispatchType/app.js'}

const entriesFeaturesTransportUnit = { ["createFeaturesTransportUnit/app"]: './src/handlers/featuresTransportUnit/createFeaturesTransportUnit/app.js',
["enableDisableFeaturesTransportUnit/app"]: './src/handlers/featuresTransportUnit/enableDisableFeaturesTransportUnit/app.js',
["getAllFeaturesTransportUnit/app"]: './src/handlers/featuresTransportUnit/getAllFeaturesTransportUnit/app.js',
["getFeaturesTransportUnitById/app"]: './src/handlers/featuresTransportUnit/getFeaturesTransportUnitById/app.js',
["updateFeaturesTransportUnit/app"]: './src/handlers/featuresTransportUnit/updateFeaturesTransportUnit/app.js'}

const entriesLoadingOrderStatus = { ["createLoadingOrderStatus/app"]: './src/handlers/loadingOrderStatus/createLoadingOrderStatus/app.js',
["enableDisableLoadingOrderStatus/app"]: './src/handlers/loadingOrderStatus/enableDisableLoadingOrderStatus/app.js',
["getAllLoadingOrderStatus/app"]: './src/handlers/loadingOrderStatus/getAllLoadingOrderStatus/app.js',
["getLoadingOrderStatusById/app"]: './src/handlers/loadingOrderStatus/getLoadingOrderStatusById/app.js',
["updateLoadingOrderStatus/app"]: './src/handlers/loadingOrderStatus/updateLoadingOrderStatus/app.js'}

const entriesMeasurementUnit = { ["createMeasurementUnit/app"]: './src/handlers/measurementUnit/createMeasurementUnit/app.js',
["enableDisableMeasurementUnit/app"]: './src/handlers/measurementUnit/enableDisableMeasurementUnit/app.js',
["getAllMeasurementUnit/app"]: './src/handlers/measurementUnit/getAllMeasurementUnit/app.js',
["getMeasurementUnitById/app"]: './src/handlers/measurementUnit/getMeasurementUnitById/app.js',
["updateMeasurementUnit/app"]: './src/handlers/measurementUnit/updateMeasurementUnit/app.js'}

const entriesModules = { ["createModules/app"]: './src/handlers/modules/createModules/app.js',
["enableDisableModules/app"]: './src/handlers/modules/enableDisableModules/app.js',
["getAllModules/app"]: './src/handlers/modules/getAllModules/app.js',
["getModulesById/app"]: './src/handlers/modules/getModulesById/app.js',
["updateModules/app"]: './src/handlers/modules/updateModules/app.js'}

const entriesNews = { ["createNews/app"]: './src/handlers/news/createNews/app.js',
["deleteNewById/app"]: './src/handlers/news/deleteNewById/app.js',
["enableDisableNews/app"]: './src/handlers/news/enableDisableNews/app.js',
["getAllNews/app"]: './src/handlers/news/getAllNews/app.js',
["getNewsId/app"]: './src/handlers/news/getNewsId/app.js',
["updateNews/app"]: './src/handlers/news/updateNews/app.js',
["uploadNewsPthoto/app"]: './src/handlers/news/uploadNewsPthoto/app.js'}

const entriesTravel = { ["assignCarrierByLoadingOrder/app"]: './src/handlers/travel/assignCarrierByLoadingOrder/app.js',
["cancelLoadingOrder/app"]: './src/handlers/travel/cancelLoadingOrder/app.js',
["cancelTravel/app"]: './src/handlers/travel/cancelTravel/app.js',
["changeLoadingOrderPaid/app"]: './src/handlers/travel/changeLoadingOrderPaid/app.js',
["createLoadingOrder/app"]: './src/handlers/travel/createLoadingOrder/app.js',
["createTravel/app"]: './src/handlers/travel/createTravel/app.js',
["createTravelAndRoute/app"]: './src/handlers/travel/createTravelAndRoute/app.js',
["createTravelAndRouteByNameCity/app"]: './src/handlers/travel/createTravelAndRouteByNameCity/app.js',
["createTravelRouteLoadingOrder/app"]: './src/handlers/travel/createTravelRouteLoadingOrder/app.js',
["createTravelRouteLoadingOrderByNameCity/app"]: './src/handlers/travel/createTravelRouteLoadingOrderByNameCity/app.js',
["getAllTravel/app"]: './src/handlers/travel/getAllTravel/app.js',
["getAllTravelByTransportUnit/app"]: './src/handlers/travel/getAllTravelByTransportUnit/app.js',
["getAllTravelToPayPaidByTransportUnit/app"]: './src/handlers/travel/getAllTravelToPayPaidByTransportUnit/app.js',
["getAllTravelWithFeature/app"]: './src/handlers/travel/getAllTravelWithFeature/app.js',
["getAllTravelWithFeatureRoundTrip/app"]: './src/handlers/travel/getAllTravelWithFeatureRoundTrip/app.js',
["getTravelById/app"]: './src/handlers/travel/getTravelById/app.js',
["getTravelByOperation/app"]: './src/handlers/travel/getTravelByOperation/app.js',
["getTravelByOperationFms/app"]: './src/handlers/travel/getTravelByOperationFms/app.js',
["getTravelByTransportUnit/app"]: './src/handlers/travel/getTravelByTransportUnit/app.js',
["getTravelWithFeatureById/app"]: './src/handlers/travel/getTravelWithFeatureById/app.js',
["notifyLoactionLoadingOrder/app"]: './src/handlers/travel/notifyLoactionLoadingOrder/app.js',
["notifyPublish/app"]: './src/handlers/travel/notifyPublish/app.js',
["publishTravel/app"]: './src/handlers/travel/publishTravel/app.js',
["unpublishAutomatic/app"]: './src/handlers/travel/unpublishAutomatic/app.js',
["updateTravel/app"]: './src/handlers/travel/updateTravel/app.js'}

const entriesNotification = { ["createNotification/app"]: './src/handlers/notification/createNotification/app.js',
["getAllNotification/app"]: './src/handlers/notification/getAllNotification/app.js',
["getAllNotificationByUser/app"]: './src/handlers/notification/getAllNotificationByUser/app.js',
["getNotificationId/app"]: './src/handlers/notification/getNotificationId/app.js',
["notifyAutomatic/app"]: './src/handlers/notification/notifyAutomatic/app.js',
["updateNotification/app"]: './src/handlers/notification/updateNotification/app.js'}

const entriesOperation = { ["cancelOperation/app"]: './src/handlers/operation/cancelOperation/app.js',
["createOperation/app"]: './src/handlers/operation/createOperation/app.js',
["getAllOperation/app"]: './src/handlers/operation/getAllOperation/app.js',
["getAllOperationAndCheckpoints/app"]: './src/handlers/operation/getAllOperationAndCheckpoints/app.js',
["getAllOperationFms/app"]: './src/handlers/operation/getAllOperationFms/app.js',
["getAllOperationPrueba/app"]: './src/handlers/operation/getAllOperationPrueba/app.js',
["getOperationById/app"]: './src/handlers/operation/getOperationById/app.js',
["updateOperation/app"]: './src/handlers/operation/updateOperation/app.js'}

const entriesOperationStatus = { ["createOperationStatus/app"]: './src/handlers/operationStatus/createOperationStatus/app.js',
["enableDisableOperationStatus/app"]: './src/handlers/operationStatus/enableDisableOperationStatus/app.js',
["getAllOperationStatus/app"]: './src/handlers/operationStatus/getAllOperationStatus/app.js',
["getOperationStatusById/app"]: './src/handlers/operationStatus/getOperationStatusById/app.js',
["updateOperationStatus/app"]: './src/handlers/operationStatus/updateOperationStatus/app.js'}

const entriesOperator = { ["assignModulesUser/app"]: './src/handlers/Operator/assignModulesUser/app.js',
["confirmarUserBy/app"]: './src/handlers/Operator/confirmarUserBy/app.js',
["enableDisableOperator/app"]: './src/handlers/Operator/enableDisableOperator/app.js',
["getAllOperator/app"]: './src/handlers/Operator/getAllOperator/app.js',
["getOperatorById/app"]: './src/handlers/Operator/getOperatorById/app.js',
["getOperatorByUsersId/app"]: './src/handlers/Operator/getOperatorByUsersId/app.js',
["updateUserOperator/app"]: './src/handlers/Operator/updateUserOperator/app.js',
["uploadFilesProfileUsers/app"]: './src/handlers/Operator/uploadFilesProfileUsers/app.js',
["uploadFilesResourcesUsers/app"]: './src/handlers/Operator/uploadFilesResourcesUsers/app.js'}

const entriesPlace = { ["createPlace/app"]: './src/handlers/place/createPlace/app.js',
["enableDisablePlace/app"]: './src/handlers/place/enableDisablePlace/app.js',
["getAllPlace/app"]: './src/handlers/place/getAllPlace/app.js',
["getCityByPlaceIdStatesId/app"]: './src/handlers/place/getCityByPlaceIdStatesId/app.js',
["getPlaceById/app"]: './src/handlers/place/getPlaceById/app.js',
["updatePlace/app"]: './src/handlers/place/updatePlace/app.js'}

const entriesPostulation = { ["acceptedPostulation/app"]: './src/handlers/postulation/acceptedPostulation/app.js',
["cancelledPostulation/app"]: './src/handlers/postulation/cancelledPostulation/app.js',
["confirmPostulation/app"]: './src/handlers/postulation/confirmPostulation/app.js',
["createPostulation/app"]: './src/handlers/postulation/createPostulation/app.js',
["declinePostulation/app"]: './src/handlers/postulation/declinePostulation/app.js',
["getAllPostulation/app"]: './src/handlers/postulation/getAllPostulation/app.js',
["getPostulationAcceptedByTransportUnit/app"]: './src/handlers/postulation/getPostulationAcceptedByTransportUnit/app.js',
["getPostulationById/app"]: './src/handlers/postulation/getPostulationById/app.js',
["getPostulationByTravelId/app"]: './src/handlers/postulation/getPostulationByTravelId/app.js',
["getPostulationDateConfirmeByTravelId/app"]: './src/handlers/postulation/getPostulationDateConfirmeByTravelId/app.js',
["rejectPostulation/app"]: './src/handlers/postulation/rejectPostulation/app.js'}

const entriesRating = { ["createRating/app"]: './src/handlers/rating/createRating/app.js',
["existRatingByTravel/app"]: './src/handlers/rating/existRatingByTravel/app.js',
["getAllRating/app"]: './src/handlers/rating/getAllRating/app.js',
["getAllRatingsByUser/app"]: './src/handlers/rating/getAllRatingsByUser/app.js',
["getRatingByTravel/app"]: './src/handlers/rating/getRatingByTravel/app.js',
["getRatingId/app"]: './src/handlers/rating/getRatingId/app.js'}

const entriesReport = { ["reportByOperation/app"]: './src/handlers/report/reportByOperation/app.js'}

const entriesRoute = { ["createRoute/app"]: './src/handlers/route/createRoute/app.js',
["deleteRouteById/app"]: './src/handlers/route/deleteRouteById/app.js',
["enableDisableRoute/app"]: './src/handlers/route/enableDisableRoute/app.js',
["getAllRoute/app"]: './src/handlers/route/getAllRoute/app.js',
["getRouteById/app"]: './src/handlers/route/getRouteById/app.js',
["UpdateRoute/app"]: './src/handlers/route/UpdateRoute/app.js'}

const entriesSetting = { ["createSetting/app"]: './src/handlers/setting/createSetting/app.js',
["getAllSetting/app"]: './src/handlers/setting/getAllSetting/app.js',
["pruebaPush/app"]: './src/handlers/setting/pruebaPush/app.js'}

const entriesStages = { ["copiTaskAndStages/app"]: './src/handlers/stages/copiTaskAndStages/app.js',
["createStages/app"]: './src/handlers/stages/createStages/app.js',
["getTaskById/app"]: './src/handlers/stages/getTaskById/app.js',
["performTasks/app"]: './src/handlers/stages/performTasks/app.js',
["performTasksFile/app"]: './src/handlers/stages/performTasksFile/app.js',
["rejectTasks/app"]: './src/handlers/stages/rejectTasks/app.js',
["validateTasks/app"]: './src/handlers/stages/validateTasks/app.js'}

const entriesStagesTemplate = { ["createArrayStagesTemplate/app"]: './src/handlers/stagesTemplate/createArrayStagesTemplate/app.js',
["createStagesTemplate/app"]: './src/handlers/stagesTemplate/createStagesTemplate/app.js',
["createStagesTemplateAutomatic/app"]: './src/handlers/stagesTemplate/createStagesTemplateAutomatic/app.js',
["deleteTaskByStagesTemplate/app"]: './src/handlers/stagesTemplate/deleteTaskByStagesTemplate/app.js',
["enableDisableStagesTemplate/app"]: './src/handlers/stagesTemplate/enableDisableStagesTemplate/app.js',
["getAllStagesTemplate/app"]: './src/handlers/stagesTemplate/getAllStagesTemplate/app.js',
["getAllStagesTemplateGroupConfig/app"]: './src/handlers/stagesTemplate/getAllStagesTemplateGroupConfig/app.js',
["getStagesTemplateByConfig/app"]: './src/handlers/stagesTemplate/getStagesTemplateByConfig/app.js',
["getStagesTemplateById/app"]: './src/handlers/stagesTemplate/getStagesTemplateById/app.js',
["updateStagesTemplate/app"]: './src/handlers/stagesTemplate/updateStagesTemplate/app.js'}

const entriesTask = { ["createTask/app"]: './src/handlers/task/createTask/app.js',
["enableDisableTask/app"]: './src/handlers/task/enableDisableTask/app.js',
["getAllTask/app"]: './src/handlers/task/getAllTask/app.js',
["getTaskById/app"]: './src/handlers/task/getTaskById/app.js',
["UpdateTask/app"]: './src/handlers/task/UpdateTask/app.js'}

const entriesTransportUnit = { ["appropierTransportUnitFms/app"]: './src/handlers/transportUnit/appropierTransportUnitFms/app.js',
["createTransportUnit/app"]: './src/handlers/transportUnit/createTransportUnit/app.js',
["deleteResourcesPhotosTransportUnit/app"]: './src/handlers/transportUnit/deleteResourcesPhotosTransportUnit/app.js',
["getAllTransportUnitsByCompanyFms/app"]: './src/handlers/transportUnit/getAllTransportUnitsByCompanyFms/app.js',
["getTransportUnitAndUserById/app"]: './src/handlers/transportUnit/getTransportUnitAndUserById/app.js',
["getTransportUnitById/app"]: './src/handlers/transportUnit/getTransportUnitById/app.js',
["misappropriateTransportUnitFms/app"]: './src/handlers/transportUnit/misappropriateTransportUnitFms/app.js',
["searchTransportUnitPlate/app"]: './src/handlers/transportUnit/searchTransportUnitPlate/app.js',
["searchTransportUnitPlateFms/app"]: './src/handlers/transportUnit/searchTransportUnitPlateFms/app.js',
["updateTransportUnit/app"]: './src/handlers/transportUnit/updateTransportUnit/app.js',
["searchTransportUnitPlateForLoadingFms/app"]: './src/handlers/transportUnit/searchTransportUnitPlateForLoadingFms/app.js'}

const entriesTravelStatus = { ["createTravelStatus/app"]: './src/handlers/travelStatus/createTravelStatus/app.js',
["enableDisableTravelStatus/app"]: './src/handlers/travelStatus/enableDisableTravelStatus/app.js',
["getAllTravelStatus/app"]: './src/handlers/travelStatus/getAllTravelStatus/app.js',
["getTravelStatusById/app"]: './src/handlers/travelStatus/getTravelStatusById/app.js',
["updateTravelStatus/app"]: './src/handlers/travelStatus/updateTravelStatus/app.js'}

const entriesTypeServices = { ["createTypeServices/app"]: './src/handlers/typeServices/createTypeServices/app.js',
["enableDisableTypeServices/app"]: './src/handlers/typeServices/enableDisableTypeServices/app.js',
["getAllTypeServices/app"]: './src/handlers/typeServices/getAllTypeServices/app.js',
["getTypeServicesById/app"]: './src/handlers/typeServices/getTypeServicesById/app.js',
["UpdateTypeServices/app"]: './src/handlers/typeServices/UpdateTypeServices/app.js'}

const entriesTypeTransportUnit = { ["createTypeTransportUnit/app"]: './src/handlers/typeTransportUnit/createTypeTransportUnit/app.js',
["enableDisableTypeTransportUnit/app"]: './src/handlers/typeTransportUnit/enableDisableTypeTransportUnit/app.js',
["getAllTypeTransportUnit/app"]: './src/handlers/typeTransportUnit/getAllTypeTransportUnit/app.js',
["getTypeTransportUnitById/app"]: './src/handlers/typeTransportUnit/getTypeTransportUnitById/app.js',
["updateTypeTransportUnit/app"]: './src/handlers/typeTransportUnit/updateTypeTransportUnit/app.js'}

const entriesVersions = { ["createVersions/app"]: './src/handlers/versions/createVersions/app.js',
["getAllVersions/app"]: './src/handlers/versions/getAllVersions/app.js',
["pruebaPush/app"]: './src/handlers/versions/pruebaPush/app.js',
["updateVersions/app"]: './src/handlers/versions/updateVersions/app.js'}

const entriesVipDrivers = { ["createVipDriver/app"]: './src/handlers/vipDrivers/createVipDriver/app.js',
["enableDisableVipDriver/app"]: './src/handlers/vipDrivers/enableDisableVipDriver/app.js',
["getAllVipDriver/app"]: './src/handlers/vipDrivers/getAllVipDriver/app.js',
["getVipDriverById/app"]: './src/handlers/vipDrivers/getVipDriverById/app.js',
["updateVipDriver/app"]: './src/handlers/vipDrivers/updateVipDriver/app.js'}

const entriesUsers = { ["confirmUser/app"]: './src/handlers/users/confirmUser/app.js',
["confrimUsersCarrier/app"]: './src/handlers/users/confrimUsersCarrier/app.js', 
["deleteUserResourcesPhotos/app"]: './src/handlers/users/deleteUserResourcesPhotos/app.js', 
["DeleteUsersClientAutomaticForTravelLink/app"]: './src/handlers/users/DeleteUsersClientAutomaticForTravelLink/app.js', 
["getAllUsers/app"]: './src/handlers/users/getAllUsers/app.js', 
["getUserById/app"]: './src/handlers/users/getUserById/app.js',
["getUserByIdCognito/app"]: './src/handlers/users/getUserByIdCognito/app.js',
["getUserByNumberPhone/app"]: './src/handlers/users/getUserByNumberPhone/app.js',
["getUsersByCompanyClientId/app"]: './src/handlers/users/getUsersByCompanyClientId/app.js',
["matchUserClientAndCompanyClient/app"]: './src/handlers/users/matchUserClientAndCompanyClient/app.js',
["pushTokenNotification/app"]: './src/handlers/users/pushTokenNotification/app.js',
["searchUserClientByEmail/app"]: './src/handlers/users/searchUserClientByEmail/app.js',
["searchUserTransportUnit/app"]: './src/handlers/users/searchUserTransportUnit/app.js',
["updateArrayCompany/app"]: './src/handlers/users/updateArrayCompany/app.js',
["updateArrayToken/app"]: './src/handlers/users/updateArrayToken/app.js',
["updateConfigUsers/app"]: './src/handlers/users/updateConfigUsers/app.js',
["updateTokenSession/app"]: './src/handlers/users/updateTokenSession/app.js',
["updateUserClientAndCompanyClient/app"]: './src/handlers/users/updateUserClientAndCompanyClient/app.js',
["updateUserProfileAddress/app"]: './src/handlers/users/updateUserProfileAddress/app.js',
["userClientRegister/app"]: './src/handlers/users/userClientRegister/app.js',
["userRegister/app"]: './src/handlers/users/userRegister/app.js',
["userRegisterCarrier/app"]: './src/handlers/users/userRegisterCarrier/app.js',
["verifyUserClientHaveOperaction/app"]: './src/handlers/users/verifyUserClientHaveOperaction/app.js',
["userRegisterMaster/app"]: './src/handlers/users/userRegisterMaster/app.js',
}

const entriesCarrier = { ["deleteCarrierAndTransportUnit/app"]: './src/handlers/carrier/deleteCarrierAndTransportUnit/app.js',
["getAllCarrier/app"]: './src/handlers/carrier/getAllCarrier/app.js',
["getCarrierById/app"]: './src/handlers/carrier/getCarrierById/app.js',
["searchCarrierFreeTransportUnit/app"]: './src/handlers/carrier/searchCarrierFreeTransportUnit/app.js',
["updateCarrierById/app"]: './src/handlers/carrier/updateCarrierById/app.js',
["userRegistreCarrier/app"]: './src/handlers/carrier/userRegistreCarrier/app.js',
["userRegistreForLink/app"]: './src/handlers/carrier/userRegistreForLink/app.js',
}


export {
  entriesBasicTransportUnit,
  entriesBenefits,
  entriesBoardingMode,
  entriesBrand,
  entriesCategory,
  entriesCheckPoints,
  entriesCities,
  entriesClient,
  entriesCompany,
  entriesCompanyClient,
  entriesDashboard,
  entriesDispatchType,
  entriesFeaturesTransportUnit,
  entriesLoadingOrderStatus,
  entriesMeasurementUnit,
  entriesModules,
  entriesNews,
  entriesTravel,
  entriesNotification,
  entriesOperation,
  entriesOperationStatus,
  entriesOperator,
  entriesPlace,
  entriesPostulation,
  entriesRating,
  entriesReport,
  entriesRoute,
  entriesSetting,
  entriesStages,
  entriesStagesTemplate,
  entriesTask,
  entriesTransportUnit,
  entriesTravelStatus,
  entriesTypeServices,
  entriesTypeTransportUnit,
  entriesVersions,
  entriesVipDrivers,
  entriesUsers,
  entriesCarrier,
};
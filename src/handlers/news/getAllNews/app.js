const { dbConnection } = require('../../../database/config');
const News = require('../../../models/news');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    await dbConnection(process.env.dataBaseConnection);
    let app = event?.queryStringParameters?.app;
    let news = [];
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    if (app) {
      if (page || size) {
        console.log('entro por verdadero page');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
      }
      news = await News.find({ 'account.enable': true })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort({ 'account.createDate': -1 });

      let totalApp;
      if (page || size) {
        totalApp = await News.find({
          'account.enable': true,
        }).countDocuments();
        totalApp = parseInt(totalApp, 10);
        totalPage = totalApp / sizeNumber;
        totalPage = Math.ceil(totalPage);
      }
    } else {
      if (page || size) {
        console.log('entro por verdadero page');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
      }
      let objectOrder = {};
      if (order) {
        objectOrder[`${order}`] = 1;
      }
      let objectFilter = {};
      if (filter) {
        objectFilter = {
          'account.enable': true,
        };
      }

      let objectSearch;
      if (search) {
        objectSearch = {
          name: new RegExp(search, 'i'),
        };
      }
      news = await await News.find({
        ...objectFilter,
        ...objectSearch,
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort(objectOrder);
      let total;
      if (page || size) {
        total = await News.find({
          ...objectFilter,
          ...objectSearch,
        }).countDocuments();
        total = parseInt(total, 10);
        totalPage = total / sizeNumber;
        totalPage = Math.ceil(totalPage);
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        news,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const News = require('../../../models/news');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const newsId = event.pathParameters.newsId;
    console.log('newsId===>>>', newsId);

    let newsOne = await News.findOne({ _id: newsId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        new: newsOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

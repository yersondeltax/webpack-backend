const { dbConnection } = require('../../../database/config');
const News = require('../../../models/news');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const newsId = event?.pathParameters?.newsId || '';
    console.log('newsId===>>>', newsId);

    let newOne = await News.deleteOne({ _id: newsId });

    console.log('newOne====>>>', newOne);

    if (newOne.deletedCount > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Noticia eliminado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Error al eliminar la noticia.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

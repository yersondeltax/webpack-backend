var AWS = require('aws-sdk');
// var lambda = new AWS.Lambda({ region: 'us-east-1' });
var stepFunctions = new AWS.StepFunctions({ region: 'us-east-1' });
const { dbConnection } = require('../../../database/config');
const News = require('../../../models/news');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const stateMachineArn = process?.env?.stateMachineArn || '';
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    console.log('entro aca===>>>');

    const newNews = new News(body);
    const newsSaved = await newNews.save();
    let travelId = 0;
    let news = newsSaved._id;
    let type = 'new';

    const payload = JSON.stringify({ travelId: travelId, news, type });
    // let params = {
    //   FunctionName: 'travel-notifyPublish-c7dedNURMXDe',
    //   InvocationType: 'RequestResponse',
    //   Payload: payload,
    // };
    // //====== produccion ====
    // let params = {
    //   stateMachineArn:
    //     'arn:aws:states:us-east-1:879323694935:stateMachine:notifyPublishStateMachine-MI0e1C2LuRB5',
    //   input: payload,
    // };

    // ====== staging ======
    // let params = {
    //   stateMachineArn:
    //     'arn:aws:states:us-east-1:345062269307:stateMachine:notifyPublishStateMachine-ayLxMACz2i6S',
    //   input: payload,
    // };
    let params = {
      stateMachineArn: stateMachineArn,
      input: payload,
    };

    // ====== staging ======
    // let params = {
    //   stateMachineArn:
    //     'arn:aws:states:us-east-1:345062269307:stateMachine:notifyPublishStateMachine-ayLxMACz2i6S',
    //   input: payload,
    // };
    // await lambda.invoke(params).promise();
    await stepFunctions.startExecution(params).promise();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Noticia creada exitosamente',
        new: newsSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const News = require('../../../models/news');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // await dbConnection(process.env.dataBaseConnection);
    // let updateDate = moment().format();
    // console.log('event ==> ', event);
    // const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    // console.log('body===>>>', body);

    // const newsId = event.pathParameters.newsId;
    // console.log('newsId===>>>', newsId);

    // let newsUpdate = await News.updateOne(
    //   { _id: newsId },
    //   {
    //     $set: { ...body, 'account.updateDate': updateDate },
    //   }
    // );
    // console.log('newsUpdate===>>>', newsUpdate);
    // if (newsUpdate.nModified > 0) {
    //   return {
    //     statusCode: 200,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'Noticia actualizada.',
    //     }),
    //   };
    // } else {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No se pudo actulizar.',
    //     }),
    //   };
    // }

    console.log('event=====>>', event);
    console.log('context=====>>', context);
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

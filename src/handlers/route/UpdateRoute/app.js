const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Route = require('../../../models/route');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const routeId = event.pathParameters.routeId;
    console.log('routeId===>>>', routeId);

    let updateDate = moment().format();
    let routeUpdate = await Route.updateOne(
      { _id: routeId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('routeUpdate===>>>', routeUpdate);
    if (routeUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Dato actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

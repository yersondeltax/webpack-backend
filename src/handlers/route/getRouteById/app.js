const { dbConnection } = require('../../../database/config');
const Route = require('../../../models/route');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const routeId = event?.pathParameters?.routeId || '';
    console.log('routeId===>>>', routeId);

    let route = await Route.findOne({ _id: routeId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        route,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

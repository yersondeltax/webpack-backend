const { dbConnection } = require('../../../database/config');
const Route = require('../../../models/route');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    const routeDB = new Route(data);
    const routeDBSave = await routeDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Ruta creada exitosamente',
        route: routeDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

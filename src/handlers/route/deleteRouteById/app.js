const { dbConnection } = require('../../../database/config');
const Route = require('../../../models/route');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const routeId = event?.pathParameters?.routeId || '';
    console.log('routeId===>>>', routeId);

    let routeOne = await Route.deleteOne({ _id: routeId });

    console.log('routeOne====>>>', routeOne);

    if (routeOne.deletedCount > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Ruta eliminada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Error al eliminar la ruta.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

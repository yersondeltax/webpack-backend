const { dbConnection } = require('../../../database/config');
const Rating = require('../../../models/rating');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newRating = new Rating(body);
    const ratingSaved = await newRating.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Calificación registrada exitosamente',
        rating: ratingSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

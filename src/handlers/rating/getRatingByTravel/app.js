const { dbConnection } = require('../../../database/config');
const Rating = require('../../../models/rating');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const travelId = event?.pathParameters?.travelId;
    console.log('travelId===>>>', travelId);

    let ratingOne = await Rating.findOne({ travelId: travelId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        rating: ratingOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

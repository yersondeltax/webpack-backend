const { dbConnection } = require('../../../database/config');
const mongoose = require('mongoose');
const Rating = require('../../../models/rating');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let userId = event?.pathParameters?.userId || '';
    console.log('userId====>>', userId);
    await dbConnection(process.env.dataBaseConnection);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let ratings = [];
    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let average = 0;
    let percentageFive = 0;
    let percentageFour = 0;
    let percentageThree = 0;
    let percentageTwo = 0;
    let percentageOne = 0;

    ratings = await Rating.find({ userId: userId, 'account.enable': true })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort({ 'account.createDate': -1 });
    if (ratings.length > 0) {
      let resultAverage = await Rating.aggregate([
        {
          $match: { userId: mongoose.Types.ObjectId(`${userId}`) },
        },
        {
          $group: {
            _id: '$userId',
            avgValue: { $avg: '$value' },
          },
        },
      ]);
      average = resultAverage[0]?.avgValue || 0;

      let countRatingFull = await Rating.find({
        userId: userId,
      }).countDocuments();

      let countRatingFive = await Rating.find({
        userId: userId,
        value: 5,
      }).countDocuments();
      percentageFive = (countRatingFive * 100) / countRatingFull;
      percentageFive = percentageFive.toFixed(2);

      let countRatingFour = await Rating.find({
        userId: userId,
        value: 4,
      }).countDocuments();

      percentageFour = (countRatingFour * 100) / countRatingFull;
      percentageFour = percentageFour.toFixed(2);

      let countRatingThree = await Rating.find({
        userId: userId,
        value: 3,
      }).countDocuments();

      percentageThree = (countRatingThree * 100) / countRatingFull;
      percentageThree = percentageThree.toFixed(2);

      let countRatingTwo = await Rating.find({
        userId: userId,
        value: 2,
      }).countDocuments();

      percentageTwo = (countRatingTwo * 100) / countRatingFull;
      percentageTwo = percentageTwo.toFixed(2);

      let countRatingOne = await Rating.find({
        userId: userId,
        value: 1,
      }).countDocuments();

      percentageOne = (countRatingOne * 100) / countRatingFull;
      percentageOne = percentageOne.toFixed(2);
    }
    let total;
    if (page || size) {
      total = await Rating.find({ userId: userId, 'account.enable': true }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        average,
        percentageFive,
        percentageFour,
        percentageThree,
        percentageTwo,
        percentageOne,
        ratings,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const CheckPoints = require('../../../models/checkPoints');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newCheckPoint = new CheckPoints(body);
    const CheckPointSaved = await newCheckPoint.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        checkPoint: CheckPointSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

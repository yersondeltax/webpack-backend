var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const { v4: uuidv4 } = require('uuid');

const CheckPoints = require('../../../models/checkPoints');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const Notification = require('../../../models/notification');
const User = require('../../../models/user');

const { dbConnection } = require('../../../database/config');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    const travelId = event.pathParameters.travelId;

    let idNotification = uuidv4().toString();
    const newCheckPoint = new CheckPoints(body);
    const CheckPointSaved = await newCheckPoint.save();

    let checkPointId = CheckPointSaved._id;

    let loadingOrderId = CheckPointSaved.loadingOrderId;

    let loadingOrder = await Travel.findOne({
      'loadingOrder.loadingOrderId': loadingOrderId,
      _id: travelId,
    });
    console.log('loadingOrder===', loadingOrder);
    let idOperation = loadingOrder.operationId;

    let listOperation = await Operation.findById(idOperation);
    console.log('listOperation===', listOperation);

    let userOperadorId = listOperation.userOperatorId;

    let messagePublish = 'checkPoint recibido.';
    let typeNotification = 'checkPoints';

    let userOperador = await User.findById(userOperadorId);
    console.log('userOperador===', userOperador);
    let userId = userOperador?._id || null;
    console.log('userId===>>', userId);
    let tokenOperator = userOperador?.device?.pushToken || null;
    // let tokenOperator = operadorDevices[operadorDevices.length - 1]?.pushToken || '';
    console.log('tokenOperator===>>', tokenOperator);

    let dataBody = {
      travelId: travelId,
      userId: userId,
      tokenUser: tokenOperator,
      message: messagePublish,
      type: typeNotification,
      operationId: idOperation,
      checkPointId: checkPointId,
      loadingOrderId: loadingOrderId,
    };
    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    // console.log('entrooo aca');

    // let payloadNotification = {
    //   default: 'default',
    //   GCM: {
    //     notification: {
    //       title: 'DeltaX',
    //       body: 'checkPoint recibido.',
    //       sound: 'default',
    //     },
    //     data: {
    //       type: 'checkPoints',
    //       operationId: idOperation,
    //       loadingOrderId: loadingOrderId,
    //       travel: travelId,
    //       id: idNotification,
    //       checkPointId: checkPointId,
    //     },
    //   },
    // };
    // console.log('payloadNotification=====>>', payloadNotification);

    // payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    // payloadNotification = JSON.stringify(payloadNotification);

    // const params_sns = {
    //   TargetArn: tokenOperator,
    //   // TargetArn:
    //   //   'arn:aws:sns:us-east-1:879323694935:endpoint/GCM/sendNotificationDelta/a9070445-e03b-36c6-8352-ea2bd2059680',
    //   Message: payloadNotification,
    //   MessageStructure: 'json',
    // };
    // const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    // const publishTextPromise2 = await sns_new.publish(params_sns).promise();
    // console.log('publishTextPromise2=====>>', publishTextPromise2);
    // console.log('Termino todo bien');

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        checkPoint: CheckPointSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const CheckPoints = require('../../../models/checkPoints');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const checkPoints = await CheckPoints.find().sort({ dateTime: 1 });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        checkPoints,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const CheckPoints = require('../../../models/checkPoints');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const loadingOrderId = event.pathParameters.loadingOrderId;
    const type = event.pathParameters.type;
    let checkPoints = [];
    if (type == 'all') {
      checkPoints = await CheckPoints.find({
        loadingOrderId: loadingOrderId,
      }).sort({ dateTime: 1 });
    } else {
      if (type == 'last') {
        let checkPointsDes = await CheckPoints.find({
          loadingOrderId: loadingOrderId,
        }).sort({ _id: -1 });
        if (checkPointsDes.length) {
          let lastCheckPoints = checkPointsDes[0];
          checkPoints.push(lastCheckPoints);
        }
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        checkPoints: checkPoints,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

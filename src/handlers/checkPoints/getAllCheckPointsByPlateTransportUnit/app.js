const { dbConnection } = require('../../../database/config');
const CheckPoints = require('../../../models/checkPoints');
const TransportUnit = require('../../../models/transportUnit');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const plate = event?.pathParameters?.plate || '';
    const filter = event?.queryStringParameters?.filter || 'all';
    console.log('plate===>>', plate);
    console.log('filter===>>', filter);
    if (plate == '') {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Ingrese la placa de una unidad de transporte.',
        }),
      };
    }
    const transportUnitData = await TransportUnit.findOne({
      plate,
    });

    if (!transportUnitData) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'la placa ingresada no existe.',
        }),
      };
    }

    const travelData = await Travel.findOne({
      'loadingOrder.assignment.transportUnitId': transportUnitData._id,
      'loadingOrder.assignment.assignmentStatus': 1,
    });

    if (!travelData) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'La unidad de transporte no tiene orden de carga asignada.',
        }),
      };
    }

    let loadingOrderId = travelData?.loadingOrder?.loadingOrderId || null;

    let checkPoints = [];
    if (filter == 'all') {
      checkPoints = await CheckPoints.find({
        loadingOrderId: loadingOrderId,
      }).sort({ dateTime: 1 });
    } else {
      if (filter == 'last') {
        let checkPointsDes = await CheckPoints.find({
          loadingOrderId: loadingOrderId,
        }).sort({ dateTime: -1 });
        if (checkPointsDes.length) {
          let lastCheckPoints = checkPointsDes[0];
          checkPoints.push(lastCheckPoints);
        }
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        checkPoints: checkPoints,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const clients = await User.find({
      type: 'CLIENT',
    }).sort({ 'account.createDate': 1 });

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        clients,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

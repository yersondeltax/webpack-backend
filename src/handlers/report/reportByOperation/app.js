const PDFDocument = require('pdfkit');
var AWS = require('aws-sdk');
const path = require('path');
AWS.config.update({ region: 'us-east-1' });
const s3 = new AWS.S3();
const moment = require('moment');
const momentTime = require('moment-timezone');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const TransportUnit = require('../../../models/transportUnit');
const MeasurementUnit = require('../../../models/measurementUnit');
const User = require('../../../models/user');

const generatePdf = async (travels) => {
  let travelsOperation = travels;
  console.log('travelsOperation====?>>>', travelsOperation);
  return await new Promise((resolve, reject) => {
    const doc = new PDFDocument({
      size: [612.0, 792.0], //[792.0, 612.0]
      margins: {
        top: 10,
        bottom: 10,
        left: 10,
        right: 10,
      },
      info: {
        Title: 'Reporte por operación',
        Author: 'DeltaX SRL.',
      },
    });

    let direccion = path.resolve(__dirname, 'logo-deltax.png');
    console.log('direccion====>>>', direccion);
    doc
      .image('logo-deltax.png', 350, 50, { width: 200, height: 50 })
      // .fillColor('black')
      // .fontSize(8)
      // .text('DeltaX SRL.', 200, 50, { width: 100, align: 'right' })
      // .text('deltax@gmail.com', 200, 65, { width: 100, align: 'right' })
      // .text('Avenida Tte. Walter Vega', 200, 80, { width: 100, align: 'right' })
      .moveDown();
    // doc.text('How Are You');
    doc.font('Helvetica-Bold', 12).text('DELTAEXPRESS S.R.L.', 35, 130);

    doc
      .font('Helvetica-Bold', 8)
      .fontSize(8)
      .lineJoin('round')
      .rect(35, 200 - 5, 545, 15)
      .fillOpacity(0.9)
      .fillAndStroke('#EF9405')
      .fillColor('black')
      .text('N°', 43, 200)
      .text('Fecha', 80, 200)
      .text('Origen', 150, 200)
      .text('Destino', 230, 200)
      .text('Peso(Tn)', 300, 200)
      .text('Flete(Bs)', 350, 200)
      .text('Transportista', 420, 200)
      .text('Placa', 535, 200);

    doc.strokeColor('#000000').lineWidth(1.5).moveTo(35, 210).lineTo(580, 210).stroke();
    doc.moveTo(35, 210).lineTo(35, 195).stroke();
    doc.moveTo(65, 210).lineTo(65, 195).stroke();
    doc.moveTo(125, 210).lineTo(125, 195).stroke();
    doc.moveTo(205, 210).lineTo(205, 195).stroke();
    doc.moveTo(295, 210).lineTo(295, 195).stroke();
    doc.moveTo(345, 210).lineTo(345, 195).stroke();
    doc.moveTo(395, 210).lineTo(395, 195).stroke();
    doc.moveTo(515, 210).lineTo(515, 195).stroke();
    doc.moveTo(580, 210).lineTo(580, 195).stroke();

    doc.moveDown();

    let x = 35;
    let y = 220;
    let totalWeightUnit = 0;

    for (let i = 0; i < travelsOperation.length; i++) {
      let data = travelsOperation[i];
      let dateLoad = data?.travel?.dates?.loadingDate || false;
      let dateLoading = '';
      console.log('dateLoad====>>', dateLoad);
      console.log('dateLoading====>>', dateLoading);
      if (dateLoad) {
        dateLoading = momentTime
          .utc(dateLoad, 'YYYY-MM-DDTHH:mm:ss')
          .tz('America/La_Paz')
          .format('DD-MM-YYYY');
      }
      let contador = i + 1;
      let cityOrigin = data?.travel?.route?.origin?.cityOrigin || '';
      const arrayNameCity = cityOrigin.split(',');
      console.log('arrayNameCity===>>', arrayNameCity);
      let nameCityRoute = arrayNameCity[arrayNameCity.length - 1] || '';
      nameCityRoute = nameCityRoute.trim();
      console.log('nameCityRoute====>>', nameCityRoute);
      let cityDestination = data?.travel?.route?.destination?.cityDestination || '';
      const arrayNameCityDestination = cityDestination.split(',');
      console.log('arrayNameCityDestination===>>', arrayNameCityDestination);
      let nameCityRouteDestination =
        arrayNameCityDestination[arrayNameCityDestination.length - 1] || '';
      nameCityRouteDestination = nameCityRouteDestination.trim();
      console.log('nameCityRouteDestination====>>', nameCityRouteDestination);

      let weightUnit = data?.travel?.loadingOrder?.weightUnit?.value || 0;
      let weightUnitAbbreviation = data?.travel?.loadingOrder?.weightUnit?.abbreviation || '';
      let freightValue = data?.travel?.loadingOrder?.carrierFreight?.freightValue || 0;

      let fleteCarrierUnitAbbreviation = data?.fleteCarrierUnit?.abbreviation || '';

      let plate = data?.transportUnitTravel?.plate || '';

      let firstNameUser = data?.userCarrier?.profile?.firstName || '';
      firstNameUser = firstNameUser.trim();
      let lastNameUser = data?.userCarrier?.profile?.lastName || '';
      lastNameUser = lastNameUser.trim();

      let nameUserArray = firstNameUser.split(' ');
      firstNameUser = nameUserArray[0];
      let lastNameUserArray = lastNameUser.split(' ');
      lastNameUser = lastNameUserArray[0];
      let fullNameUser = firstNameUser + '' + lastNameUser;

      totalWeightUnit = totalWeightUnit + weightUnit;

      doc
        .font('Helvetica-Bold', 8)
        .fontSize(8)
        .text(`${contador}`, 35, y, { width: 25, align: 'center', height: 30 })
        .text(`${dateLoading}`, 65, y, { width: 58, align: 'center', height: 30 })
        .text(`${nameCityRoute}`, 125, y, { width: 78, align: 'center', height: 30 })
        .text(`${nameCityRouteDestination}`, 205, y, { width: 78, align: 'center', height: 30 })
        .text(`${weightUnit} (${weightUnitAbbreviation})`, 295, y, {
          width: 45,
          align: 'center',
          height: 30,
        })
        .text(`${freightValue} (${fleteCarrierUnitAbbreviation})`, 345, y, {
          width: 50,
          align: 'center',
          height: 30,
        })
        .text(`${fullNameUser}`, 398, y, { width: 115, align: 'center', height: 30 })
        .text(`${plate}`, 515, y, { width: 60, align: 'center', height: 30 });

      doc.moveDown();

      y = y + 20;
    }

    doc
      .font('Helvetica-Bold', 8)
      .fontSize(8)
      .lineJoin('round')
      .rect(35, y - 5, 260, 15)
      .fillOpacity(0.9)
      .fillAndStroke('#EF9405')
      .fillColor('black')
      .text('Total', 35, y, { width: 290, align: 'center', height: 30 });
    doc.font('Helvetica-Bold', 8).fontSize(8).text(`${totalWeightUnit}`, 295, y, {
      width: 45,
      align: 'center',
      height: 30,
    });

    doc
      .strokeColor('#000000')
      .lineWidth(1.5)
      .moveTo(35, y - 5)
      .lineTo(515, y - 5)
      .stroke();
    doc
      .strokeColor('#000000')
      .lineWidth(1.5)
      .moveTo(35, y + 10)
      .lineTo(515, y + 10)
      .stroke();

    doc.end();
    const buffers = [];
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', () => {
      const pdfData = Buffer.concat(buffers);
      resolve(pdfData);
    });
  });
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const operationId = event.pathParameters.operationId;
    let travels = await Travel.find({
      operationId: operationId,
      'loadingOrder.loadingOrderId': { $exists: true },
      $or: [
        { 'loadingOrder.LoadingOrderStatus.order': 3 },
        { 'loadingOrder.LoadingOrderStatus.order': 4 },
      ],
    });
    if (travels.length == 0) {
      return {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          // 'Content-type': 'application/pdf',
          // 'content-disposition': 'attachment; filename=test.pdf',
        },
        body: JSON.stringify({
          message: 'No se encuentran ordenes de carga en estado por pagar o pagadas.',
        }),
      };
    }
    let newTravelTransportUnit = [];
    for (const dataTravel of travels) {
      let typeCurrencyFreightId =
        dataTravel?.loadingOrder?.carrierFreight?.typeCurrencyFreightId || '';
      let fleteCarrierUnit = {};
      fleteCarrierUnit = await MeasurementUnit.findOne({ _id: typeCurrencyFreightId });
      let transportUnitTravel = {};
      let transportUnitId = dataTravel?.loadingOrder?.assignment?.transportUnitId || '';
      let user = {};
      if (transportUnitId != '') {
        transportUnitTravel = await TransportUnit.findOne({ _id: transportUnitId });
        let userId =
          transportUnitTravel.drivers[transportUnitTravel.drivers.length - 1].userId || '';
        user = await User.findById(userId);
      }

      let newObject = {
        travel: dataTravel,
        fleteCarrierUnit,
        transportUnitTravel,
        userCarrier: user,
      };
      newTravelTransportUnit.push(newObject);
    }
    const stream = await generatePdf(newTravelTransportUnit);
    return {
      statusCode: 200,
      isBase64Encoded: true,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        // 'Content-type': 'application/pdf',
        // 'content-disposition': 'attachment; filename=test.pdf',
      },
      body: JSON.stringify({
        stream: stream.toString('base64'),
      }),
      // body: stream.toString('base64'),
    };
  } catch (err) {
    console.log(err);
    return err;
  }
  // console.log('=====>>1');
  // const data = {
  //   title: ' Pdf generation using puppeteer',
  //   text: ' Handlebar is awesome!',
  // };
  // console.log('=====>>2');
  // const executablePath = await chromium.executablePath;
  // const file = fs.readFileSync(path.resolve(__dirname, 'template.hbs'), 'utf8');
  // const template = handlebars.compile(file);
  // const html = template(data);

  // let browser = null;

  // console.log('=====>>3');
  // try {
  //   console.log('=====>>4');
  //   browser = await chromium.puppeteer.launch({
  //     args: chromium.args,
  //     defaultViewport: chromium.defaultViewport,
  //     executablePath,
  //     headless: chromium.headless,
  //   });
  //   console.log('=====>>5');
  //   const page = await browser.newPage();

  //   page.setContent(html);
  //   console.log('=====>>6');
  //   const pdf = await page.pdf({
  //     format: 'A4',
  //     printBackground: true,
  //     margin: { top: '1cm', right: '1cm', bottom: '1cm', left: '1cm' },
  //   });
  //   console.log('=====>>7');
  //   // TODO: Response with PDF (or error if something went wrong )
  //   const response = {
  //     headers: {
  //       'Content-type': 'application/pdf',
  //       'content-disposition': 'attachment; filename=test.pdf',
  //     },
  //     statusCode: 200,
  //     body: pdf.toString('base64'),
  //     isBase64Encoded: true,
  //   };

  //   const output_filename = 'pdf-demo.pdf';

  //   const s3Params = {
  //     Bucket: 'pdf-demo-pipeso',
  //     Key: `public/pdfs/${output_filename}`,
  //     Body: pdf,
  //     ContentType: 'application/pdf',
  //     ServerSideEncryption: 'AES256',
  //   };

  //   s3.putObject(s3Params, (err) => {
  //     if (err) {
  //       console.log('err', err);
  //       return callBack(null, { error });
  //     }
  //   });

  //   context.succeed(response);
  // } catch (error) {
  //   return context.fail(error);
  // } finally {
  //   if (browser !== null) {
  //     await browser.close();
  //   }
  // }
};

var AWS = require('aws-sdk');
const moment = require('moment');
const mongoose = require('mongoose');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Operation = require('../../../models/operation');
const Postulation = require('../../../models/postulation');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const Notification = require('../../../models/notification');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const travelId = event?.pathParameters?.travelId || null;
    const transportUnitId = body?.assignment?.transportUnitId || null;
    let date = moment().format();
    let loadingOrderStatusAsignmen = await LoadingOrderStatus.findOne({
      order: 1,
    });
    let status = {
      statusId: loadingOrderStatusAsignmen?._id || '',
      name: loadingOrderStatusAsignmen?.name || '',
      date: date,
      order: loadingOrderStatusAsignmen?.order,
    };
    console.log('status===>>>', status);

    let travelAndTransportUnit = await Travel.findOne({
      'loadingOrder.assignment.transportUnitId': transportUnitId,
      'loadingOrder.assignment.assignmentStatus': 1,
    });
    console.log('travelAndTransportUnit======>>>', travelAndTransportUnit);
    let objectoNuevoTravel = {};
    let newComparationByTravel =
      JSON.stringify(travelAndTransportUnit) === JSON.stringify(objectoNuevoTravel);
    console.log('newComparationByTravel======>>>', newComparationByTravel);
    if (travelAndTransportUnit != null) {
      if (newComparationByTravel == false) {
        return {
          statusCode: 200,
          headers,
          body: JSON.stringify({
            message: 'El chofer asignado tiene una orden de carga ya asignada.',
          }),
        };
      }
    }

    const travelUpdate = await Travel.findOneAndUpdate(
      { _id: travelId },
      {
        $set: {
          'loadingOrder.assignment.transportUnitId': transportUnitId,
          'loadingOrder.assignment.assignmentStatus': 1,
          'loadingOrder.LoadingOrderStatus': [status],
        },
      },
      { new: true }
    );

    let operationId = travelUpdate?.operationId || null;
    let loadingOrderId = travelUpdate?.loadingOrder?.loadingOrderId || null;

    // let operationStatusProgres = await OperationStatus.findOne({ order: 2 });
    // console.log('operationStatusProgres===>>>', operationStatusProgres);
    // let operationStatusProgresId = operationStatusProgres?._id || '';

    // let operationData = await Operation.findOneAndUpdate(
    //   {
    //     _id: operationId,
    //   },
    //   { $set: { statusOperationId: operationStatusProgresId } },
    //   { new: true }
    // );

    const operationTravel = await Operation.findById(operationId);

    const transportUnitFind = await TransportUnit.findById(transportUnitId);
    console.log('transportUnitFind ==> ', transportUnitFind);

    const carrierId =
      transportUnitFind?.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

    let messagePublishAssinment = `Se te ha asignado una orden de carga. Ingresa para ver más detalles.`;
    let typeNotificationAssinment = 'AssignedLoadOrder';

    const carrierFind = await User.findById(carrierId);
    console.log('carrierFind ==> ', carrierFind);

    let userIdOn = carrierFind?._id || null;
    console.log('userIdOn===>>', userIdOn);

    const token = carrierFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let dataBodyAssinment = {
      userId: userIdOn,
      tokenUser: token,
      message: messagePublishAssinment,
      type: typeNotificationAssinment,
      travelId: travelId,
    };

    let newNotificationAssinment = new Notification(dataBodyAssinment);
    let notificationSavedAssinment = await newNotificationAssinment.save();
    console.log('notificationSavedAssinment===>>', notificationSavedAssinment);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Orden de carga creada correctamente.',
        operation: operationTravel,
        travel: travelUpdate,
        loadingOrderId: loadingOrderId,
      }),
    };
  } catch (err) {
    console.log('err ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

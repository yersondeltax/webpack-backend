const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const TransportUnit = require('../../../models/transportUnit');
const Setting = require('../../../models/setting');
const Postulation = require('../../../models/postulation');
const MeasurementUnit = require('../../../models/measurementUnit');
const Company = require('../../../models/company');
const Operation = require('../../../models/operation');
const User = require('../../.../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('event=======>>', event);
    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 5;
    var type = event?.queryStringParameters?.type || 'full';
    var pageNumber = parseInt(page, 10);
    var sizeNumber = parseInt(size, 10);
    console.log('page===>>', page);
    console.log('size===>>', size);
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;
    const transportUnit = await TransportUnit.findById(transportUnitId);
    let travels = [];
    if (type == 'full') {
      travels = await Travel.find({ publish: true })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort({ 'row.createDate': -1 });
      console.log('travelsFull======>>>', travels);
    } else {
      if (type == 'date') {
        console.log('entro en if de date');
        let dateBegin = event?.queryStringParameters?.dateBegin || '';
        let dateEnd = event?.queryStringParameters?.dateEnd || '';
        console.log('dateBegin=====>>>', dateBegin);
        console.log('dateEnd=====>>>', dateEnd);
        travels = await Travel.find({
          publish: true,
          'dates.loadingDate': { $gte: new Date(dateBegin), $lte: new Date(dateEnd) },
        })
          .limit(sizeNumber)
          .skip(pageNumber * sizeNumber)
          .sort({ 'row.createDate': -1 });
        console.log('travelsDates======>>>', travels);
      } else {
        if (type == 'category') {
          var categoryId = event?.queryStringParameters?.categoryId || '';
          travels = await Travel.find({ publish: true, 'categoryLoad.categoryLoadId': categoryId })
            .limit(sizeNumber)
            .skip(pageNumber * sizeNumber)
            .sort({ 'row.createDate': -1 });
          console.log('travelsCategory======>>>', travels);
        } else {
          if (type == 'categoryDate') {
            console.log('entro en if de categoryDate');
            let dateBegin = event?.queryStringParameters?.dateBegin || '';
            let dateEnd = event?.queryStringParameters?.dateEnd || '';
            let categoryId = event?.queryStringParameters?.categoryId || '';
            console.log('dateBegin=====>>>', dateBegin);
            console.log('dateEnd=====>>>', dateEnd);
            console.log('categoryId=====>>>', categoryId);
            travels = await Travel.find({
              publish: true,
              'dates.loadingDate': { $gte: new Date(dateBegin), $lte: new Date(dateEnd) },
              'categoryLoad.categoryLoadId': categoryId,
            })
              .limit(sizeNumber)
              .skip(pageNumber * sizeNumber)
              .sort({ 'row.createDate': -1 });
            console.log('travelsCategoryDate======>>>', travels);
          }
        }
      }
    }

    let newTravels = [];
    const setting = await Setting.find();

    for (let index = 0; index < travels.length; index++) {
      const travel = travels[index]._doc;
      let typeCurrencyOfferedId =
        travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      let operationId = travel?.operationId || '';
      const operation = await Operation.findById(operationId);
      let companyOperatorId = operation?.companyOperatorId || '';
      const companyData = await Company.findById(companyOperatorId);
      // let userOperatorId = operation?.userOperatorId || '';
      // const userOperatorData = await User.findById(userOperatorId);
      // const route = await Route.findById(travel.routeId);
      const postulation = await Postulation.find({
        travelId: travel._id,
        transportUnitId: transportUnitId,
        'account.enable': 1,
      });
      let flag = true;
      if (travel.features) {
        for (let j = 0; j < travel.features.length; j++) {
          const feature = travel.features[j];
          const newFeature = {
            name: feature.name,
            featuresTransportUnitId: feature.featuresTransportUnitId,
            valueQualitative: feature.valueQualitative || '',
            valueQuantitative: feature.valueQuantitative || 0,
          };
          const found = await transportUnit.features.find((featureTU) => {
            const newFeatureTU = {
              name: featureTU.name,
              featuresTransportUnitId: featureTU.featuresTransportUnitId,
              valueQualitative: featureTU.valueQualitative || '',
              // valueQuantitative: featureTU.valueQuantitative || '',
              valueQuantitative:
                featureTU.valueQuantitative <= newFeature.valueQuantitative
                  ? featureTU.valueQuantitative
                  : newFeature.valueQuantitative || 0,
            };
            return JSON.stringify(newFeatureTU) === JSON.stringify(newFeature);
          });
          if (!found) {
            if (setting[0].featureRestriction) {
              flag = false;
              break;
            } else {
              travel.features[j] = { ...feature._doc, hashtag: 'warning' };
            }
          }
        }
      }
      if (flag) {
        newTravels.push({
          ...travel,
          // route,
          typeCurrencyOffered,
          postulation,
          company: companyData,
          // userOperator: userOperatorData,
        });
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travels: newTravels,
      }),
    };
  } catch (err) {
    console.log('ERR ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

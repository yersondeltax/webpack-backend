const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const TransportUnit = require('../../../models/transportUnit');
const Setting = require('../../../models/setting');
const Postulation = require('../../../models/postulation');
const MeasurementUnit = require('../../../models/measurementUnit');
const Company = require('../../../models/company');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 5;
    page = parseInt(page, 10);
    size = parseInt(size, 10);
    console.log('page===>>', page);
    console.log('size===>>', size);
    const transportUnitId = event.pathParameters.transportUnitId;
    console.log('transportUnitId===>', transportUnitId);
    const deliveryDate = event.pathParameters.deliveryDate;
    console.log('deliveryDate===>', deliveryDate);
    let dias = 3;
    let newDateAdd = moment(deliveryDate).add(dias, 'day').format();
    console.log('newDateAdd===>', newDateAdd);
    let city = event.pathParameters.city || '';
    // console.log('city===>', city);
    // city = city.replace('%20', ' ');
    const transportUnit = await TransportUnit.findById(transportUnitId);
    ////////////////////////////////////
    // const routeIdCity = await Route.find({
    //   'origin.cityOrigin': city,
    // }).select('_id');
    // console.log('routeIdCity ==> ', routeIdCity);
    // const arrayRouteIds = [];
    // for (const routeCity of routeIdCity) {
    //   arrayRouteIds.push(routeCity._id);
    // }
    // console.log('arrayRouteIds====>>>', arrayRouteIds);
    /////////////////////////////////////
    const travels = await Travel.find({
      $and: [
        {
          'dates.loadingDate': {
            $gte: new Date(deliveryDate),
          },
        },
        {
          'dates.loadingDate': {
            $lte: new Date(newDateAdd),
          },
        },
        { 'route.origin.cityOriginId': city },
        { publish: true },
      ],
    })
      .limit(size)
      .skip(page * size)
      .sort({ 'row.createDate': 'descending' });
    console.log('longuitud del array===>>', travels.length);
    // let newTravels = travels;
    let newTravels = [];
    const setting = await Setting.find();

    for (let index = 0; index < travels.length; index++) {
      const travel = travels[index]._doc;
      // const route = await Route.findById(travel.routeId);
      let typeCurrencyOfferedId =
        travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      let operationId = travel?.operationId || '';
      const operation = await Operation.findById(operationId);
      let companyOperatorId = operation?.companyOperatorId || '';
      const companyData = await Company.findById(companyOperatorId);
      // let userOperatorId = operation?.userOperatorId || '';
      // const userOperatorData = await User.findById(userOperatorId);
      const postulation = await Postulation.find({
        travelId: travel._id,
        transportUnitId: transportUnitId,
        'account.enable': 1,
      });
      let flag = true;
      if (travel.features) {
        for (let j = 0; j < travel.features.length; j++) {
          const feature = travel.features[j];
          const newFeature = {
            name: feature.name,
            featuresTransportUnitId: feature.featuresTransportUnitId,
            valueQualitative: feature.valueQualitative || '',
            valueQuantitative: feature.valueQuantitative || 0,
          };
          const found = transportUnit.features.find((featureTU) => {
            const newFeatureTU = {
              name: featureTU.name,
              featuresTransportUnitId: featureTU.featuresTransportUnitId,
              valueQualitative: featureTU.valueQualitative || '',
              valueQuantitative:
                featureTU.valueQuantitative <= newFeature.valueQuantitative
                  ? featureTU.valueQuantitative
                  : newFeature.valueQuantitative || 0,
            };
            return JSON.stringify(newFeatureTU) === JSON.stringify(newFeature);
          });
          if (!found) {
            if (setting[0].featureRestriction) {
              flag = false;
              break;
            } else {
              travel.features[j] = { ...feature._doc, hashtag: 'warning' };
            }
          }
        }
      }
      if (flag) {
        newTravels.push({
          ...travel,
          // route,
          typeCurrencyOffered,
          postulation,
          company: companyData,
          // userOperator: userOperatorData,
        });
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travels: newTravels,
      }),
    };
  } catch (err) {
    console.log('ERR ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

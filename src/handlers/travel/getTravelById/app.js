const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const Stages = require('../../../models/stages');
const MeasurementUnit = require('../../../models/measurementUnit');
const Task = require('../../../models/task');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const travelId = event.pathParameters.travelId;

    const travel = await Travel.findById(travelId);
    let typeCurrencyOfferedId = travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
    let typeCurrencyFreightId = travel?.freightValues?.clientFreight?.typeCurrencyFreightId || '';
    const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
    const typeCurrencyFreight = await MeasurementUnit.findById(typeCurrencyFreightId);
    let travelFms = travel?.travelFms || false;
    // const route = await Route.findById(travel.routeId);
    let newTravel = {
      ...travel._doc,
      typeCurrencyOffered,
      typeCurrencyFreight,
      // route: {
      //   origin: route.origin,
      //   destination: route.destination,
      //   checkPoints: route.checkPoints,
      // },
    };

    if (
      (travel.loadingOrder.LoadingOrderStatus.length > 0 && travelFms == false) ||
      travelFms == true
    ) {
      const stages = await Stages.find({ loadingOrderId: travel?.loadingOrder?.loadingOrderId });
      let stagesArray = [];
      for (const dataStages of stages) {
        let data = dataStages._doc;
        let idStates = data?._id || '';
        let tasks = [];
        tasks = await Task.find({ stagesId: idStates });
        let objectStates = {
          ...data,
          tasks,
        };
        stagesArray.push(objectStates);
      }
      newTravel = { ...newTravel, stages: stagesArray };
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travel: newTravel,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

var AWS = require('aws-sdk');
const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const User = require('../../.../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const OperationStatus = require('../../../models/operationStatus');
const Operation = require('../../../models/operation');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const travelId = event.pathParameters.travelId;
    // const transportUnitId = body.assignment.transportUnitId;
    let date = moment().format();

    const travelStatusPay = await LoadingOrderStatus.findOne({ order: 4 });
    console.log('travelStatusPay====>>>', travelStatusPay);

    const status = {
      statusId: travelStatusPay?._id,
      name: travelStatusPay?.name,
      order: travelStatusPay?.order,
      date,
    };

    const travelUpdate = await Travel.findOneAndUpdate(
      { _id: travelId },
      {
        $push: { 'loadingOrder.LoadingOrderStatus': status },
      },
      { new: true }
    );
    let idOperation = travelUpdate.operationId;
    const loadingOrdersFinishByOperation = await Travel.find({
      operationId: idOperation,
      $or: [
        { 'loadingOrder.LoadingOrderStatus': { $size: 0 } },
        { 'loadingOrder.LoadingOrderStatus': { $size: 1 } },
        { 'loadingOrder.LoadingOrderStatus': { $size: 2 } },
        { 'loadingOrder.LoadingOrderStatus': { $size: 3 } },
      ],
      'row.disableDate': { $exists: false },
    });

    console.log('loadingOrdersFinishByOperation====>>', loadingOrdersFinishByOperation);

    if (loadingOrdersFinishByOperation.length == 0) {
      console.log('entro al if de sin orden');
      const operationStatusToPay = await OperationStatus.findOne({ order: 3 });
      console.log('operationStatusToPay====>>>', operationStatusToPay);
      let idOperationStatusToPay = operationStatusToPay?._id;

      await Operation.updateOne(
        {
          _id: idOperation,
        },
        { $set: { statusOperationId: idOperationStatusToPay } }
      );
    }

    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    let transportUnitId = travelUpdate.loadingOrder.assignment.transportUnitId;

    const transportUnitFind = await TransportUnit.findById(transportUnitId);
    console.log('transportUnitFind ==> ', transportUnitFind);
    if (transportUnitFind.drivers.length > 0) {
      const carrierId =
        transportUnitFind.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

      let messagePublish = `Pago realizado de un viaje.`;
      let typeNotification = 'travelPaid';

      const carrierFind = await User.findById(carrierId);
      console.log('carrierFind ==> ', carrierFind);

      let userIdOne = carrierFind?._id || null;
      console.log('userIdOne===>>', userIdOne);

      const token = carrierFind?.device?.pushToken || null;
      console.log('token ==> ', token);

      let dataBody = {
        userId: userIdOne,
        tokenUser: token,
        message: messagePublish,
        type: typeNotification,
        travelId: travelId,
      };

      let newNotification = new Notification(dataBody);
      let notificationSaved = await newNotification.save();
      console.log('notificationSaved===>>', notificationSaved);

      // const messagePublish = `Pago realizado de un viaje.`;
      // let payloadNotification = {
      //   default: 'default',
      //   GCM: {
      //     notification: {
      //       title: 'DeltaX',
      //       body: messagePublish,
      //       sound: 'default',
      //     },
      //     data: {
      //       travelId,
      //       type: 'travelPaid',
      //     },
      //   },
      // };
      // payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
      // payloadNotification = JSON.stringify(payloadNotification);
      // const params_sns = {
      //   TargetArn: token,
      //   Message: payloadNotification,
      //   MessageStructure: 'json',
      // };
      // const publishText = await sns_new
      //   .publish(params_sns)
      //   .promise()
      //   .then(
      //     function (data) {
      //       /* process the data */
      //       console.log('dataaa', data);
      //       return data;
      //     },
      //     function (error) {
      //       console.log('errorrrr', error);
      //       return error;
      //       /* handle the error */
      //     }
      //   );
      // console.log('publishText => ', publishText);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Orden de carga pagada exitosamente.',
        travel: travelUpdate,
      }),
    };
  } catch (err) {
    console.log('err ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    let updateDate = moment().format();
    console.log('updateDate ==> ', updateDate);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const travelId = event?.pathParameters?.travelId || '';
    console.log('travelId===>>>', travelId);

    let travelUpdate = await Travel.updateOne(
      { _id: travelId },
      {
        $set: { ...body, 'row.updateDate': updateDate },
      }
    );
    console.log('travelUpdate===>>>', travelUpdate);
    if (travelUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Oportunidad actualizada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

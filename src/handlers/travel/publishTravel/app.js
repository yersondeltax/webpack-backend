var AWS = require('aws-sdk');
const moment = require('moment');
// var lambda = new AWS.Lambda({ region: 'us-east-1' });
var stepFunctions = new AWS.StepFunctions();
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const stateMachineArn = process?.env?.stateMachineArn || '';
    // console.log('entro0000000');
    // console.log('event ==> ', event);
    // const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    // console.log('Body ==> ', body);
    // const { publish } = body;
    const travelId = event.pathParameters.travelId;
    console.log('entrooooo');
    console.log('event====>>', event);
    let invoke = event.invoke || false;
    let body;
    let publish;
    if (invoke) {
      console.log('entro en verda');
      body = event.body;
      console.log('body====>>', body);
      publish = body.publish;
      console.log('publish====>>', publish);
    } else {
      console.log('entro en  false');
      body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
      publish = body.publish;
    }

    await dbConnection(process.env.dataBaseConnection);
    let message = 'Publicado correctamente';
    let publishDate = moment().format();
    let travel = await Travel.findById(travelId);
    console.log('travel====>>>', travel);
    let arrayTravelStatus = travel.travelstatus;
    console.log('arrayTravelStatus====>>>', arrayTravelStatus);
    let news = 0;
    let type = 'travel';
    if (publish) {
      const payload = JSON.stringify({ travelId, news, type });
      // let params = {
      //   FunctionName: 'travel-notifyPublish-c7dedNURMXDe',
      //   InvocationType: 'RequestResponse',
      //   Payload: payload,
      // };

      //========Producion =====
      // let params = {
      //   stateMachineArn:
      //     'arn:aws:states:us-east-1:879323694935:stateMachine:notifyPublishStateMachine-MI0e1C2LuRB5',
      //   input: payload,
      // };
      // ====== staging ======
      // let params = {
      //   stateMachineArn:
      //     'arn:aws:states:us-east-1:345062269307:stateMachine:notifyPublishStateMachine-ayLxMACz2i6S',
      //   input: payload,
      // };

      let params = {
        stateMachineArn: stateMachineArn,
        input: payload,
      };
      // await lambda.invoke(params).promise();
      await stepFunctions.startExecution(params).promise();

      console.log('longuitud======>>', arrayTravelStatus.length);
      if (arrayTravelStatus.length < 2) {
        console.log('entro al if de leng');
        let dataStatus = {
          statusId: '60be755ce0f3570008db72d4',
          name: 'Publicada',
          date: publishDate,
          order: 2,
        };
        arrayTravelStatus.push(dataStatus);
      }
    } else {
      publishDate = null;
      message = 'Publicación cancelada correctamente';
    }
    console.log('arrayTravelStatus222222====>>>', arrayTravelStatus);
    await Travel.updateOne(
      { _id: travelId },
      {
        $set: { publish, 'dates.publishDate': publishDate, travelstatus: arrayTravelStatus },
      },
      { upsert: true }
    );

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

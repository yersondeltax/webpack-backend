const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const TransportUnit = require('../../../models/transportUnit');
const Setting = require('../../../models/setting');
const Postulation = require('../../../models/postulation');
const MeasurementUnit = require('../../../models/measurementUnit');
const Company = require('../../../models/company');
const Operation = require('../../../models/operation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;
    const travelId = event.pathParameters.travelId;
    const transportUnit = await TransportUnit.findById(transportUnitId);
    let travel = await Travel.findById(travelId);
    travel = travel._doc;
    let newTravel = {};
    const setting = await Setting.find();

    // for (let index = 0; index < travels.length; index++) {
    // const travel = travels[index]._doc;
    // const route = await Route.findById(travel.routeId);
    let typeCurrencyOfferedId = travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
    const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
    let operationId = travel?.operationId || '';
    const operation = await Operation.findById(operationId);
    let companyOperatorId = operation?.companyOperatorId || '';
    const companyData = await Company.findById(companyOperatorId);
    const postulation = await Postulation.find({
      travelId: travel._id,
      transportUnitId: transportUnitId,
      'account.enable': 1,
    });
    let flag = true;
    if (travel.features) {
      for (let j = 0; j < travel.features.length; j++) {
        const feature = travel.features[j];
        const newFeature = {
          name: feature.name,
          featuresTransportUnitId: feature.featuresTransportUnitId,
          valueQualitative: feature.valueQualitative || '',
          valueQuantitative: feature.valueQuantitative || 0,
        };
        const found = transportUnit.features.find((featureTU) => {
          const newFeatureTU = {
            name: featureTU.name,
            featuresTransportUnitId: featureTU.featuresTransportUnitId,
            valueQualitative: featureTU.valueQualitative || '',
            valueQuantitative:
              featureTU.valueQuantitative <= newFeature.valueQuantitative
                ? featureTU.valueQuantitative
                : newFeature.valueQuantitative || 0,
          };
          return JSON.stringify(newFeatureTU) === JSON.stringify(newFeature);
        });
        if (!found) {
          if (setting[0].featureRestriction) {
            flag = false;
            break;
          } else {
            travel.features[j] = { ...feature._doc, hashtag: 'warning' };
          }
        }
      }
    }
    if (flag) {
      newTravel = {
        ...travel,
        // route,
        typeCurrencyOffered,
        postulation,
        company: companyData,
      };
    }
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travel: newTravel,
      }),
    };
  } catch (err) {
    console.log('ERR ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

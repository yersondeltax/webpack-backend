var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let funcionPublicTravel = process.env.funcionPublicTravel;
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    let dateTime = moment().format();
    let arrayTravelStatus = [];
    let dataStatus = {
      statusId: '60be7548e0f3570008db72d3',
      name: 'Creada',
      date: dateTime,
      order: 1,
    };
    arrayTravelStatus.push(dataStatus);

    let data = { ...body, travelstatus: arrayTravelStatus };
    const newTravel = new Travel(data);
    const travelSaved = await newTravel.save();

    let travelId = travelSaved._id;

    ////// invocar a otra lambda  ///////

    console.log('aca empieza');

    let payload = JSON.stringify({
      invoke: true,
      pathParameters: {
        travelId: travelId,
      },
      body: {
        publish: true,
      },
    });
    console.log('payload', payload);

    let params = {
      // FunctionName: 'travel-publishTravel-MviwMIfvDlUm',
      FunctionName: funcionPublicTravel,
      InvocationType: 'RequestResponse',
      // InvocationType='Event',
      Payload: payload,
    };

    let dataResponse = await lambda.invoke(params).promise();
    console.log('dataResponse=====>>', JSON.parse(dataResponse.Payload));
    dataResponse = JSON.parse(dataResponse.Payload);
    console.log('statusCode===>>', dataResponse.statusCode);
    let objectReturn;
    if (dataResponse.statusCode == 200) {
      console.log('entro a true');
      delete travelSaved._doc.publish;
      objectReturn = { ...travelSaved._doc, publish: true };
    } else {
      objectReturn = travelSaved._doc;
    }

    let operationId = objectReturn.operationId;

    await Operation.updateOne(
      {
        _id: operationId,
      },
      { $set: { statusOperationId: '60ba328058236f0009d6f6b2' } }
    );

    /////// termina de invocar /////////

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Oportunidad de carga creada exitosamente.',
        travel: objectReturn,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

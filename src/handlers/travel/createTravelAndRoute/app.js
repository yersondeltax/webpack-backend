const axios = require('axios');

var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const Operation = require('../../../models/operation');
const TravelStatus = require('../../../models/travelStatus');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
const key = 'AIzaSyAE8QygNNc0lhFs5oY0KtIJZoR17LDSJWM';

exports.lambdaHandler = async (event, context) => {
  await dbConnection(process.env.dataBaseConnection);

  // const session = await Route.startSession();
  // session.startTransaction();

  try {
    let funcionPublicTravel = process.env.funcionPublicTravel;
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    // const opts = { session };
    const origin = body.origin || {};
    const destination = body.destination || {};
    const checkPoints = body.checkPoints || {};
    console.log('destination======>', destination);
    console.log('checkPoints======>', checkPoints);

    // let originPlaceId = origin.PlaceId;
    let cityOriginId = origin.cityOriginId;
    let originDirection = origin.direction;
    // let stateOriginId = origin.stateOriginId;
    // let stateOrigin = origin.stateOrigin;
    // let destinationPlaceId = destination.PlaceId;
    let cityDestinationId = destination.cityDestinationId;
    let directionDestination = destination.directionDestination;
    // let stateDestinationId = destination.stateDestinationId;
    // let stateDestination = destination.stateDestination;

    const routes = await Route.findOne({
      'origin.cityOriginId': cityOriginId,
      'destination.cityDestinationId': cityDestinationId,
    });

    console.log('routes======>', routes);

    if (!routes) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let objectoNew = {};
    let comparation = JSON.stringify(routes) === JSON.stringify(objectoNew);
    if (comparation) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let checkPointsRoute = routes.checkPoints;
    let routesOrigin = routes.origin;
    let routesDestination = routes.destination;

    let latitudeOrigin = checkPoints[0]?.lat || 0;
    let longitudeOrigin = checkPoints[0]?.lng || 0;
    let latitudeDestination = checkPoints[checkPoints.length - 1]?.lat || 0;
    let longitudeDestination = checkPoints[checkPoints.length - 1]?.lng || 0;

    const respondeGooleApi = await axios.get(
      `https://maps.googleapis.com/maps/api/directions/json?origin=${latitudeOrigin},${longitudeOrigin}&destination=${latitudeDestination},${longitudeDestination}&key=${key}`
    );

    let routesGoogle = respondeGooleApi?.data?.routes[0] || [];
    let distance = routesGoogle?.legs[0]?.distance || {};

    let distanceTravel = {
      name: distance?.text || '',
      value: distance?.value || 0,
    };

    let checkPointsIni = [];
    // let checkPointsFin = [];
    // checkPointsIni.push(checkPoints[0]);
    // checkPointsFin.push(checkPoints[checkPoints.length - 1]);
    // checkPointsIni = checkPointsIni.concat(checkPointsRoute);
    // checkPointsIni = checkPointsIni.concat(checkPointsFin);

    checkPointsIni = checkPoints.concat(checkPointsRoute);

    console.log('checkPointsIni======>', checkPointsIni);

    routesOrigin = {
      ...routesOrigin,
      direction: originDirection,
      // stateOriginId: stateOriginId,
      // stateOrigin: stateOrigin,
    };
    console.log('routesOrigin======>', routesOrigin);
    routesDestination = {
      ...routesDestination,
      directionDestination: directionDestination,
      // stateDestinationId: stateDestinationId,
      // stateDestination: stateDestination,
    };
    console.log('routesDestination======>', routesDestination);

    let dataRoute = {
      origin: routesOrigin,
      destination: routesDestination,
      checkPoints: checkPointsIni,
    };

    console.log('dataRoute======>', dataRoute);

    let dateTime = moment().format();
    let arrayTravelStatus = [];

    let travelStatusCreate = await TravelStatus.findOne({ order: 1 });
    console.log('travelStatusCreate===>>>', travelStatusCreate);
    let dataStatus = {
      statusId: travelStatusCreate?._id || '',
      name: travelStatusCreate?.name || '',
      date: dateTime,
      order: travelStatusCreate?.order,
    };

    // let dataStatus = {
    //   statusId: '60be7548e0f3570008db72d3',
    //   name: 'Creada',
    //   date: dateTime,
    //   order: 1,
    // };
    arrayTravelStatus.push(dataStatus);

    let data = {
      ...body,
      route: dataRoute,
      travelstatus: arrayTravelStatus,
      distanceTravel: distanceTravel,
    };

    const newTravel = new Travel(data);
    const travelSaved = await newTravel.save();
    // await session.commitTransaction();
    // session.endSession();

    let travelId = travelSaved._id;

    ////// invocar a otra lambda  ///////

    console.log('aca empieza');

    let payload = JSON.stringify({
      invoke: true,
      pathParameters: {
        travelId: travelId,
      },
      body: {
        publish: true,
      },
    });
    console.log('payload', payload);

    let params = {
      // FunctionName: 'travel-publishTravel-MviwMIfvDlUm',
      FunctionName: funcionPublicTravel,
      InvocationType: 'RequestResponse',
      // InvocationType='Event',
      Payload: payload,
    };

    let dataResponse = await lambda.invoke(params).promise();
    console.log('dataResponse=====>>', JSON.parse(dataResponse.Payload));
    dataResponse = JSON.parse(dataResponse.Payload);
    console.log('statusCode===>>', dataResponse.statusCode);
    let objectReturn;
    if (dataResponse.statusCode == 200) {
      console.log('entro a true');
      delete travelSaved._doc.publish;
      objectReturn = { ...travelSaved._doc, publish: true };
    } else {
      objectReturn = travelSaved._doc;
    }
    /////// termina de invocar /////////

    let operationId = objectReturn.operationId;

    let operationStatusProgres = await OperationStatus.findOne({ order: 2 });
    console.log('operationStatusProgres===>>>', operationStatusProgres);
    let operationStatusProgresId = operationStatusProgres?._id || '';

    await Operation.updateOne(
      {
        _id: operationId,
      },
      { $set: { statusOperationId: operationStatusProgresId } }
    );

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Oportunidad de carga creada exitosamente.',
        travel: objectReturn,
      }),
    };
  } catch (err) {
    // await session.abortTransaction();
    // session.endSession();
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

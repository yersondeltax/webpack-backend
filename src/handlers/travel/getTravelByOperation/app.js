const mongoose = require('mongoose');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const operationId = event.pathParameters.operationId;
    let carrier = null;

    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let search = event?.queryStringParameters?.search;
    console.log('page===>>', page);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let sequence = event?.queryStringParameters?.sequence;
    console.log('sequence===>>', sequence);
    let type = event?.queryStringParameters?.type || 'false';
    console.log('type===>>', type);
    var pageNumber;
    var sizeNumber;
    let totalPage = 1;
    let travels = [];
    let travelFinish = [];
    let travelStatusName = '';
    console.log('travels=====>>>', travels);
    let objectSearch;
    if (search) {
      objectSearch = {
        $match: {
          'transport.plate': new RegExp(search, 'i'),
          // 'row.disableDate': { $exists: false },
          operationId: mongoose.Types.ObjectId(`${operationId}`),
        },
      };
    } else {
      objectSearch = {
        $match: {
          // 'row.disableDate': { $exists: false },
          operationId: mongoose.Types.ObjectId(`${operationId}`),
        },
      };
    }
    let objectOrder;
    if (order && sequence) {
      objectOrder = {
        $sort: {},
      };
      if (sequence == 'asc') {
        objectOrder.$sort[`${order}`] = 1;
      } else {
        objectOrder.$sort[`${order}`] = -1;
      }
    }
    let fullTravel = await Travel.find({
      // 'row.disableDate': { $exists: false },
      operationId,
    }).countDocuments();
    if (fullTravel > 0) {
      if (page || size) {
        console.log('entro por verdadero');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
        // travels = await Travel.find({ 'row.disableDate': { $exists: false }, operationId })
        //   .limit(sizeNumber)
        //   .skip(pageNumber * sizeNumber)
        //   .sort({ 'row.createDate': -1 });
        let skipNumber = pageNumber * sizeNumber;
        let limitNumber = sizeNumber;
        travels = await Travel.aggregate([
          {
            $lookup: {
              from: 'transportunits',
              localField: 'loadingOrder.assignment.transportUnitId',
              foreignField: '_id',
              as: 'transport',
            },
          },
          objectSearch,
          objectOrder,
          {
            $skip: skipNumber,
          },
          {
            $limit: limitNumber,
          },
        ]);
        // let total = await Travel.find({ 'row.disableDate': { $exists: false }, operationId }).countDocuments();
        let total = await Travel.aggregate([
          {
            $lookup: {
              from: 'transportunits',
              localField: 'loadingOrder.assignment.transportUnitId',
              foreignField: '_id',
              as: 'transport',
            },
          },
          objectSearch,
          {
            $count: 'cant',
          },
        ]);
        if (travels.length > 1) {
          let newCant = total[0].cant;
          total = parseInt(newCant, 10);
          totalPage = total / sizeNumber;
          totalPage = Math.ceil(totalPage);
        }
      } else {
        console.log('entro por falso');
        // travels = await Travel.find({ 'row.disableDate': { $exists: false }, operationId }).sort({
        //   'row.createDate': -1,
        // });
        travels = await Travel.aggregate([
          {
            $lookup: {
              from: 'transportunits',
              localField: 'loadingOrder.assignment.transportUnitId',
              foreignField: '_id',
              as: 'transport',
            },
          },
          objectSearch,
          objectOrder,
          // {
          //   $count: 'nuevo',
          // },
          // {
          //   $skip: 4,
          // },
          // {
          //   $limit: 2,
          // },
        ]);
      }
      let nameStatus;

      for (let index = 0; index < travels.length; index++) {
        console.log('entrooo en el for');
        const travel = travels[index];
        let flag = false;
        console.log('travel=====>>>', travel);
        // const route = await Route.findById(travel.routeId);
        if (travel.loadingOrder) {
          console.log('entrooo en el primer if');
          if (travel.loadingOrder.LoadingOrderStatus.length > 0) {
            console.log('entrooo en el segundo if');
            // travel.loadingOrder.assignment.transportUnitId
            const transportUnitTemp = await TransportUnit.findOne({
              _id: travel.loadingOrder.assignment.transportUnitId,
            }).exec();
            carrier = await User.findOne({
              _id: transportUnitTemp.drivers[transportUnitTemp.drivers.length - 1].userId,
            });
            flag = true;
            nameStatus =
              travel.loadingOrder.LoadingOrderStatus[
                travel.loadingOrder.LoadingOrderStatus.length - 1
              ].name;
          } else {
            nameStatus = travel.travelstatus[travel.travelstatus.length - 1].name;
          }
        }
        if (type == 'true') {
          if (flag == true) {
            let objectClient = {
              ...travel,
              carrier,
              travelStatusName: nameStatus,
            };
            travelFinish.push(objectClient);
          }
        } else {
          let objectFms = {
            ...travel,
            carrier,
            travelStatusName: nameStatus,
          };
          travelFinish.push(objectFms);
        }
        // travels[index] = {
        //   ...travel,
        //   // route: { origin: route.origin, destination: route.destination },
        //   carrier,
        //   travelStatusName: nameStatus,
        // };
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        travels: travelFinish,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

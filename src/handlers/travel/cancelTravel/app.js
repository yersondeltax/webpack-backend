const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const TravelStatus = require('../../../models/travelStatus');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    let disableDate = moment().format();
    console.log('disableDate ==> ', disableDate);
    const travelId = event?.pathParameters?.travelId || '';
    console.log('travelId===>>>', travelId);
    let ordetravelStatus = 5;
    console.log('ordetravelStatus===>>>', ordetravelStatus);
    let travelStatusCancel = await TravelStatus.findOne({ order: ordetravelStatus });
    console.log('travelStatusCancel===>>>', travelStatusCancel);
    let dataCancel = {
      statusId: travelStatusCancel?._id || '',
      name: travelStatusCancel?.name || '',
      date: disableDate,
      order: travelStatusCancel?.order,
    };

    let travelCancel = await Travel.findOneAndUpdate(
      {
        _id: travelId,
      },
      {
        $set: { 'row.disableDate': disableDate, publish: false },
        $push: { travelstatus: dataCancel },
      },
      { new: true }
    );
    console.log('travelCancel===>>', travelCancel);
    let objectLoadingOrder = travelCancel?.loadingOrder || {};
    let objectoNuevo = {};
    let nuevo = JSON.stringify(objectLoadingOrder) === JSON.stringify(objectoNuevo);
    if (nuevo == false) {
      console.log('entrooo al primer if');
      let objectLoadingOrderAssignment = travelCancel?.loadingOrder?.assignment || {};
      let nuevo22 = JSON.stringify(objectLoadingOrderAssignment) === JSON.stringify(objectoNuevo);
      if (nuevo22 == false) {
        console.log('entrooo al 2 if');
        let loadingOrderId = travelCancel?.loadingOrder?.loadingOrderId || '';
        console.log('loadingOrderId=====>', loadingOrderId);
        let assignment = 0;
        let loadingOrderStatusCancel = await LoadingOrderStatus.findOne({
          order: ordetravelStatus,
        });
        console.log('loadingOrderStatusCancel====>>', loadingOrderStatusCancel);
        let dataCancelLoadingOrder = {
          statusId: loadingOrderStatusCancel?._id || '',
          name: loadingOrderStatusCancel?.name || '',
          date: disableDate,
          order: loadingOrderStatusCancel?.order,
        };
        console.log('dataCancelLoadingOrder===>>>', dataCancelLoadingOrder);
        travelCancel = await Travel.findOneAndUpdate(
          {
            _id: travelId,
            'loadingOrder.loadingOrderId': loadingOrderId,
          },
          {
            $set: {
              'loadingOrder.assignment.assignmentStatus': 0,
            },
            $push: { 'loadingOrder.LoadingOrderStatus': dataCancelLoadingOrder },
          },
          { new: true }
        );
        console.log('dataLoadingOrder===Cancel>>>', travelCancel);
      }
    }
    let operationTravel = travelCancel.operationId;
    let countTravelsPlanned = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 0 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsPlanned======>>', countTravelsPlanned);

    let countTravelsAssignment = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 1 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsAssignment======>>', countTravelsAssignment);

    let countTravelsProgress = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 2 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsProgress======>>', countTravelsProgress);

    let countTravelsToPay = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 3 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsToPay======>>', countTravelsToPay);

    let countTravelsPaid = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 4 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsPaid======>>', countTravelsPaid);

    let orderOperationStatus = 0;
    let operationStatus;
    let idOperationStatus;
    if (
      (countTravelsPlanned == 0 &&
        countTravelsAssignment == 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0 &&
        countTravelsPaid == 0) ||
      (countTravelsPlanned >= 1 &&
        countTravelsAssignment >= 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0)
    ) {
      orderOperationStatus = 1;
      console.log('orderOperationStatus===>>>', orderOperationStatus);
      operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
      console.log('operationStatus===>>>', operationStatus);
      idOperationStatus = operationStatus?._id;
      console.log('idOperationStatus===>>>', idOperationStatus);
      let operationPlanif = await Operation.updateOne(
        { _id: operationTravel },
        {
          $set: { statusOperationId: idOperationStatus },
        }
      );
      console.log('operationPlanif===>>>', operationPlanif);
    } else {
      if (
        countTravelsPlanned == 0 &&
        countTravelsAssignment == 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0 &&
        countTravelsPaid >= 1
      ) {
        orderOperationStatus = 3;
        console.log('orderOperationStatusPli===>>>', orderOperationStatus);
        operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
        console.log('operationStatusPli===>>>', operationStatus);
        idOperationStatus = operationStatus?._id;
        console.log('idOperationStatusPli===>>>', idOperationStatus);
        let operationPlanif = await Operation.updateOne(
          { _id: operationTravel },
          {
            $set: { statusOperationId: idOperationStatus },
          }
        );
        console.log('operationPli===>>>', operationPlanif);
      } else {
        orderOperationStatus = 2;
        console.log('orderOperationStatusEC===>>>', orderOperationStatus);
        operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
        console.log('operationStatusEC===>>>', operationStatus);
        idOperationStatus = operationStatus?._id;
        console.log('idOperationStatusEC===>>>', idOperationStatus);
        let operationPlanif = await Operation.updateOne(
          { _id: operationTravel },
          {
            $set: { statusOperationId: idOperationStatus },
          }
        );
        console.log('operationEC===>>>', operationPlanif);
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Viaje cancelado.',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Stages = require('../../../models/stages');
const Operation = require('../../../models/operation');
const MeasurementUnit = require('../../../models/measurementUnit');
const Company = require('../../../models/company');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event?.pathParameters?.transportUnitId || '';
    let type = event?.queryStringParameters?.type || 'full';

    let travelPaid = [];
    if (type == 'full' || type == 'paid') {
      travelPaid = await Travel.find({
        // 'loadingOrder.assignment.assignmentStatus': 1,
        'loadingOrder.assignment.transportUnitId': transportUnitId,
        'loadingOrder.LoadingOrderStatus': { $size: 4 },
        'loadingOrder.LoadingOrderStatus.3.order': 4,
      }).sort({ 'loadingOrder.LoadingOrderStatus.3.date': -1 });

      console.log('travelPaid====>>>', travelPaid);
    }

    let travelToPay = [];
    if (type == 'full' || type == 'topaid') {
      travelToPay = await Travel.find({
        // 'loadingOrder.assignment.assignmentStatus': 1,
        'loadingOrder.assignment.transportUnitId': transportUnitId,
        'loadingOrder.LoadingOrderStatus': { $size: 3 },
        'loadingOrder.LoadingOrderStatus.2.order': 3,
      }).sort({ 'loadingOrder.LoadingOrderStatus.2.date': -1 });

      console.log('travelToPay====>>>', travelToPay);
    }

    let travelFull = travelToPay.concat(travelPaid);
    let newTravels = [];
    for (const travelId of travelFull) {
      let typeCurrencyOfferedId =
        travelId?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      let operationId = travelId?.operationId || '';
      const operation = await Operation.findById(operationId);
      let companyOperatorId = operation?.companyOperatorId || '';
      const companyData = await Company.findById(companyOperatorId);
      newTravels.push({
        ...travelId._doc,
        // route,
        typeCurrencyOffered,
        company: companyData,
      });
    }

    console.log('newTravels====>>>', newTravels);
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travels: newTravels,
      }),
    };
  } catch (err) {
    console.log('err ==>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const axios = require('axios');

var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });
const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

const moment = require('moment');
const mongoose = require('mongoose');

const { dbConnection } = require('../../../database/config');

const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const TravelStatus = require('../../../models/travelStatus');
const OperationStatus = require('../../../models/operationStatus');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const Notification = require('../../../models/notification');
const Postulation = require('../../../models/postulation');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

const key = 'AIzaSyAE8QygNNc0lhFs5oY0KtIJZoR17LDSJWM';

exports.lambdaHandler = async (event, context) => {
  await dbConnection(process.env.dataBaseConnection);

  // const session = await Route.startSession();
  // session.startTransaction();

  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    let travelFms = body?.travelFms || false;
    const origin = body?.origin || {};
    const destination = body?.destination || {};
    const checkPoints = body?.checkPoints || {};
    let loadingOrder = body?.loadingOrder || {};
    const transportUnitId = body?.loadingOrder?.assignment?.transportUnitId;
    let initialTravel = body?.initialTravel || null;

    delete body.loadingOrder;

    console.log('destination======>', destination);
    console.log('checkPoints======>', checkPoints);

    let cityOriginId = origin.cityOriginId;
    let originDirection = origin.direction;
    let cityDestinationId = destination.cityDestinationId;
    let directionDestination = destination.directionDestination;

    let nameCityRoute = cityOriginId;
    let nameCityRouteDestination = cityDestinationId;
    let linkApp = '';
    // let nameCity = cityOriginId;
    // const arrayNameCity = nameCity.split(',');
    // console.log('arrayNameCity===>>', arrayNameCity);
    // let nameCityRoute = arrayNameCity[arrayNameCity.length - 1] || '';
    // nameCityRoute = nameCityRoute.trim();
    // console.log('nameCityRoute====>>', nameCityRoute);

    // let nameCityDestination = cityDestinationId;
    // const arrayNameCityDestination = nameCityDestination.split(',');
    // console.log('arrayNameCityDestination===>>', arrayNameCityDestination);
    // let nameCityRouteDestination =
    //   arrayNameCityDestination[arrayNameCityDestination.length - 1] || '';
    // nameCityRouteDestination = nameCityRouteDestination.trim();
    // console.log('nameCityRouteDestination====>>', nameCityRouteDestination);

    if (initialTravel != null) {
      const cancelPostulation = await Postulation.findOneAndUpdate(
        { travelId: initialTravel, transportUnitId: transportUnitId, 'account.enable': 1 },
        {
          $set: { 'account.enable': 0 },
        },
        { new: true }
      );
    }

    const routes = await Route.findOne({
      'origin.cityOrigin': nameCityRoute,
      'destination.cityDestination': nameCityRouteDestination,
    });

    console.log('routes======>', routes);

    if (!routes) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let objectoNew = {};
    let comparation = JSON.stringify(routes) === JSON.stringify(objectoNew);
    if (comparation) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let checkPointsRoute = routes.checkPoints;
    let routesOrigin = routes.origin;
    let routesDestination = routes.destination;

    let latitudeOrigin = checkPoints[0]?.lat || 0;
    let longitudeOrigin = checkPoints[0]?.lng || 0;
    let latitudeDestination = checkPoints[checkPoints.length - 1]?.lat || 0;
    let longitudeDestination = checkPoints[checkPoints.length - 1]?.lng || 0;

    const respondeGooleApi = await axios.get(
      `https://maps.googleapis.com/maps/api/directions/json?origin=${latitudeOrigin},${longitudeOrigin}&destination=${latitudeDestination},${longitudeDestination}&key=${key}`
    );

    let routesGoogle = respondeGooleApi?.data?.routes[0] || [];
    let distance = routesGoogle?.legs[0]?.distance || {};

    let distanceTravel = {
      name: distance?.text || '',
      value: distance?.value || 0,
    };

    let checkPointsIni = [];
    checkPointsIni = checkPoints.concat(checkPointsRoute);

    console.log('checkPointsIni======>', checkPointsIni);

    routesOrigin = {
      ...routesOrigin,
      direction: originDirection,
    };
    console.log('routesOrigin======>', routesOrigin);
    routesDestination = {
      ...routesDestination,
      directionDestination: directionDestination,
    };
    console.log('routesDestination======>', routesDestination);

    let dataRoute = {
      origin: routesOrigin,
      destination: routesDestination,
      checkPoints: checkPointsIni,
    };

    console.log('dataRoute======>', dataRoute);

    let dateTime = moment().format();
    let arrayTravelStatus = [];
    let travelStatusCreate = await TravelStatus.findOne({ order: 1 });
    console.log('travelStatusCreate===>>>', travelStatusCreate);
    let dataStatus = {
      statusId: travelStatusCreate?._id || '',
      name: travelStatusCreate?.name || '',
      date: dateTime,
      order: travelStatusCreate?.order,
    };
    // let dataStatus = {
    //   statusId: '60be7548e0f3570008db72d3',
    //   name: 'Creada',
    //   date: dateTime,
    //   order: 1,
    // };
    arrayTravelStatus.push(dataStatus);

    //////////////// objeto loading order ///////

    let date = moment().format();
    let loadingOrderStatusAsignmen = await LoadingOrderStatus.findOne({
      order: 1,
    });
    let status = {
      statusId: loadingOrderStatusAsignmen?._id || '',
      name: loadingOrderStatusAsignmen?.name || '',
      date: date,
      order: loadingOrderStatusAsignmen?.order,
    };
    console.log('status===>>>', status);
    // const status = {
    //   statusId: '60b125d93e9ef60008aa9c95',
    //   name: 'Asignada',
    //   order: 1,
    //   date,
    // };

    let weightUnit = body.weightUnit || {};
    let volumeUnit = body.volumeUnit || {};

    let prueba = body?.data?.prueba || 'esto es la prueba';
    console.log('prueba====>>', prueba);

    let typeCurrencyFreightId =
      body?.freightValues?.freightOffered?.typeCurrencyOfferedId || '00000';
    let freightValue = body?.freightValues?.freightOffered?.value || 0;
    let invoice = body?.freightValues?.freightOffered?.invoice || false;

    let carrierFreight = {
      typeCurrencyFreightId,
      freightValue,
      invoice,
    };

    const loadingOrderId = mongoose.Types.ObjectId();
    if (travelFms == false) {
      loadingOrder = {
        ...loadingOrder,
        weightUnit,
        volumeUnit,
        carrierFreight,
        loadingOrderId,
        LoadingOrderStatus: [status],
      };
    } else {
      loadingOrder = {
        ...loadingOrder,
        weightUnit,
        volumeUnit,
        carrierFreight,
        loadingOrderId,
      };
    }

    // const data = { ...body, LoadingOrderStatus: [status], loadingOrderId };

    let keyLink = process?.env?.keyLink || '';
    let domainUriPrefix = process?.env?.domainUriPrefix || '';
    let dynamicLink = process?.env?.dynamicLink || '';
    let nameAppInfo = process?.env?.nameAppInfo || '';

    const respondeGooleApiLink = await axios({
      method: 'post',
      url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${keyLink}`,
      data: {
        dynamicLinkInfo: {
          domainUriPrefix: `${domainUriPrefix}`,
          link: `${dynamicLink}/operation_page`,
          androidInfo: {
            androidPackageName: `${nameAppInfo}`,
          },
          iosInfo: {
            iosBundleId: `${nameAppInfo}`,
          },
        },
      },
    });

    console.log('respondeGooleApiLink====>>>', respondeGooleApiLink);
    console.log('respondeGooleApiLink.data====>>>', respondeGooleApiLink?.data);

    linkApp = respondeGooleApiLink?.data?.shortLink || '';

    let data = {
      ...body,
      route: dataRoute,
      travelstatus: arrayTravelStatus,
      loadingOrder: loadingOrder,
      distanceTravel: distanceTravel,
      linkApp: linkApp,
    };
    console.log('data ==> ', data);

    const newTravel = new Travel(data);
    const travelSaved = await newTravel.save();

    let travelId = travelSaved._id;

    let operationId = travelSaved.operationId;

    let operationStatusProgres = await OperationStatus.findOne({ order: 2 });
    console.log('operationStatusProgres===>>>', operationStatusProgres);
    let operationStatusProgresId = operationStatusProgres?._id || '';

    let operationData = await Operation.findOneAndUpdate(
      {
        _id: operationId,
      },
      { $set: { statusOperationId: operationStatusProgresId } },
      { new: true }
    );

    if (travelFms == false) {
      //////////////////// notificar al transportista /////////

      // let operationId = travelUpdate.operationId;

      // const operationTravel = await Operation.findById(operationId);

      const transportUnitFind = await TransportUnit.findById(transportUnitId);
      console.log('transportUnitFind ==> ', transportUnitFind);

      const carrierId =
        transportUnitFind?.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

      let messagePublish = `Se te ha asignado una orden de carga, ingresa para ver más detalles.`;
      let typeNotification = 'AssignedLoadOrder';

      const carrierFind = await User.findById(carrierId);
      console.log('carrierFind ==> ', carrierFind);

      let userIdOne = carrierFind?._id || null;
      console.log('userIdOne===>>', userIdOne);

      const token = carrierFind?.device?.pushToken || null;
      console.log('token ==> ', token);

      let dataBody = {
        userId: userIdOne,
        tokenUser: token,
        message: messagePublish,
        type: typeNotification,
        travelId: travelId,
      };

      let newNotification = new Notification(dataBody);
      let notificationSaved = await newNotification.save();
      console.log('notificationSaved===>>', notificationSaved);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Orden de carga creada exitosamente.',
        operation: operationData,
        loadingOrderId: loadingOrderId,
        travel: travelSaved,
      }),
    };
  } catch (err) {
    // await session.abortTransaction();
    // session.endSession();
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const axios = require('axios');

var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const Operation = require('../../../models/operation');
const TravelStatus = require('../../../models/travelStatus');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
const key = 'AIzaSyAE8QygNNc0lhFs5oY0KtIJZoR17LDSJWM';

exports.lambdaHandler = async (event, context) => {
  console.log('event======>>>>', event);
  await dbConnection(process.env.dataBaseConnection);

  // const session = await Route.startSession();
  // session.startTransaction();

  try {
    let funcionPublicTravel = process.env.funcionPublicTravel;
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body======>>>>', body);
    // const opts = { session };
    const origin = body?.origin || {};
    const destination = body?.destination || {};
    const checkPoints = body?.checkPoints || {};
    console.log('origin======>', origin);
    console.log('destination======>', destination);
    console.log('checkPoints======>', checkPoints);
    delete body?.origin;
    delete body?.destination;
    delete body?.checkPoints;

    // let originPlaceId = origin.PlaceId;
    let cityOriginId = origin.cityOriginId;
    let originDirection = origin.direction;
    // let stateOriginId = origin.stateOriginId;
    // let stateOrigin = origin.stateOrigin;
    // let destinationPlaceId = destination.PlaceId;
    let cityDestinationId = destination.cityDestinationId;
    let directionDestination = destination.directionDestination;
    // let stateDestinationId = destination.stateDestinationId;
    // let stateDestination = destination.stateDestination;

    let nameCityRoute = cityOriginId;
    let nameCityRouteDestination = cityDestinationId;
    // let nameCity = cityOriginId;
    // const arrayNameCity = nameCity.split(',');
    // console.log('arrayNameCity===>>', arrayNameCity);
    // let nameCityRoute = arrayNameCity[arrayNameCity.length - 1] || '';
    // nameCityRoute = nameCityRoute.trim();
    // console.log('nameCityRoute====>>', nameCityRoute);

    // let nameCityDestination = cityDestinationId;
    // const arrayNameCityDestination = nameCityDestination.split(',');
    // console.log('arrayNameCityDestination===>>', arrayNameCityDestination);
    // let nameCityRouteDestination =
    //   arrayNameCityDestination[arrayNameCityDestination.length - 1] || '';
    // nameCityRouteDestination = nameCityRouteDestination.trim();
    // console.log('nameCityRouteDestination====>>', nameCityRouteDestination);

    const routes = await Route.findOne({
      'origin.cityOrigin': nameCityRoute,
      'destination.cityDestination': nameCityRouteDestination,
    });

    console.log('routes======>', routes);

    if (!routes) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let objectoNew = {};
    let comparation = JSON.stringify(routes) === JSON.stringify(objectoNew);
    if (comparation) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se encontro la ruta para el origen y destino seleccionado.',
        }),
      };
    }

    let checkPointsRoute = routes.checkPoints;
    let routesOrigin = routes.origin;
    let routesDestination = routes.destination;
    console.log('routesOriginroutesOrigin====', routesOrigin);
    console.log('routesDestinationroutesDestination====', routesDestination);

    let latitudeOrigin = checkPoints[0]?.lat || 0;
    let longitudeOrigin = checkPoints[0]?.lng || 0;
    let latitudeDestination = checkPoints[checkPoints.length - 1]?.lat || 0;
    let longitudeDestination = checkPoints[checkPoints.length - 1]?.lng || 0;

    const respondeGooleApi = await axios.get(
      `https://maps.googleapis.com/maps/api/directions/json?origin=${latitudeOrigin},${longitudeOrigin}&destination=${latitudeDestination},${longitudeDestination}&key=${key}`
    );

    let routesGoogle = respondeGooleApi?.data?.routes[0] || [];
    let distance = routesGoogle?.legs[0]?.distance || {};

    let distanceTravel = {
      name: distance?.text || '',
      value: distance?.value || 0,
    };

    console.log('distanceTravel======>>>', distanceTravel);

    let checkPointsIni = [];
    checkPointsIni = checkPoints.concat(checkPointsRoute);

    console.log('checkPointsIni======>', checkPointsIni);

    routesOrigin = {
      cityOriginId: routesOrigin.cityOriginId,
      cityOrigin: routesOrigin.cityOrigin,
      // ...routesOrigin,
      direction: originDirection,
      // stateOriginId: stateOriginId,
      // stateOrigin: stateOrigin,
    };
    console.log('routesOrigin======>', routesOrigin);
    routesDestination = {
      cityDestinationId: routesDestination.cityDestinationId,
      cityDestination: routesDestination.cityDestination,
      // ...routesDestination,
      directionDestination: directionDestination,
      // stateDestinationId: stateDestinationId,
      // stateDestination: stateDestination,
    };
    console.log('routesDestination======>', routesDestination);

    let dataRoute = {
      origin: routesOrigin,
      destination: routesDestination,
      checkPoints: checkPointsIni,
    };

    console.log('dataRoute======>', dataRoute);

    let dateTime = moment().format();
    console.log('dateTime======>', dateTime);
    let arrayTravelStatus = [];
    let travelStatusCreate = await TravelStatus.findOne({ order: 1 });
    console.log('travelStatusCreate===>>>', travelStatusCreate);
    let dataStatus = {
      statusId: travelStatusCreate?._id || '',
      name: travelStatusCreate?.name || '',
      date: dateTime,
      order: travelStatusCreate?.order,
    };
    // let dataStatus = {
    //   statusId: '60be7548e0f3570008db72d3',
    //   name: 'Creada',
    //   date: dateTime,
    //   order: 1,
    // };
    arrayTravelStatus.push(dataStatus);

    let data = {
      ...body,
      route: dataRoute,
      travelstatus: arrayTravelStatus,
      distanceTravel: distanceTravel,
    };
    console.log('data======>', data);

    console.log('accaaaaaaaaaaaaaa');
    const newTravel = new Travel(data);
    console.log('newTravel=====>>', newTravel);
    const travelSaved = await newTravel.save();
    console.log('travelSaved=====>>', travelSaved);
    // await session.commitTransaction();
    // session.endSession();

    let travelId = travelSaved._id;

    ////// invocar a otra lambda  ///////

    console.log('aca empieza');

    let payload = JSON.stringify({
      invoke: true,
      pathParameters: {
        travelId: travelId,
      },
      body: {
        publish: true,
      },
    });
    console.log('payload', payload);

    let params = {
      // FunctionName: 'travel-publishTravel-MviwMIfvDlUm',
      FunctionName: funcionPublicTravel,
      InvocationType: 'RequestResponse',
      // InvocationType='Event',
      Payload: payload,
    };

    let dataResponse = await lambda.invoke(params).promise();
    console.log('dataResponse=====>>', JSON.parse(dataResponse.Payload));
    dataResponse = JSON.parse(dataResponse.Payload);
    console.log('statusCode===>>', dataResponse.statusCode);
    let objectReturn;
    if (dataResponse.statusCode == 200) {
      console.log('entro a true');
      delete travelSaved._doc.publish;
      objectReturn = { ...travelSaved._doc, publish: true };
    } else {
      objectReturn = travelSaved._doc;
    }
    /////// termina de invocar /////////

    let operationId = objectReturn.operationId;

    let operationStatusProgres = await OperationStatus.findOne({ order: 2 });
    console.log('operationStatusProgres===>>>', operationStatusProgres);
    let operationStatusProgresId = operationStatusProgres?._id || '';

    await Operation.updateOne(
      {
        _id: operationId,
      },
      { $set: { statusOperationId: operationStatusProgresId } }
    );

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Oportunidad de carga creada exitosamente.',
        travel: objectReturn,
      }),
    };
  } catch (err) {
    // await session.abortTransaction();
    // session.endSession();
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

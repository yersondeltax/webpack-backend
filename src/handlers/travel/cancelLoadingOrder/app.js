var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const OperationStatus = require('../../../models/operationStatus');
const Operation = require('../../../models/operation');
const TransportUnit = require('../../../models/transportUnit');
const User = require('../../.../../../models/user');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo11');
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    let disableDate = moment().format();
    console.log('disableDate ==> ', disableDate);
    const loadingOrderId = event?.pathParameters?.loadingOrderId || '';
    console.log('loadingOrderId===>>>', loadingOrderId);
    let ordetravelStatus = 5;
    console.log('ordetravelStatus===>>>', ordetravelStatus);

    let travel = await Travel.findOne({ 'loadingOrder.loadingOrderId': loadingOrderId });
    let travelId = travel._id;
    let assignment = 0;
    let loadingOrderStatusCancel = await LoadingOrderStatus.findOne({
      order: ordetravelStatus,
    });
    let dataCancelLoadingOrder = {
      statusId: loadingOrderStatusCancel?._id || '',
      name: loadingOrderStatusCancel?.name || '',
      date: disableDate,
      order: loadingOrderStatusCancel?.order,
    };
    console.log('dataCancelLoadingOrder===>>>', dataCancelLoadingOrder);
    let travelCancel = await Travel.findOneAndUpdate(
      {
        _id: travelId,
        'loadingOrder.loadingOrderId': loadingOrderId,
      },
      {
        $set: {
          'loadingOrder.assignment.assignmentStatus': 0,
          'row.disableDate': disableDate,
          publish: false,
        },
        $push: { 'loadingOrder.LoadingOrderStatus': dataCancelLoadingOrder },
      },
      { new: true }
    );
    console.log('dataLoadingOrder===Cancel>>>', travelCancel);

    let operationTravel = travelCancel.operationId;
    // let operationTravel = travelCancel.operationId;
    let countTravelsPlanned = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 0 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsPlanned======>>', countTravelsPlanned);

    let countTravelsAssignment = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 1 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsAssignment======>>', countTravelsAssignment);

    let countTravelsProgress = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 2 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsProgress======>>', countTravelsProgress);

    let countTravelsToPay = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 3 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsToPay======>>', countTravelsToPay);

    let countTravelsPaid = await Travel.find({
      operationId: operationTravel,
      'loadingOrder.LoadingOrderStatus': { $size: 4 },
      'row.disableDate': { $exists: false },
    }).countDocuments();
    console.log('countTravelsPaid======>>', countTravelsPaid);

    let orderOperationStatus = 0;
    let operationStatus;
    let idOperationStatus;
    if (
      (countTravelsPlanned == 0 &&
        countTravelsAssignment == 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0 &&
        countTravelsPaid == 0) ||
      (countTravelsPlanned >= 1 &&
        countTravelsAssignment == 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0)
    ) {
      orderOperationStatus = 1;
      console.log('orderOperationStatus===>>>', orderOperationStatus);
      operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
      console.log('operationStatus===>>>', operationStatus);
      idOperationStatus = operationStatus?._id;
      console.log('idOperationStatus===>>>', idOperationStatus);
      let operationPlanif = await Operation.updateOne(
        { _id: operationTravel },
        {
          $set: { statusOperationId: idOperationStatus },
        }
      );
      console.log('operationPlanif===>>>', operationPlanif);
    } else {
      if (
        countTravelsPlanned == 0 &&
        countTravelsAssignment == 0 &&
        countTravelsProgress == 0 &&
        countTravelsToPay == 0 &&
        countTravelsPaid >= 1
      ) {
        orderOperationStatus = 3;
        console.log('orderOperationStatusPli===>>>', orderOperationStatus);
        operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
        console.log('operationStatusPli===>>>', operationStatus);
        idOperationStatus = operationStatus?._id;
        console.log('idOperationStatusPli===>>>', idOperationStatus);
        let operationPlanif = await Operation.updateOne(
          { _id: operationTravel },
          {
            $set: { statusOperationId: idOperationStatus },
          }
        );
        console.log('operationPli===>>>', operationPlanif);
      } else {
        orderOperationStatus = 2;
        console.log('orderOperationStatusEC===>>>', orderOperationStatus);
        operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
        console.log('operationStatusEC===>>>', operationStatus);
        idOperationStatus = operationStatus?._id;
        console.log('idOperationStatusEC===>>>', idOperationStatus);
        let operationPlanif = await Operation.updateOne(
          { _id: operationTravel },
          {
            $set: { statusOperationId: idOperationStatus },
          }
        );
        console.log('operationEC===>>>', operationPlanif);
      }
    }

    // let travelsCancel = await Travel.find({
    //   operationId: operationTravel,
    //   'row.disableDate': { $exists: false },
    // });
    // console.log('travelsCancel======>>', travelsCancel);

    // if (travelsCancel.length == 0) {
    //   let orderOperationStatus = 1;
    //   console.log('orderOperationStatus===>>>', orderOperationStatus);
    //   let operationStatusCancel = await OperationStatus.findOne({ order: orderOperationStatus });
    //   console.log('operationStatusCancel===>>>', operationStatusCancel);
    //   let idOperationPlanifi = operationStatusCancel?._id;
    //   console.log('idOperationPlanifi===>>>', idOperationPlanifi);
    //   let operationPlanif = await Operation.updateOne(
    //     { _id: operationTravel },
    //     {
    //       $set: { statusOperationId: idOperationPlanifi },
    //     }
    //   );
    //   console.log('operationPlanif===>>>', operationPlanif);
    // }

    let objectLoadingOrder = travelCancel?.loadingOrder || {};
    let idTransportUnit = objectLoadingOrder?.assignment?.transportUnitId;

    let transportUnits = await TransportUnit.findById(idTransportUnit);

    let transportUnitDrivers = transportUnits.drivers;
    let transportUnitUser = transportUnitDrivers[transportUnitDrivers.length - 1]?.userId || null;
    console.log('transportUnitUser===>>', transportUnitUser);

    let messagePublish = 'Orden de carga cancelada.';
    let typeNotification = 'cancelLoadingOrder';

    let userTransportUnit = await User.findById(transportUnitUser);
    console.log('userTransportUnit===', userTransportUnit);

    let userIdOne = userTransportUnit?._id || null;
    console.log('userIdOne===>>', userIdOne);

    let tokenCarrier = userTransportUnit?.device?.pushToken || null;
    console.log('tokenCarrier===>>', tokenCarrier);

    let dataBody = {
      userId: userIdOne,
      tokenUser: tokenCarrier,
      message: messagePublish,
      type: typeNotification,
      loadingOrderId: loadingOrderId,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    // let carrierDevices = userTransportUnit.devices;
    // if (carrierDevices.length > 0) {
    //   let tokenCarrier = carrierDevices[carrierDevices.length - 1].pushToken || '';
    //   console.log('tokenCarrier===>>', tokenCarrier);

    //   let idNotification = uuidv4().toString();

    //   let payloadNotification = {
    //     default: 'default',
    //     GCM: {
    //       notification: {
    //         title: 'DeltaX',
    //         body: 'Orden de carga cancelada.',
    //         sound: 'default',
    //       },
    //       data: {
    //         type: 'cancelLoadingOrder',
    //         // operationId: idOperation,
    //         loadingOrderId: loadingOrderId,
    //         id: idNotification,
    //       },
    //     },
    //   };

    //   payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    //   payloadNotification = JSON.stringify(payloadNotification);

    //   const params_sns = {
    //     TargetArn: tokenCarrier,
    //     // TargetArn:
    //     //   'arn:aws:sns:us-east-1:879323694935:endpoint/GCM/sendNotificationDelta/a9070445-e03b-36c6-8352-ea2bd2059680',
    //     Message: payloadNotification,
    //     MessageStructure: 'json',
    //   };
    //   const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    //   const publishTextPromise2 = await sns_new.publish(params_sns).promise();
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Orden de carga cancelada.',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

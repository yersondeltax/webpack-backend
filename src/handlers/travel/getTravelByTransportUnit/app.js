const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Stages = require('../../../models/stages');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const Route = require('../../../models/route');
const MeasurementUnit = require('../../../models/measurementUnit');
const Task = require('../../../models/task');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event?.pathParameters?.transportUnitId;

    const travels = await Travel.find({
      'loadingOrder.assignment.assignmentStatus': 1,
      'loadingOrder.assignment.transportUnitId': transportUnitId,
    });

    for (let index = 0; index < travels.length; index++) {
      const travel = travels[index];
      let operationId = travel?.operationId || '';
      const operation = await Operation.findById(operationId);
      let userOperatorId = operation?.userOperatorId || '';
      const userOperatorData = await User.findById(userOperatorId);
      let authOperator = userOperatorData?.auth || {};
      let typeCurrencyOfferedId =
        travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      // const route = await Route.findById(travel.routeId);
      const stages = await Stages.find({ loadingOrderId: travel.loadingOrder.loadingOrderId });
      let stagesArray = [];
      for (const stagesTravel of stages) {
        console.log('stagesTravel===>>', stagesTravel);
        let idStages = stagesTravel._id || '';
        let tasksCarrier = [];
        tasksCarrier = await Task.find({
          stagesId: idStages,
          viewCarrier: true,
          'account.enable': true,
        });
        // let tasksStages = stagesTravel.tasks;
        // let tasksCarrier = [];
        // for (const tasks of tasksStages) {
        //   console.log('tasks=====>', tasks);
        //   let viewCarrier = tasks?.viewCarrier || false;
        //   if (viewCarrier) {
        //     tasksCarrier.push(tasks);
        //   }
        // }
        // delete stagesTravel?.tasks;
        // console.log('stagesTravelNew====>>', stagesTravel);
        let newObjectStages = {
          ...stagesTravel._doc,
          tasks: tasksCarrier,
        };
        console.log('newObjectStages====>>', newObjectStages);
        stagesArray.push(newObjectStages);
      }
      travels[index] = {
        ...travel._doc,
        stages: stagesArray,
        authUsers: authOperator,
        typeCurrencyOffered,
        // route,
      };
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travels,
      }),
    };
  } catch (err) {
    console.log('err ==>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

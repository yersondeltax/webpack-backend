const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Stages = require('../../../models/stages');
const Route = require('../../../models/route');
const MeasurementUnit = require('../../../models/measurementUnit');
const Rating = require('../../../models/rating');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 5;
    page = parseInt(page, 10);
    size = parseInt(size, 10);
    console.log('page===>>', page);
    console.log('size===>>', size);
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;

    const travel = await Travel.findOne({
      'loadingOrder.assignment.assignmentStatus': 1,
      'loadingOrder.assignment.transportUnitId': transportUnitId,
      'row.disableDate': { $exists: false },
    });

    console.log('travel====>>>', travel);
    let statusLoadingOrder = [];
    if (travel) {
      console.log('tiene viaje');
      statusLoadingOrder = travel.loadingOrder.LoadingOrderStatus || [];
    }

    let currentTravel = {};
    let futureTravel = {};
    if (statusLoadingOrder.length == 1) {
      futureTravel = travel._doc;
      let typeCurrencyOfferedId =
        futureTravel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      let typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      futureTravel = { ...futureTravel, typeCurrencyOffered };
    } else {
      if (statusLoadingOrder.length > 1) {
        currentTravel = travel._doc;
        let typeCurrencyOfferedId =
          currentTravel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
        let typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
        currentTravel = { ...currentTravel, typeCurrencyOffered };
      }
    }

    const travelsPast = await Travel.find({
      'loadingOrder.assignment.assignmentStatus': 0,
      'loadingOrder.assignment.transportUnitId': transportUnitId,
      'row.disableDate': { $exists: false },
    })
      .limit(size)
      .skip(page * size)
      .sort({ 'row.createDate': 'descending' });

    let array = [];
    for (const travelDoc of travelsPast) {
      let travelId = travelDoc._doc;
      let typeCurrencyOfferedIds =
        travelId?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      let typeCurrencyOffereds = await MeasurementUnit.findById(typeCurrencyOfferedIds);
      let idTravel = travelId?._id || '';
      let ratingOne = await Rating.findOne({ travelId: idTravel });
      travelId = { ...travelId, typeCurrencyOffered: typeCurrencyOffereds, rating: ratingOne };
      array.push(travelId);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        futureTravel,
        currentTravel,
        travelsPast: array,
      }),
    };
  } catch (err) {
    console.log('err ==>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

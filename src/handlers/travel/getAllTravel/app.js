const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const Route = require('../../../models/route');
const Operation = require('../../../models/operation');
const MeasurementUnit = require('../../../models/measurementUnit');
const Company = require('../../../models/company');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 5;
    var type = event?.queryStringParameters?.type || 'full';
    page = parseInt(page, 10);
    size = parseInt(size, 10);
    console.log('page===>>', page);
    console.log('size===>>', size);
    console.log('type===>>', type);
    await dbConnection(process.env.dataBaseConnection);

    let travels = [];
    if (type == 'full') {
      travels = await Travel.find({ publish: true })
        .limit(size)
        .skip(page * size)
        .sort({ 'row.createDate': -1 });
      console.log('travelsFull======>>>', travels);
    } else {
      if (type == 'date') {
        console.log('entro en if de date');
        let dateBegin = event?.queryStringParameters?.dateBegin || '';
        let dateEnd = event?.queryStringParameters?.dateEnd || '';
        console.log('dateBegin=====>>>', dateBegin);
        console.log('dateEnd=====>>>', dateEnd);
        travels = await Travel.find({
          publish: true,
          'dates.loadingDate': { $gte: new Date(dateBegin), $lte: new Date(dateEnd) },
        })
          .limit(size)
          .skip(page * size)
          .sort({ 'row.createDate': -1 });
        console.log('travelsDates======>>>', travels);
      } else {
        if (type == 'category') {
          var categoryId = event?.queryStringParameters?.categoryId || '';
          travels = await Travel.find({ publish: true, 'categoryLoad.categoryLoadId': categoryId })
            .limit(size)
            .skip(page * size)
            .sort({ 'row.createDate': -1 });
          console.log('travelsCategory======>>>', travels);
        } else {
          if (type == 'categoryDate') {
            console.log('entro en if de categoryDate');
            let dateBegin = event?.queryStringParameters?.dateBegin || '';
            let dateEnd = event?.queryStringParameters?.dateEnd || '';
            let categoryId = event?.queryStringParameters?.categoryId || '';
            console.log('dateBegin=====>>>', dateBegin);
            console.log('dateEnd=====>>>', dateEnd);
            console.log('categoryId=====>>>', categoryId);
            travels = await Travel.find({
              publish: true,
              'dates.loadingDate': { $gte: new Date(dateBegin), $lte: new Date(dateEnd) },
              'categoryLoad.categoryLoadId': categoryId,
            })
              .limit(size)
              .skip(page * size)
              .sort({ 'row.createDate': -1 });
            console.log('travelsCategoryDate======>>>', travels);
          }
        }
      }
    }

    for (let index = 0; index < travels.length; index++) {
      const travel = travels[index];
      // const route = await Route.findById(travel.routeId);
      let typeCurrencyOfferedId =
        travel?.freightValues?.freightOffered?.typeCurrencyOfferedId || '';
      const typeCurrencyOffered = await MeasurementUnit.findById(typeCurrencyOfferedId);
      const operation = await Operation.findById(travel.operationId);
      let companyOperatorId = operation?.companyOperatorId || '';
      const companyData = await Company.findById(companyOperatorId);
      travels[index] = {
        ...travel._doc,
        // route,
        typeCurrencyOffered,
        operation,
        company: companyData,
      };
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travels,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

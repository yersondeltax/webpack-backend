var AWS = require('aws-sdk');

const Pusher = require('pusher');

const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const User = require('../../.../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    // const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const travelId = event?.pathParameters?.travelId || null;
    const loadingOrderId = event?.pathParameters?.loadingOrderId || null;

    const travelUpdate = await Travel.findOne({
      'loadingOrder.loadingOrderId': loadingOrderId,
      _id: travelId,
    });

    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    let transportUnitId = travelUpdate.loadingOrder.assignment.transportUnitId;

    const transportUnitFind = await TransportUnit.findById(transportUnitId);
    console.log('transportUnitFind ==> ', transportUnitFind);

    const carrierId =
      transportUnitFind?.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

    let messagePublish = `Ubicacion actualizada.`;
    let typeNotification = 'location';

    const carrierFind = await User.findById(carrierId);
    console.log('carrierFind ==> ', carrierFind);

    let userIdOne = carrierFind?._id || null;
    console.log('userIdOne===>>', userIdOne);

    const token = carrierFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    //////////// notificacion pusher /////////

    const pusher = new Pusher({
      appId: '1266998',
      key: 'f8f1c4d725ab089521d6',
      secret: '491a6e8ca25b98ea5455',
      cluster: 'us2',
      useTLS: true,
    });

    await pusher.trigger(`loading-${travelId}`, 'location', {
      message: 'Localización actualizada.',
    });

    /////////////////////////////////////////

    // const messagePublish = `Ubicacion actualizada.`;
    // let payloadNotification = {
    //   default: 'default',
    //   GCM: {
    //     notification: {
    //       title: 'DeltaX',
    //       body: messagePublish,
    //       sound: 'default',
    //     },
    //     data: {
    //       type: 'location',
    //     },
    //   },
    // };
    // payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    // payloadNotification = JSON.stringify(payloadNotification);
    // const params_sns = {
    //   TargetArn: token,
    //   Message: payloadNotification,
    //   MessageStructure: 'json',
    // };
    // const publishText = await sns_new
    //   .publish(params_sns)
    //   .promise()
    //   .then(
    //     function (data) {
    //       /* process the data */
    //       console.log('dataaa', data);
    //       return data;
    //     },
    //     function (error) {
    //       console.log('errorrrr', error);
    //       return error;
    //       /* handle the error */
    //     }
    //   );
    // console.log('publishText => ', publishText);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Pedido de ubicación realizada.',
      }),
    };
  } catch (err) {
    console.log('err ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

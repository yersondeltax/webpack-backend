var AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const User = require('../../.../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Setting = require('../../../models/setting');
const News = require('../../../models/news');
const Notification = require('../../../models/notification');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const { travelId, news, type } = event;
    console.log('event====>>>', event);
    await dbConnection(process.env.dataBaseConnection);
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    let messagePublish = '';
    let typeNotification = '';
    let dataNews = false;
    let notificationTravel;
    let dataFind = {};
    let travelIdNotification = null;
    if (type == 'new') {
      let newsId = await News.findById(news);
      let name = newsId.name;
      messagePublish = `Noticia: ${name}`;
      typeNotification = 'news';
      dataNews = true;
    } else {
      if (type == 'notification') {
        // let newsId = await News.findById(news);
        // let name = newsId.name;
        messagePublish = news;
        typeNotification = 'notification';
        dataNews = true;
      } else {
        travelIdNotification = travelId;
        notificationTravel = await Travel.findById(travelId)
          .exec()
          .then(async (travelFind) => {
            messagePublish = `Se ha publicado una nueva oportunidad de ${travelFind.categoryLoad.name}`;

            return { messagePublish, travelFind };
          });
        messagePublish = notificationTravel.messagePublish;

        dataFind = notificationTravel.travelFind._doc;

        typeNotification = 'travel';

        dataNews = false;
      }
    }

    // let payloadNotification = {
    //   default: 'default',
    //   GCM: {
    //     notification: {
    //       title: 'DeltaX',
    //       body: messagePublish,
    //       sound: 'default',
    //     },
    //     data: {
    //       type: typeNotification,
    //     },
    //   },
    // };
    // payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    // payloadNotification = JSON.stringify(payloadNotification);
    ////////////////////////////////////////////////////////////////////
    const setting = await Setting.find();
    const transportUnits = await TransportUnit.find();
    const travelFind = dataFind;

    for (const transportUnit of transportUnits) {
      let flag = true;
      if (dataNews == false) {
        if (setting[0].featureRestriction) {
          if (travelFind.features) {
            for (let i = 0; i < travelFind.features.length; i++) {
              const feature = travelFind.features[i];
              const newFeature = {
                name: feature.name,
                featuresTransportUnitId: feature.featuresTransportUnitId,
                valueQualitative: feature.valueQualitative || '',
                valueQuantitative: feature.valueQuantitative || '',
              };
              const found = transportUnit.features.find((featureTU) => {
                const newFeatureTU = {
                  name: featureTU.name,
                  featuresTransportUnitId: featureTU.featuresTransportUnitId,
                  valueQualitative: featureTU.valueQualitative || '',
                  valueQuantitative: featureTU.valueQuantitative || '',
                };
                return JSON.stringify(newFeatureTU) === JSON.stringify(newFeature);
              });

              if (!found) {
                flag = false;
                break;
              }
            }
          }
        }
      }
      //notificar
      if (flag) {
        const carrier = await User.findById(
          transportUnit.drivers[transportUnit.drivers.length - 1].userId
        );

        let userIdOne = carrier?._id || null;
        console.log('userIdOne===>>', userIdOne);

        const token = carrier?.device?.pushToken || null;
        console.log('TOKEN => ', token);
        // let carrierId = transportUnit?.drivers[transportUnit.drivers.length - 1]?.userId || '';
        let dataBody = {
          userId: userIdOne,
          travelId: travelIdNotification,
          tokenUser: token,
          message: messagePublish,
          type: typeNotification,
        };
        let newNotification = new Notification(dataBody);
        let notificationSaved = await newNotification.save();
        // if (carrier) {
        //   console.log('usersCarrier => ', carrier);

        // if (carrier?.devices?.length > 0) {
        //   const token = carrier.devices[carrier.devices.length - 1].pushToken;
        //   console.log('TOKEN => ', token);
        //   const params_sns = {
        //     TargetArn: token,
        //     Message: payloadNotification,
        //     MessageStructure: 'json',
        //   };
        //   const publishTextPromise2 = await sns_new
        //     .publish(params_sns)
        //     .promise()
        //     .then(
        //       function (data) {
        //         /* process the data */
        //         console.log('dataaa', data);
        //         return data;
        //       },
        //       function (error) {
        //         console.log('errorrrr', error);
        //         return error;
        //         /* handle the error */
        //       }
        //     );
        //   console.log('publishTextPromise2 => ', publishTextPromise2);
        // }
        // }
      }
    }
    ////////////////////////////////////////////////////////////////////
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Publicado correctamente',
      }),
    };
  } catch (err) {
    console.log('err ===>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');
const momentTime = require('moment-timezone');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const TravelStatus = require('../../../models/travelStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entro al llamado automatico');
    await dbConnection(process.env.dataBaseConnection);
    console.log('event=====>>>', event);

    const date = moment().format();
    console.log('date======>>', date);
    const year = momentTime
      .utc(date, 'YYYY-MM-DDTHH:mm:ss')
      .tz('America/La_Paz')
      .format('YYYY-MM-DDTHH:mm:ss');

    console.log('year======>>', year);

    let day = 5;
    let newDateSubtract = moment(year).subtract(day, 'day').format('YYYY-MM-DDTHH:mm:ss');

    let travelStatusCancel = await TravelStatus.findOne({ order: 3 });
    console.log('travelStatusCancel===>>>', travelStatusCancel);
    let dataCancel = {
      statusId: travelStatusCancel?._id || '',
      name: travelStatusCancel?.name || '',
      date: date,
      order: travelStatusCancel?.order,
    };

    const res = await Travel.updateMany(
      { 'dates.loadingDate': { $lte: new Date(newDateSubtract) }, publish: true },
      { $set: { publish: false, $push: { travelstatus: dataCancel } } },
      { multi: true }
    );

    console.log('res=====>>>', res);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Bien',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

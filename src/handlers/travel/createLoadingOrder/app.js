var AWS = require('aws-sdk');
const moment = require('moment');
const axios = require('axios');

const mongoose = require('mongoose');
const { dbConnection } = require('../../../database/config');
const Travel = require('../../../models/travel');
const User = require('../../.../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Operation = require('../../../models/operation');
const Postulation = require('../../../models/postulation');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const Notification = require('../../../models/notification');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const travelId = event.pathParameters.travelId;
    const transportUnitId = body.assignment.transportUnitId;
    let date = moment().format();
    let loadingOrderStatusAsignmen = await LoadingOrderStatus.findOne({
      order: 1,
    });
    let status = {
      statusId: loadingOrderStatusAsignmen?._id || '',
      name: loadingOrderStatusAsignmen?.name || '',
      date: date,
      order: loadingOrderStatusAsignmen?.order,
    };
    console.log('status===>>>', status);
    // const status = {
    //   statusId: '60b125d93e9ef60008aa9c95',
    //   name: 'Asignada',
    //   order: 1,
    //   date,
    // };

    let linkApp = '';

    let travelAndTransportUnit = await Travel.findOne({
      'loadingOrder.assignment.transportUnitId': transportUnitId,
      'loadingOrder.assignment.assignmentStatus': 1,
    });
    console.log('travelAndTransportUnit======>>>', travelAndTransportUnit);
    let objectoNuevoTravel = {};
    let newComparationByTravel =
      JSON.stringify(travelAndTransportUnit) === JSON.stringify(objectoNuevoTravel);
    console.log('newComparationByTravel======>>>', newComparationByTravel);
    if (travelAndTransportUnit != null) {
      if (newComparationByTravel == false) {
        return {
          statusCode: 200,
          headers,
          body: JSON.stringify({
            message:
              'No se puede crear la orden de carga, el chofer asignado tiene una orden de carga ya asignada.',
          }),
        };
      }
    }

    const loadingOrderId = mongoose.Types.ObjectId();
    const data = { ...body, LoadingOrderStatus: [status], loadingOrderId };
    console.log('data ==> ', data);
    // await Travel.updateOne(
    //   { _id: travelId },
    //   {
    //     $set: { loadingOrder: data },
    //   },
    //   { upsert: true }
    // );

    const cancelPostulation = await Postulation.findOneAndUpdate(
      { travelId: travelId, transportUnitId: transportUnitId, 'account.enable': 1 },
      {
        $set: { 'account.enable': 0 },
      },
      { new: true }
    );

    let keyLink = process?.env?.keyLink || '';
    let domainUriPrefix = process?.env?.domainUriPrefix || '';
    let dynamicLink = process?.env?.dynamicLink || '';
    let nameAppInfo = process?.env?.nameAppInfo || '';

    const respondeGooleApiLink = await axios({
      method: 'post',
      url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${keyLink}`,
      data: {
        dynamicLinkInfo: {
          domainUriPrefix: `${domainUriPrefix}`,
          link: `${dynamicLink}/operation_page`,
          androidInfo: {
            androidPackageName: `${nameAppInfo}`,
          },
          iosInfo: {
            iosBundleId: `${nameAppInfo}`,
          },
        },
      },
    });

    console.log('respondeGooleApiLink====>>>', respondeGooleApiLink);
    console.log('respondeGooleApiLink.data====>>>', respondeGooleApiLink?.data);

    linkApp = respondeGooleApiLink?.data?.shortLink || '';

    const travelUpdate = await Travel.findOneAndUpdate(
      { _id: travelId },
      {
        $set: { loadingOrder: data, publish: false, linkApp: linkApp },
      },
      { new: true }
    );

    const transportUnitRejectedIds = await Postulation.find({
      $and: [
        { travelId: travelId },
        { confirmedDate: { $exists: false } },
        { transportUnitId: { $ne: transportUnitId } },
      ],
    }).select('transportUnitId -_id');
    console.log('transportUnitRejectedIds ==> ', transportUnitRejectedIds);
    const arrayTransportIds = [];
    for (const transportUnitRejectedId of transportUnitRejectedIds) {
      arrayTransportIds.push(transportUnitRejectedId.transportUnitId);
    }

    const transportUnitRejected = await TransportUnit.find({
      _id: { $in: arrayTransportIds },
    });
    console.log('transportUnitRejected ==> ', transportUnitRejected);
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    // // // // // STARTFOR // // // // //
    for (const transportUnitTemp of transportUnitRejected) {
      const driver =
        transportUnitTemp.drivers[transportUnitTemp.drivers.length - 1]?.userId || null;
      let messagePublish = `Lamentablemente tu postulación ya ha sido asignada a otro chófer.`;
      let typeNotification = 'UnassignedLoadOrder';

      const driverNotify = await User.findById(driver);
      console.log('driverNotify ==> ', driverNotify);

      let userIdOne = driverNotify?._id || null;
      console.log('userIdOne===>>', userIdOne);

      const tokenDriver = driverNotify?.device?.pushToken || null;
      console.log('tokenDriver ==> ', tokenDriver);

      let dataBody = {
        userId: userIdOne,
        tokenUser: tokenDriver,
        message: messagePublish,
        type: typeNotification,
        travelId: travelId,
      };

      let newNotification = new Notification(dataBody);
      let notificationSaved = await newNotification.save();
      console.log('notificationSaved===>>', notificationSaved);

      // const messagePublish = `Lamentablemente tu postulación ya ha sido asignada a otro chófer.`;
      // let payloadNotification = {
      //   default: 'default',
      //   GCM: {
      //     notification: {
      //       title: 'DeltaX',
      //       body: messagePublish,
      //       sound: 'default',
      //     },
      //     data: {
      //       travelId,
      //       type: 'UnassignedLoadOrder',
      //     },
      //   },
      // };
      // payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
      // payloadNotification = JSON.stringify(payloadNotification);
      // const params_sns = {
      //   TargetArn: tokenDriver,
      //   Message: payloadNotification,
      //   MessageStructure: 'json',
      // };
      // const publishTextTemp = await sns_new
      //   .publish(params_sns)
      //   .promise()
      //   .then(
      //     function (data) {
      //       /* process the data */
      //       console.log('dataaa', data);
      //       return data;
      //     },
      //     function (error) {
      //       console.log('errorrrr', error);
      //       return error;
      //       /* handle the error */
      //     }
      //   );
      // console.log('publishTextTemp ==>', publishTextTemp);
    }
    // // // // // ENDFOR // // // // //

    let operationId = travelUpdate.operationId;

    let operationStatusProgres = await OperationStatus.findOne({ order: 2 });
    console.log('operationStatusProgres===>>>', operationStatusProgres);
    let operationStatusProgresId = operationStatusProgres?._id || '';

    let operationData = await Operation.findOneAndUpdate(
      {
        _id: operationId,
      },
      { $set: { statusOperationId: operationStatusProgresId } },
      { new: true }
    );

    const operationTravel = await Operation.findById(operationId);

    const transportUnitFind = await TransportUnit.findById(transportUnitId);
    console.log('transportUnitFind ==> ', transportUnitFind);

    const carrierId =
      transportUnitFind?.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

    let messagePublishAssinment = `Se te ha asignado una orden de carga. Ingresa para ver más detalles.`;
    let typeNotificationAssinment = 'AssignedLoadOrder';

    const carrierFind = await User.findById(carrierId);
    console.log('carrierFind ==> ', carrierFind);

    let userIdOn = carrierFind?._id || null;
    console.log('userIdOn===>>', userIdOn);

    const token = carrierFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let dataBodyAssinment = {
      userId: userIdOn,
      tokenUser: token,
      message: messagePublishAssinment,
      type: typeNotificationAssinment,
      travelId: travelId,
    };

    let newNotificationAssinment = new Notification(dataBodyAssinment);
    let notificationSavedAssinment = await newNotificationAssinment.save();
    console.log('notificationSavedAssinment===>>', notificationSavedAssinment);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Orden de carga creada correctamente.',
        operation: operationTravel,
        travel: travelUpdate,
        loadingOrderId: loadingOrderId,
      }),
    };
  } catch (err) {
    console.log('err ==> ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

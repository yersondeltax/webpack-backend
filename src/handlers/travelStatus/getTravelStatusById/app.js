const { dbConnection } = require('../../../database/config');
const TravelStatus = require('../../../models/travelStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const travelStatusId = event.pathParameters.travelStatusId;
    console.log('travelStatusId===>>>', travelStatusId);

    let travelStatus = await TravelStatus.findOne({ _id: travelStatusId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        travelStatus,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

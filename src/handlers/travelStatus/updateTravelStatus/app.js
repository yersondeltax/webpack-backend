const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const TravelStatus = require('../../../models/travelStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const travelStatusId = event.pathParameters.travelStatusId;
    console.log('travelStatusId===>>>', travelStatusId);

    let updateDate = moment().format();
    let travelStatusUpdate = await TravelStatus.updateOne(
      { _id: travelStatusId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('travelStatusUpdate===>>>', travelStatusUpdate);
    if (travelStatusUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Estado actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

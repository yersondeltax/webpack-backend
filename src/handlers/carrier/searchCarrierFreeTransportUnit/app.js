const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Travel = require('../../../models/travel');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    console.log('event====>>', event);
    let phone = event?.queryStringParameters?.phone;
    console.log('phone====>>', phone);
    await dbConnection(process.env.dataBaseConnection);
    let carrierSearch = await User.find({
      'auth.phone': new RegExp(phone, 'i'),
      type: 'Carrier',
    });

    const listCarrierFree = [];
    for (const carrier of carrierSearch) {
      let carrierId = carrier?._id || null;
      let transportUnits = await TransportUnit.findOne({
        'drivers.userId': carrierId,
        'drivers.active': true,
      });
      let drivers = transportUnits?.drivers || [];
      if (drivers.length > 0) {
      } else {
        listCarrierFree.push(carrier);
      }
    }

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        carrier: listCarrierFree,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Algo salio mal.',
        error: err,
      }),
    };
  }
  return response;
};

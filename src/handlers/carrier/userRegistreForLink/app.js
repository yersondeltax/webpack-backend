const AWS = require('aws-sdk');
const CryptoJS = require('crypto-js');

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const { v4: uuidv4 } = require('uuid');
const VipDrivers = require('../../../models/vipDrivers');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
const UserPoolId = 'us-east-1_uR6LFXj4U';
let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    console.log('EVENT ==>', event);
    console.log('AWS===>>', AWS);
    console.log('cognitoidentityserviceprovider===>>', cognitoidentityserviceprovider);
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    let params = {
      Username: data.auth.email,
      UserPoolId: UserPoolId,
    };

    console.log('paso  1');
    const newUser = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.adminCreateUser(params, function (err, data) {
        if (err) {
          // callback.callbackRegisterComplete(err);
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
          // callback.callbackRegisterComplete(null); //
        }
      });
    });

    console.log('newUser====>>', newUser);
    let password = '123456789651651';

    if (newUser.ok) {
      const payload = {
        userId: newUser.User.Username,
        ...data,
      };
      const userDB = new User(payload);
      let userDBSave = await userDB.save();
      console.log('userDBSave====>>', userDBSave);
      let userCognitoId = newUser?.User?.Username || '';
      let passs = uuidv4().toString();
      console.log('passs=====>>>', passs);

      let params2 = {
        Password: password,
        UserPoolId: UserPoolId,
        Username: userCognitoId,
        Permanent: true,
      };

      console.log('params2===>>>', params2);

      const newUserCognito = await new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.adminSetUserPassword(params2, function (err, data) {
          if (err) {
            console.log('ERR ==>', err);
            reject({ ok: false, error: { err } });
          } else {
            console.log('DATA ==>', data);
            resolve({ ok: true, ...data });
          }
        });
      });
      console.log('newUserCognito====>>>', newUserCognito);
    }
    let cadena = `https://www.deltaxbeta.com/loadOrder/show/614b3daf34ebe80009eeeb41?token=`;

    let emailNew = data?.auth?.email || '';
    let token = `${emailNew}&pass=${password}`;

    let encrypt = CryptoJS.AES.encrypt(token, 'DX373968026').toString();
    let fullCadenaEncrip = cadena + encrypt;

    console.log('encrypt====>>>', encrypt);

    var bytes = CryptoJS.AES.decrypt(encrypt, 'DX373968026');
    let decryptedPassword = bytes.toString(CryptoJS.enc.Utf8);
    console.log('decryptedPassword====>>>', decryptedPassword);

    let fullCadenaDescrip = cadena + decryptedPassword;
    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Usuario creado exitosamente',
        fullCadenaDescrip: fullCadenaDescrip,
        fullCadenaEncrip,
      }),
    };
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        error: err,
      }),
    };
    // return err;
  }
  return response;
};

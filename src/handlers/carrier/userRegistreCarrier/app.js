const AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const VipDrivers = require('../../../models/vipDrivers');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
const UserPoolId = process?.env?.IdGrupoCognitoCarrier || '';
const clienteIdCognito = process?.env?.IdClientCognitoCarrier || '';
let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    console.log('EVENT ==>', event);
    console.log('AWS===>>', AWS);
    let companyId = event?.queryStringParameters?.companyId || '';
    console.log('companyId====>>', companyId);
    console.log('cognitoidentityserviceprovider===>>', cognitoidentityserviceprovider);
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    let phoneCarrier = data?.phone || '';
    let params = {
      ClientId: clienteIdCognito,
      Password: '12345678',
      Username: phoneCarrier,
      UserAttributes: [
        {
          Name: 'custom:token',
          Value: '1',
        },
        {
          Name: 'phone_number',
          Value: phoneCarrier,
        },
      ],
    };

    console.log('paso  1');
    const newUser = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.signUp(params, function (err, data) {
        if (err) {
          // callback.callbackRegisterComplete(err);
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
          // callback.callbackRegisterComplete(null); //
        }
      });
    });

    console.log('newUser====>>', newUser);

    let userCarrierRegister = {};

    if (newUser.ok) {
      let userCognitoId = newUser?.UserSub || '';
      let params = {
        UserAttributes: [
          {
            Name: 'phone_number_verified',
            Value: 'true',
          },
        ],
        Username: userCognitoId,
        UserPoolId: UserPoolId,
      };
      console.log('params===>>>', params);

      const newUserCognito = await new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
          if (err) {
            console.log('ERR ==>', err);
            reject({ ok: false, error: { err } });
          } else {
            console.log('DATA ==>', data);
            resolve({ ok: true, ...data });
          }
        });
      });
      console.log('newUserCognito====>>>', newUserCognito);

      let params2 = {
        Username: userCognitoId,
        UserPoolId: UserPoolId,
      };
      console.log('params2===>>>', params2);

      const newUserCognito2 = await new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.adminConfirmSignUp(params2, function (err, data) {
          if (err) {
            console.log('ERR ==>', err);
            reject({ ok: false, error: { err } });
          } else {
            console.log('DATA ==>', data);
            resolve({ ok: true, ...data });
          }
        });
      });
      console.log('newUserCognito2====>>>', newUserCognito2);
      // if (newUserCognito.ok) {
      let userUpdate = await User.updateOne(
        { userId: userCognitoId },
        {
          $set: { 'auth.confirmed': true },
        }
      );

      console.log('userUpdate===>>>', userUpdate);
      // let res = phoneCarrier.split('+591');
      // let phone = res[1];
      // console.log('numero===>>', phone);

      // let dataVipDrivers = { phone: phone, companyId: companyId };
      // const newVipDrivers = new VipDrivers(dataVipDrivers);
      // const vipDriversSaved = await newVipDrivers.save();
      userCarrierRegister = await User.findOne({ userId: userCognitoId, type: 'Carrier' });
    }

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Usuario creado exitosamente',
        user: userCarrierRegister,
      }),
    };

    // event.response = { autoConfirmUser: true };
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        error: err,
      }),
    };
    // return err;
  }
  return response;
};

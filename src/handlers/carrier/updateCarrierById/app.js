const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const Notification = require('../../../models/notification');
const moment = require('moment');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    let updateDate = moment().format();
    await dbConnection(process.env.dataBaseConnection);
    const userId = event?.pathParameters?.userId || null;
    let data = { ...body, updateDate: updateDate };

    let userUpdated = await User.updateOne(
      { _id: userId },
      {
        $set: data,
      },
      { upsert: true }
    );

    const carrierFind = await User.findById(userId);
    console.log('carrierFind ==> ', carrierFind);

    let userIdOne = carrierFind?._id || null;
    console.log('userIdOne===>>', userIdOne);

    const token = carrierFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: 'Datos actualizados.',
      type: 'updateuser',
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Datos actualizado exitosamente',
        user: carrierFind,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

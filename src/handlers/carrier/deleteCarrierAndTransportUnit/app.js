const AWS = require('aws-sdk');
const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
// const UserPoolId = 'us-east-1_Rhq7ZwU3v';
const UserPoolId = 'us-east-1_7JlBMUgy1';

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let phone = body?.Phone || '';
    const userCognito = await User.findOne({ 'auth.phone': phone, type: 'Carrier' });
    if (!userCognito) {
      return {
        statusCode: 500,
        headers,
        body: JSON.stringify({
          message: 'Numero no encontrado',
        }),
      };
    }
    const userCognitoId = userCognito?.userId || '';
    let userIdData = userCognito?._id || null;
    let userUpdate;
    let mensaje = 'desabilitado';
    let params = {
      Username: userCognitoId,
      UserPoolId: UserPoolId,
    };
    let usersDelete = await User.deleteOne({ _id: userIdData });
    console.log('usersDelete====>>', usersDelete);
    const newUserCognito = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.adminDeleteUser(params, function (err, data) {
        if (err) {
          // callback.callbackRegisterComplete(err);
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
          // callback.callbackRegisterComplete(null); //
        }
      });
    });
    console.log('newUserCognito====>>>', newUserCognito);

    const dataTransportUnit = await TransportUnit.findOne({ 'drivers.userId': userIdData });
    if (!dataTransportUnit) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'unidad no encontrada pero usurios eliminados',
        }),
      };
    }

    let transportUnitDelete = await TransportUnit.deleteOne({ _id: dataTransportUnit._id });
    console.log('transportUnitDelete====>>', transportUnitDelete);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'usuarios y unidad de transporte eliminados correctamente',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

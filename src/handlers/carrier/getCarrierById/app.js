const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const userId = event?.pathParameters?.userId || null;
    const userCarrier = await User.findById(userId);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        user: userCarrier,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        error: err,
      }),
    };
  }
};

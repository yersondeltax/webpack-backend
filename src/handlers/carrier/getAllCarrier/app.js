const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 100;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    let type = event?.queryStringParameters?.type || 'asc';
    console.log('type===>>', type);
    let vip = event?.queryStringParameters?.vip;
    console.log('vip===>>', vip);
    var pageNumber = 0;
    var sizeNumber = 10;
    let totalPage = 1;
    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectOrder;
    if (order) {
      objectOrder = {
        $sort: {},
      };
      if (type == 'asc') {
        objectOrder.$sort[`${order}`] = 1;
      } else {
        objectOrder.$sort[`${order}`] = -1;
      }
    }
    let objectFilter;
    if (filter) {
      if (filter == '0') {
        objectFilter = {
          $match: {
            $or: [
              {
                'profile.firstName': {
                  $exists: true,
                },
              },
              {
                'profile.firstName': {
                  $exists: false,
                },
              },
              {
                'profile.lastName': {
                  $exists: true,
                },
              },
              {
                'profile.lastName': {
                  $exists: false,
                },
              },
              {
                'profile.birthDate': {
                  $exists: true,
                },
              },
              {
                'profile.birthDate': {
                  $exists: false,
                },
              },
              {
                'profile.documentId': {
                  $exists: true,
                },
              },
              {
                'profile.documentId': {
                  $exists: false,
                },
              },
              {
                'auth.phone': {
                  $exists: true,
                },
              },
              {
                'auth.phone': {
                  $exists: false,
                },
              },
            ],
          },
        };
      }
      if (filter == '1') {
        objectFilter = {
          $match: {
            $and: [
              {
                'profile.firstName': {
                  $exists: true,
                },
              },
              {
                'profile.lastName': {
                  $exists: true,
                },
              },
              {
                'profile.birthDate': {
                  $exists: true,
                },
              },
              {
                'profile.documentId': {
                  $exists: true,
                },
              },
              {
                'auth.phone': {
                  $exists: true,
                },
              },
            ],
          },
        };
      }
      if (filter == '2') {
        objectFilter = {
          $match: {
            $or: [
              {
                'profile.firstName': {
                  $exists: false,
                },
              },
              {
                'profile.lastName': {
                  $exists: false,
                },
              },
              {
                'profile.birthDate': {
                  $exists: false,
                },
              },
              {
                'profile.documentId': {
                  $exists: false,
                },
              },
              {
                'auth.phone': {
                  $exists: false,
                },
              },
            ],
          },
        };
      }
    }

    let objectSearch;
    if (search) {
      objectSearch = {
        $match: {
          type: 'Carrier',
          'auth.phone': new RegExp(search, 'i'),
        },
      };
    } else {
      objectSearch = {
        $match: {
          type: 'Carrier',
        },
      };
    }

    let objectVip;
    if (vip) {
      if (vip == '0') {
        objectVip = {
          $match: { $or: [{ phone: { $size: 1 } }, { phone: { $size: 0 } }] },
        };
      }
      if (vip == '1') {
        objectVip = {
          $match: { phone: { $size: 1 } },
        };
      }
      if (vip == '2') {
        objectVip = {
          $match: { phone: { $size: 0 } },
        };
      }
    }

    let carriers = await User.aggregate([
      {
        $lookup: {
          from: 'vipdrivers',
          localField: 'auth.phone',
          foreignField: 'phone',
          as: 'phone',
        },
      },
      objectSearch,
      objectOrder,
      objectFilter,
      objectVip,
      {
        $skip: pageNumber * sizeNumber,
      },
      {
        $limit: sizeNumber,
      },
    ]);

    let total = await User.aggregate([
      {
        $lookup: {
          from: 'vipdrivers',
          localField: 'auth.phone',
          foreignField: 'phone',
          as: 'phone',
        },
      },
      objectSearch,
      objectOrder,
      objectFilter,
      objectVip,
      {
        $count: 'cant',
      },
    ]);
    if (carriers.length > 0) {
      let newCant = total[0].cant;
      console.log('newCant====>>', newCant);
      total = parseInt(newCant, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    console.log('carriers=====>>>', carriers);
    let newCarrierTransportUnit = [];
    for (const objectCarrier of carriers) {
      let transportUnitFull = {};

      let carrierId = objectCarrier?._id || null;
      let transportUnits = await TransportUnit.find({
        'drivers.userId': carrierId,
        'drivers.active': true,
      });
      if (transportUnits.length > 0) {
        for (const transportUnitObject of transportUnits) {
          let drivers = transportUnitObject?.drivers || [];
          let objectDrivers = drivers[drivers.length - 1] || {};
          let driver = objectDrivers?.userId || '';
          let active = objectDrivers?.active;
          let objectCarrie = {
            carrierId,
            active: true,
          };
          let objectoNew = {
            carrierId: driver,
            active: active,
          };
          let comparation = JSON.stringify(objectCarrie) === JSON.stringify(objectoNew);
          console.log('comparation=====>>>', comparation);
          if (comparation) {
            transportUnitFull = transportUnitObject;
          }
        }
      }
      let carrierTransportUnit = {
        carrier: objectCarrier,
        transportUnit: transportUnitFull,
      };
      newCarrierTransportUnit.push(carrierTransportUnit);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        carrier: newCarrierTransportUnit,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        error: err,
      }),
    };
  }
};

var AWS = require('aws-sdk');
// var lambda = new AWS.Lambda({ region: 'us-east-1' });
var stepFunctions = new AWS.StepFunctions({ region: 'us-east-1' });
const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entroo acaa');
    const stateMachineArn = process?.env?.stateMachineArn || '';
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    let userId = body?.userId || null;

    let type = event?.queryStringParameters?.type;
    console.log('type====>>', type);
    let notificationSaved = {};

    if (type == 'one') {
      let userOne = await User.findOne({ _id: userId });
      let userIdOne = userOne?._id || null;
      console.log('userIdOne===>>', userIdOne);
      let token = userOne?.device?.pushToken || null;
      let data = { ...body, type: 'notification', tokenUser: token, userId: userIdOne };
      const newNotification = new Notification(data);
      notificationSaved = await newNotification.save();
    } else {
      let travelId = 0;
      let news = body?.message || '';
      let type = 'notification';

      const payload = JSON.stringify({ travelId: travelId, news, type });
      //// ===== produccion ===
      // let params = {
      //   stateMachineArn:
      //     'arn:aws:states:us-east-1:879323694935:stateMachine:notifyPublishStateMachine-MI0e1C2LuRB5',
      //   input: payload,
      // };
      // ====== staging ======
      // let params = {
      //   stateMachineArn:
      //     'arn:aws:states:us-east-1:345062269307:stateMachine:notifyPublishStateMachine-ayLxMACz2i6S',
      //   input: payload,
      // };
      let params = {
        stateMachineArn: stateMachineArn,
        input: payload,
      };

      await stepFunctions.startExecution(params).promise();
    }
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Notificación registrada exitosamente',
        notification: notificationSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

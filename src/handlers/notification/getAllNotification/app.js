const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let userId = event?.queryStringParameters?.userId;
    // let enable = event?.queryStringParameters?.enable || 'false';

    // console.log('userId====>>', userId);
    // console.log('enable====>>', enable);
    await dbConnection(process.env.dataBaseConnection);
    let notifications = [];
    notifications = await Notification.find();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        notifications,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

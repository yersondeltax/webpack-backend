const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    console.log('event===>>', event);
    let userId = event?.pathParameters?.userId || null;
    if (typeof userId == 'undefined' || !userId || userId === 'undefined' || userId == '0') {
      console.log('entro en verdadero');
      userId = event?.queryStringParameters?.userId;
    }
    console.log('userId====>>', userId);
    let type = event?.queryStringParameters?.type;
    console.log('type====>>', type);
    await dbConnection(process.env.dataBaseConnection);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let notifications = [];
    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectFilter = {};
    if (type) {
      objectFilter = {
        type: type,
      };
    }
    notifications = await Notification.find({ userId: userId, ...objectFilter })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort({ createDate: -1 });
    let total;
    if (page || size) {
      total = await Notification.find({ userId: userId, ...objectFilter }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        notifications,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

var AWS = require('aws-sdk');
// var lambda = new AWS.Lambda({ region: 'us-east-1' });
const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const User = require('../../../models/user');
const nodemailer = require('nodemailer');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entroo acaa');
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    let emailCorreo = body?.email || '';

    let fullNameOperator = 'Yerson Rafael Fuentes Herrera';
    let inCharge = 'Encargado Backend';
    let fullPhone = '70289279';
    let emailOperator = 'yerson.fuentes2012@gmail.com';

    let contentHTML = `
            <h1>
            <font color="Black" face="Sans-serif">
            Tarea Realizada
            </h1>
            <br></br>
            <p><font color="Black" face="Sans-serif">
            La tarea de llegada al punto de cargio ya fue realizada</p>
            <br></br>`;

    let contentImagen = `<table style="width: 100%; border-collapse: collapse; border-spacing: 0">
        <tr>
          <td style="width: 35%; padding-top: 10px; padding-bottom: 10px">
            <img
              src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/deltaEmail.png"
              width="100%"
            />
          </td>
          <td style="width: 65%; padding-top: 10px; padding-bottom: 10px">
            <div style="font-weight: bold; font-size: 16px; color: black; font-family: Sans-serif">${fullNameOperator}</div>
            <div style="font-weight: bold; font-size: 14px; color: orange; margin-top: 5px; font-family:Sans-serif">
            ${inCharge} - DeltaX
            </div>
            <div style="display: flex; flex-direction: row; align-items: center; margin-top: 5px">
              <div style="margin-right: 10px; align-items: 'center'">
                <img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/telefonoEmail.png"
                  alt="Telefono"
                  height="18"
                />
              </div style="font-family: Sans-serif">
              <a href="https://wa.me/+59170289279">${fullPhone}</a>
              </div>
            <div style="display: flex; flex-direction: row; align-items: center; margin-top: 5px">
              <div style="margin-right: 10px; align-items: 'center'">
                <img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/correoEmail.png"
                  alt="Correo"
                  height="18"
                />
              </div>
              <a href="mailto:${emailOperator}" style="font-family: Sans-serif">${emailOperator}</a>
            </div>
            <div
              style="
                display: flex;
                flex-direction: row;
                align-items: center;
                margin-top: 5px;
                margin-bottom: 5px;
                color: Black;
              "
            >
              <div style="margin-right: 10px; align-items: 'center'">
                <img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/ubicacionEmail.png"
                  alt="Ubicacion"
                  height="18"
                />
              </div>
              C/ Teniente Vega Nº360, Santa Cruz de la Sierra</div>
            <div>
              <a
                href="https://www.facebook.com/deltax.la"
                style="margin-right: 10px;"
              >
                <img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/facebookEmail.gif"
                  alt="Facebook"
                  height="24"
                  width="24"
              /></a>
              <a href="https://www.instagram.com/deltax.la" style="margin-right: 10px"
                ><img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/instagramEmail.gif"
                  alt="Instagram"
                  height="24"
                  width="24"
              /></a>
              <a href="https://www.linkedin.com/company/delta-express-la" style="margin-right: 10px"
                ><img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/linkedinEmail.gif"
                  alt="Linkedin"
                  height="24"
                  width="24"
              /></a>
              <a href="https://www.deltax.la"
                ><img
                  src="https://news-new-brdona3lw8k2.s3.amazonaws.com/logosEmail/websiteEmail.gif"
                  alt="Enlace"
                  height="24"
                  width="24"
              /></a>
            </div>
          </td>
        </tr>
      </table>`;

    let gracias = `<h4><font color="Black" face="Sans-serif">Gracias por confiar en nuestros servicios.</h4>`;

    let newContentHTML = contentHTML + gracias + contentImagen;
    let transporter = nodemailer.createTransport({
      host: 'email-smtp.us-east-1.amazonaws.com',
      port: 587,
      secure: false,
      auth: {
        user: 'AKIAVAV2QMF55WQEOKV3',
        pass: 'BLGNnPUBY+5ck0lY8Qd8dH5uou7nGG/dcmii/yUPrv8J',
      },
    });

    let info = await transporter.sendMail({
      from: `"DeltaX" <info@deltaxla.com>`,
      to: emailCorreo,
      // to: 'yerson_fuentes2012@hotmail.com',
      //cc: 'yerson.fuentes2012@gmail.com',
      // cc: operatorEmail,
      subject: `DeltaX: Operación`,
      //text: message,
      html: newContentHTML,
    });
    console.log('info====>>', info);
    if (info.messageId) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Correo enviado exitosamente',
        }),
      };
    } else {
      return {
        statusCode: 500,
        headers,
        body: JSON.stringify({
          message: 'Error: Correo no enviado',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const notificationId = event?.pathParameters?.notificationId;
    console.log('notificationId===>>>', notificationId);

    let notificationOne = await Notification.findOne({ _id: notificationId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        notification: notificationOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

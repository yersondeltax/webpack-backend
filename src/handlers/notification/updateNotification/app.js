const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    let updateDate = moment().format();
    console.log('event ==> ', event);
    console.log('nuevooooo evento');
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const notificationId = event?.pathParameters?.notificationId;
    console.log('notificationId===>>>', notificationId);

    let notificationUpdate = await Notification.updateOne(
      { _id: notificationId },
      {
        $set: { ...body },
      }
    );
    console.log('notificationUpdate===>>>', notificationUpdate);
    if (notificationUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Notificación actualizada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

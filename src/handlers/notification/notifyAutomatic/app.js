var AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const Notification = require('../../../models/notification');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // console.log('entro en disprador');
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });
    await dbConnection(process.env.dataBaseConnection);
    let notifications = [];
    notifications = await Notification.find({ sent: false });
    // console.log('notifications====>>>', notifications);
    let notificationIds = [];
    for (const dataNotification of notifications) {
      let notificationId = dataNotification._id;
      notificationIds.push(notificationId);
      // if (dataNotification?.userId) {
      // let userId = dataNotification?.userId || '';
      // let dataUser = await User.findById(userId);
      // if (dataUser?.device) {
      const token = dataNotification?.tokenUser || '';
      // console.log('TOKEN => ', token);
      let idNotification = dataNotification?._id || '';
      let mensaje = dataNotification?.message || '';
      let typeNotification = dataNotification?.type || '';
      let travelId = dataNotification?.travelId || '';
      let loadingOrderId = dataNotification?.loadingOrderId || '';
      let taskId = dataNotification?.taskId || '';
      let operationId = dataNotification?.operationId || '';
      let checkPointId = dataNotification?.checkPointId || '';
      let postulationId = dataNotification?.postulationId || '';
      let payloadNotification = {
        default: 'default',
        GCM: {
          notification: {
            title: 'DeltaX',
            body: mensaje,
            sound: 'default',
          },
          data: {
            id: idNotification,
            type: typeNotification,
            travelId,
            loadingOrderId: loadingOrderId,
            taskId: taskId,
            operationId: operationId,
            checkPointId: checkPointId,
            postulationId: postulationId,
          },
        },
      };
      payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
      payloadNotification = JSON.stringify(payloadNotification);
      const params_sns = {
        TargetArn: token,
        Message: payloadNotification,
        MessageStructure: 'json',
      };
      const publishTextPromise2 = await sns_new
        .publish(params_sns)
        .promise()
        .then(
          function (data) {
            /* process the data */
            // console.log('dataaa', data);
            return data;
          },
          function (error) {
            // console.log('errorrrr', error);
            return error;
            /* handle the error */
          }
        );
      // console.log('publishTextPromise2 => ', publishTextPromise2);
      // }
      // }
    }

    let notificationUpdate = await Notification.updateMany(
      { _id: { $in: notificationIds } },
      {
        $set: { sent: true },
      },
      { multi: true }
    );
    console.log('notificationUpdate====>>', notificationUpdate);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        notifications,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

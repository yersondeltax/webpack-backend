const { dbConnection } = require('../../../database/config');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');

exports.lambdaHandler = async (event, context) => {
  //nuevo nuevoooo 1234156
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const featuresTransportUnits = await FeaturesTransportUnit.find().sort({
      'account.createDate': 1,
    });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        featuresTransportUnits,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

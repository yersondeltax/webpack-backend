const { dbConnection } = require('../../../database/config');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    const featuresTransportUnitDB = new FeaturesTransportUnit(data);
    const featuresTransportUnitDBSave = await featuresTransportUnitDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Caracteristica creado exitosamente',
        featuresTransportUnit: featuresTransportUnitDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

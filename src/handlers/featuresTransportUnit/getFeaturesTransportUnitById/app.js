const { dbConnection } = require('../../../database/config');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');

exports.lambdaHandler = async (event, context) => {
  //nuevo nuevoooo 1234156
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const featuresTransportUnitId = event.pathParameters.featuresTransportUnitId;
    console.log('featuresTransportUnitId===>>>', featuresTransportUnitId);

    let featuresTransportUnit = await FeaturesTransportUnit.findOne({
      _id: featuresTransportUnitId,
    });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        featuresTransportUnit: featuresTransportUnit,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const featuresTransportUnitId = event.pathParameters.featuresTransportUnitId;
    console.log('featuresTransportUnitId===>>>', featuresTransportUnitId);
    let featuresTransportUnitUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      featuresTransportUnitUpdate = await FeaturesTransportUnit.updateOne(
        { _id: featuresTransportUnitId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      featuresTransportUnitUpdate = await FeaturesTransportUnit.updateOne(
        { _id: featuresTransportUnitId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('featuresTransportUnitUpdate===>>>', featuresTransportUnitUpdate);
    if (featuresTransportUnitUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Caracteristicas ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

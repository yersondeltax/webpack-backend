const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const Company = require('../../../models/company');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const userId = event.pathParameters.userId;
    const userWithCompany = await User.findOne({ userId })
      .then(async (userFind) => {
        const companyFind = await Company.findById(userFind.profile.companyId);
        return { ...userFind._doc, company: companyFind };
      })
      .catch((err) => {
        console.log('err ==>', err);
        return null;
      });

    if (!userWithCompany) {
      return {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          user: userWithCompany,
        }),
      };
    } else {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          user: userWithCompany,
        }),
      };
    }
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

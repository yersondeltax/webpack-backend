const AWS = require('aws-sdk');
const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
// const UserPoolId = 'us-east-1_Rhq7ZwU3v';
const UserPoolId = process?.env?.IdGrupoCognitoOperator || '';

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const userId = event?.pathParameters?.userId || '';
    console.log('userId===>>>', userId);
    const userCognito = await User.findById(userId);
    const userCognitoId = userCognito.userId || '';

    console.log('UserPoolId===>>', UserPoolId);
    const arrayNameUserPool = UserPoolId.split('/');
    console.log('arrayNameUserPool', arrayNameUserPool);
    let nameUserPool = arrayNameUserPool[1];
    console.log('nameUserPool==>>', nameUserPool);
    let userUpdate;
    let mensaje = 'desabilitado';
    let params = {
      Username: userCognitoId,
      UserPoolId: nameUserPool,
    };
    if (enable) {
      mensaje = 'habilitado';
      const newUserCognito = await new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.adminEnableUser(params, function (err, data) {
          if (err) {
            // callback.callbackRegisterComplete(err);
            console.log('ERR ==>', err);
            reject({ ok: false, error: { err } });
          } else {
            console.log('DATA ==>', data);
            resolve({ ok: true, ...data });
            // callback.callbackRegisterComplete(null); //
          }
        });
      });
      console.log('newUserCognito====>>>', newUserCognito);
      if (newUserCognito.ok) {
        userUpdate = await User.updateOne(
          { _id: userId },
          {
            $set: { 'account.enable': enable, 'account.enableDate': date },
          }
        );
      } else {
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            message: 'Error al habilitar usuario.',
          }),
        };
      }
    } else {
      mensaje = 'desabilitado';
      const newUser = await new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.adminDisableUser(params, function (err, data) {
          if (err) {
            // callback.callbackRegisterComplete(err);
            console.log('ERR ==>', err);
            reject({ ok: false, error: { err } });
          } else {
            console.log('DATA ==>', data);
            resolve({ ok: true, ...data });
            // callback.callbackRegisterComplete(null); //
          }
        });
      });
      console.log('newUser====>>>', newUser);
      if (newUser.ok) {
        userUpdate = await User.updateOne(
          { _id: userId },
          {
            $set: { 'account.enable': enable, 'account.disableDate': date },
          }
        );
      } else {
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            message: 'Error al desabilitar usuario.',
          }),
        };
      }
    }

    console.log('userUpdate===>>>', userUpdate);
    if (userUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Operador ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

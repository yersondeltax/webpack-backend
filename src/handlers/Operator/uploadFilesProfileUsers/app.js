var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const s3 = new AWS.S3();

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');

let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ===>', event);
    const elemento = event.Records[0];
    console.log('s3 ===>', elemento.s3);
    console.log('key====>>', elemento.s3.object.key);
    let keyPhoto = elemento.s3.object.key;
    console.log('keyPhoto====>>', keyPhoto);
    // let newKey = keyPhoto.replace('%3A', ':');
    // console.log('newkey===>>', newKey);
    let bucketName = elemento.s3.bucket.name;
    console.log('bucketName======>', bucketName);
    // const paramses = { Bucket: bucketName, Key: newKey };
    const paramses = { Bucket: bucketName, Key: keyPhoto };
    console.log('parametros====>>', paramses);
    const metaData = await s3.headObject(paramses).promise();
    console.log('metaData====>>', metaData);
    let http = 'https://';
    // let namebucket = 'filestasksdelta-filestaskbucket-npwj0mzw291v';
    // let estatico = '.s3.us-east-2.amazonaws.com/';
    let estatico = '.s3.amazonaws.com/';
    // let path = http + namebucket + estatico + newKey;
    let path = http + bucketName + estatico + keyPhoto;
    let userId = metaData.Metadata.userid || '';
    console.log('path========>>', path);

    const modulesDBSave = await User.updateOne(
      { userId: userId },
      {
        $set: { 'profile.pathPhoto': path },
      }
    );

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Perfil actualizado.',
      }),
    };
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }

  return response;
};

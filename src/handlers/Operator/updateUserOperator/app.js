const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
  try {
    await dbConnection(process.env.dataBaseConnection);
    const userId = event.pathParameters.userId;
    let data = body;
    // const { profile, address } = body;
    // const userUpdated = await User.findOne({ userId })
    //   .exec()
    //   .then((userFind) => {
    //     if (!userFind) return null;
    //     userFind.profile = { ...userFind.profile, ...profile };
    //     userFind.address = { ...userFind.address, ...address };
    //     return userFind.save();
    //   });

    let userUpdated = await User.updateOne(
      { _id: userId },
      {
        $set: data,
      },
      { upsert: true }
    );

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Actualizado exitosamente',
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

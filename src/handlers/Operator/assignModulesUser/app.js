const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');

let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    let idUsers = event.pathParameters.idUsers;
    const data = body.roles;
    const usersDBSave = await User.updateOne(
      { _id: idUsers },
      {
        $set: { roles: data },
      }
      // { upsert: true }
    );

    if (usersDBSave.nModified > 0) {
      response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Asignación de modulos correcta.',
        }),
      };
    } else {
      response = {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo asignar los modulos',
        }),
      };
    }
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }

  return response;
};

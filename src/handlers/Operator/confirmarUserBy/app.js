const AWS = require('aws-sdk');
const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
// const UserPoolId = 'us-east-1_7JlBMUgy1';
const UserPoolId = process?.env?.IdGrupoCognitoCarrier || '';

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    const numberPhone = event?.pathParameters?.numberPhone || '';
    console.log('numberPhone===>>>', numberPhone);
    // console.log('userId===>>>', userId);
    const userCognito = await User.findOne({ 'auth.phone': numberPhone });
    // const userCognitoId = userCognito.userId || '';
    const userCognitoId = userCognito?.userId || '';

    console.log('UserPoolId===>>', UserPoolId);
    const arrayNameUserPool = UserPoolId.split('/');
    console.log('arrayNameUserPool', arrayNameUserPool);
    let nameUserPool = arrayNameUserPool[1];
    console.log('nameUserPool==>>', nameUserPool);

    let params = {
      UserAttributes: [
        {
          Name: 'phone_number_verified',
          Value: 'true',
        },
      ],
      Username: userCognitoId,
      UserPoolId: nameUserPool,
    };
    console.log('params===>>>', params);

    mensaje = 'habilitado';
    const newUserCognito = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
        if (err) {
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
        }
      });
    });
    console.log('newUserCognito====>>>', newUserCognito);

    let params2 = {
      Username: userCognitoId,
      UserPoolId: nameUserPool,
    };
    console.log('params2===>>>', params2);

    const newUserCognito2 = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.adminConfirmSignUp(params2, function (err, data) {
        if (err) {
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
        }
      });
    });
    console.log('newUserCognito2====>>>', newUserCognito2);
    // if (newUserCognito.ok) {
    let userUpdate = await User.updateOne(
      { userId: userCognitoId },
      {
        $set: { 'auth.confirmed': true, isSpecial: true },
      }
    );

    console.log('userUpdate===>>>', userUpdate);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: `Usuaio confirmado.`,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

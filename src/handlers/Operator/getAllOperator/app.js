const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const CompanyOperator = require('../../../models/company');
const CompanyClient = require('../../../models/companyClient');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    let companyId = event?.queryStringParameters?.companyId || '';
    console.log('companyId====>>', companyId);
    let userId = event?.pathParameters?.userId || '';
    console.log('userId====>>', userId);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    let type = event?.queryStringParameters?.type || 'OPERATOR';
    console.log('type===>>', type);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let users = [];
    // let company = {};

    if (type == 'OPERATOR') {
      console.log('entro en true de compania');
      if (page || size) {
        console.log('entro por verdadero page');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
      }
      let objectOrder = {};
      if (order) {
        objectOrder[`${order}`] = 1;
      }
      let objectFilter = {};
      if (filter) {
        objectFilter = {
          'account.enable': true,
        };
      }

      let objectSearch;
      if (search) {
        objectSearch = {
          'profile.firstName': new RegExp(search, 'i'),
        };
      }
      users = await User.find({
        type: 'OPERATOR',
        'profile.companyId': companyId,
        _id: {
          $ne: userId,
        },
        ...objectFilter,
        ...objectSearch,
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort(objectOrder);
      let total;
      if (page || size) {
        total = await User.find({
          type: 'OPERATOR',
          'profile.companyId': companyId,
          _id: {
            $ne: userId,
          },
          ...objectFilter,
          ...objectSearch,
        }).countDocuments();
        total = parseInt(total, 10);
        totalPage = total / sizeNumber;
        totalPage = Math.ceil(totalPage);
      }
      response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          totalPage,
          users,
        }),
      };
    } else {
      if (page || size) {
        console.log('entro por false paginacion');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
      }
      users = await User.find({
        type: 'CLIENT',
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort({ 'account.createDate': 1 });
      let totalClient;
      if (page || size) {
        totalClient = await User.find({
          type: 'CLIENT',
        }).countDocuments();
        totalClient = parseInt(totalClient, 10);
        totalPage = totalClient / sizeNumber;
        totalPage = Math.ceil(totalPage);
      }
      let arrayUsersClient = [];
      for (const userClient of users) {
        let companyClientId = userClient?.profile?.companyId || '';
        let companyClientData = await CompanyClient.findOne({ _id: companyClientId });
        let dataUser = {
          ...userClient._doc,
          company: companyClientData,
        };
        arrayUsersClient.push(dataUser);
      }
      users = arrayUsersClient;
      response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          totalPage,
          users,
        }),
      };
    }

    // const users = await User.find({
    //   type: 'OPERATOR',
    //   'profile.companyId': companyId,
    //   _id: {
    //     $ne: userId,
    //   },
    // });
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

const { dbConnection } = require('../../../database/config');
const Place = require('../../../models/place');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    await dbConnection(process.env.dataBaseConnection);
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }
    const places = await Place.find({ ...objectFilter }).sort({
      'account.createDate': 1,
    });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        places,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Place = require('../../../models/place');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const placeId = event.pathParameters.placeId;
    console.log('placeId===>>>', placeId);

    let place = await Place.findOne({ _id: placeId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        place: place,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

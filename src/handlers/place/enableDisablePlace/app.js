const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const place = require('../../../models/place');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const placeId = event.pathParameters.placeId;
    console.log('placeId===>>>', placeId);
    let placeUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      placeUpdate = await place.updateOne(
        { _id: placeId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      placeUpdate = await place.updateOne(
        { _id: placeId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('placeUpdate===>>>', placeUpdate);
    if (placeUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Lugar ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

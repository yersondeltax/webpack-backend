const { dbConnection } = require('../../../database/config');
const Place = require('../../../models/place');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const placeId = event.pathParameters.placeId;
    console.log('placeId===>>>', placeId);
    const statesId = event.pathParameters.statesId;
    console.log('statesId===>>>', statesId);

    let place = await Place.findOne({
      _id: placeId,
      'states._id': statesId,
    });
    console.log('place====>>', place);

    let states = place.states || [];

    let cities = [];

    for (const state of states) {
      let idState = state._id;
      let objectState = {
        _id: idState,
      };
      let objectoNuevo = { _id: statesId };
      let nuevo22 = JSON.stringify(objectState) === JSON.stringify(objectoNuevo);
      if (nuevo22) {
        cities = state.cities;
        break;
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        cities: cities,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

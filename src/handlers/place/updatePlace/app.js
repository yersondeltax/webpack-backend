const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Place = require('../../../models/place');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const placeId = event.pathParameters.placeId;
    console.log('placeId===>>>', placeId);

    let updateDate = moment().format();
    let placeUpdate = await Place.updateOne(
      { _id: placeId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('placeUpdate===>>>', placeUpdate);
    if (placeUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Dato actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

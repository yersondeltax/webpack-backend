const { dbConnection } = require('../../../database/config');
const Place = require('../../../models/place');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = { ...body, companyId: companyId };
    const placeDB = new Place(data);
    const placeDBSave = await placeDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Creado exitosamente',
        place: placeDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

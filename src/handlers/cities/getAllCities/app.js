const { dbConnection } = require('../../../database/config');
const Cities = require('../../../models/cities');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let cities = [];

    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectOrder = {};
    if (order) {
      objectOrder[`${order}`] = 1;
    }
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }

    let objectSearch;
    if (search) {
      objectSearch = {
        name: new RegExp(search, 'i'),
      };
    }
    cities = await Cities.find({
      ...objectFilter,
      ...objectSearch,
    })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort({ 'account.createDate': 1 });
    // .sort(objectOrder);
    let total;
    if (page || size) {
      console.log('entro aca');
      total = await Cities.find({
        ...objectFilter,
        ...objectSearch,
      }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    // const cities = await Cities.find();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        cities,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

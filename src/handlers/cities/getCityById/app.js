const { dbConnection } = require('../../../database/config');
const Cities = require('../../../models/cities');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const citiesId = event?.pathParameters?.citiesId || '';
    console.log('citiesId===>>>', citiesId);

    let cities = await Cities.findOne({ _id: citiesId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        cities: cities,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Cities = require('../../../models/cities');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    console.log('event====>>', event);
    // let name = event?.queryStringParameters?.name;
    // console.log('name====>>', name);
    await dbConnection(process.env.dataBaseConnection);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let objectFilter = [];
    if (filter) {
      objectFilter.push(true);
    } else {
      objectFilter.push(true);
      objectFilter.push(false);
    }
    const cities = await Cities.aggregate([
      {
        $match: {
          'account.enable': {
            $in: objectFilter,
          },
        },
      },
      { $sort: { 'account.createDate': 1 } },
      {
        $project: {
          name: '$name',
          fullName: { $concat: ['$name', ', ', '$statesName', ', ', '$countryName'] },
        },
      },
      { $sort: { fullName: 1 } },
    ]);
    console.log('cities====>>', cities);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        cities: cities,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

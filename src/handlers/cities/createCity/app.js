const { dbConnection } = require('../../../database/config');
const Cities = require('../../../models/cities');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = { ...body };
    const citiesDB = new Cities(data);
    const citiesDBSave = await citiesDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Creado exitosamente',
        city: citiesDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

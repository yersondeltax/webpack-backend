const { dbConnection } = require('../../../database/config');
// const { APIGatewayProxyResult, APIGatewayProxyEvent } = require('aws-lambda');
// import  from 'aws-lambda';
const CompanyClient = require('../../../models/companyClient');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let companiesClient = [];
    let totalPage = 1;
    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectOrder = {};
    if (order) {
      objectOrder[`${order}`] = 1;
    }
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }

    let objectSearch;
    if (search) {
      objectSearch = {
        name: new RegExp(search, 'i'),
      };
    }

    companiesClient = await CompanyClient.find({
      companyId: companyId,
      ...objectFilter,
      ...objectSearch,
    })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort(objectOrder);
    let total;
    if (page || size) {
      total = await CompanyClient.find({
        companyId: companyId,
        ...objectFilter,
        ...objectSearch,
      }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }
    return {
      body: JSON.stringify({
        totalPage,
        companiesClient,
      }),
      statusCode: 200,
      headers,
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

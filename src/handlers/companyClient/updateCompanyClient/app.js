const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const CompanyClient = require('../../../models/companyClient');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  console.log('event====>>', event);
  const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyClientId = event?.pathParameters?.companyClientId;
    let updateDate = moment().format();
    const companyClientDBSave = await CompanyClient.updateOne(
      { _id: companyClientId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Compañia cliente actualizado exitosamente',
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

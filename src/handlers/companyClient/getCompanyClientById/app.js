const { dbConnection } = require('../../../database/config');
const CompanyClient = require('../../../models/companyClient');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyClientId = event?.pathParameters?.companyClientId;
    const companyClient = await CompanyClient.findOne({ _id: companyClientId });
    if (!companyClient) {
      return {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          companyClient,
        }),
      };
    } else {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          companyClient,
        }),
      };
    }
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

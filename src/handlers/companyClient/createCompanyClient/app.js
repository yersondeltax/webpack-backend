const { dbConnection } = require('../../../database/config');
const CompanyClient = require('../../../models/companyClient');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  console.log('event=====>>>', event);
  console.log('context=====>>>', context);
  let companyId = event?.queryStringParameters?.companyId;
  console.log('companyId====>>', companyId);
  const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
  console.log('body ==> ', body);
  try {
    await dbConnection(process.env.dataBaseConnection);

    let data = { ...body, companyId: companyId };
    const companyClientDB = new CompanyClient(data);
    const companyClientDBSave = await companyClientDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Compañia cliente creada exitosamente',
        companyClient: companyClientDBSave,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

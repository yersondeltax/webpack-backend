const { dbConnection } = require('../../../database/config');
const Company = require('../../../models/company');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
  console.log('body ==> ', body);
  try {
    await dbConnection(process.env.dataBaseConnection);

    const companyDB = new Company(body);
    const companyDBSave = await companyDB.save();

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Compañia creada exitosamente',
        company: companyDBSave,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

const { dbConnection } = require('../../../database/config');
// const { APIGatewayProxyResult, APIGatewayProxyEvent } = require('aws-lambda');
// import  from 'aws-lambda';
const Company = require('../../../models/company');
const Modules = require('../../.../../../models/modules');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyId = event?.pathParameters?.companyId;
    const company = await Company.findOne({ _id: companyId }).select('access');
    if (!company) {
      return {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          company,
        }),
      };
    }
    console.log('company===>>', company);
    let accessArray = company?.access || [];
    let moduleArray = [];
    for (const moduleCompany of accessArray) {
      let moduleId = moduleCompany.modulesId;
      console.log('moduleId====>>', moduleId);
      const module = await Modules.findOne({ _id: moduleId });
      console.log('module====>>', module);
      moduleArray.push(module);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        modules: moduleArray,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

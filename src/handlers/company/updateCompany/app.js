const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Company = require('../../../models/company');
let response;

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyId = event?.pathParameters?.companyId;
    let updateDate = moment().format();
    const companyDBSave = await Company.updateOne(
      { _id: companyId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      },
      { upsert: true }
    );

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Compañia Actualizado exitosamente',
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

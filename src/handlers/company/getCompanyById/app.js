const { dbConnection } = require('../../../database/config');
const Company = require('../../../models/company');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyId = event?.pathParameters?.companyId;
    const company = await Company.findOne({ _id: companyId });
    if (!company) {
      response = {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          company,
        }),
      };
    } else {
      response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          company,
        }),
      };
    }
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

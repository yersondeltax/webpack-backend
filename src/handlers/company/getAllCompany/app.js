const { dbConnection } = require('../../../database/config');
// const { APIGatewayProxyResult, APIGatewayProxyEvent } = require('aws-lambda');
// import  from 'aws-lambda';
const Company = require('../../../models/company');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
///prueba
///prueba
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companies = await Company.find();

    response = {
      body: JSON.stringify({
        companies,
      }),
      statusCode: 200,
      headers,
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

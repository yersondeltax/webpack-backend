const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    let updateDate = moment().format();
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const transportUnitId = event.pathParameters.transportUnitId;
    await dbConnection(process.env.dataBaseConnection);
    let newData = body;
    // let row = newData.row || {};
    let plate = newData?.plate;
    if (plate) {
      let transportUnitPlate = [];
      transportUnitPlate = await TransportUnit.find({
        plate: plate,
      });
      if (transportUnitPlate.length > 0) {
        let objectNewId = { _id: transportUnitId };
        let objectNew = { _id: transportUnitPlate[0]._id };
        let comparation = JSON.stringify(objectNewId) === JSON.stringify(objectNew);

        console.log('transportUnitPlate======>', transportUnitPlate);

        if (
          transportUnitPlate.length >= 2 ||
          (transportUnitPlate.length == 1 && comparation == false)
        ) {
          return {
            statusCode: 400,
            headers,
            body: JSON.stringify({
              message: 'La placa ingresada ya existe, comuníquese con nuestras operadoras.',
            }),
          };
        }
      }
    }

    // row = { ...row, updateDate: updateDate };

    const transportUnitNew = await TransportUnit.findOneAndUpdate(
      {
        _id: transportUnitId,
      },
      { $set: { ...newData, 'row.updateDate': updateDate } },
      { new: true }
    );
    let percentage = 0;
    let data = '';
    let featuresTransportUnitsCount = await FeaturesTransportUnit.find({
      requireForCarrier: true,
    }).countDocuments();
    console.log('featuresTransportUnitsCount===>>', featuresTransportUnitsCount);
    if (transportUnitNew.plate) percentage++;
    if (transportUnitNew.color) percentage++;
    if (transportUnitNew.year) percentage++;
    if (transportUnitNew.brand) percentage++;
    if (transportUnitNew.typeTransportUnit) percentage++;
    if (transportUnitNew.features) {
      for (const value of transportUnitNew.features) {
        const featuresTransportUnits = await FeaturesTransportUnit.find({
          requireForCarrier: true,
        });
        data = value.featuresTransportUnitId || '';
        for (const element of featuresTransportUnits) {
          let nuevo = {
            id: element?._id || '',
          };
          let nuevoElemento = {
            id: data,
          };
          let comparation = JSON.stringify(nuevo) === JSON.stringify(nuevoElemento);
          if (comparation) {
            percentage++;
          }
        }
      }
    }
    if (transportUnitNew.engine) {
      if (transportUnitNew.engine.engine) percentage++;
      if (transportUnitNew.engine.fuelType) percentage++;
    }
    if (transportUnitNew.resources) {
      if (transportUnitNew.resources.photo) {
        if (transportUnitNew.resources.photo.length > 0) percentage++;
      }
      if (transportUnitNew.resources.photoRuat) percentage++;
    }
    // if (transportUnitNew.owner) {
    //   if (transportUnitNew.owner.length > 0) percentage++;
    // }
    let div = 9 + featuresTransportUnitsCount;
    percentage = percentage / div;

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'unidad de transporte actualizada.',
        transportUnit: { ...transportUnitNew._doc, advancePercentage: percentage },
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

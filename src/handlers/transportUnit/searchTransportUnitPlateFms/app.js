const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Travel = require('../../../models/travel');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    console.log('event====>>', event);
    let plate = event?.queryStringParameters?.plate;
    let type = event?.queryStringParameters?.type || 'one';
    console.log('plate====>>', plate);
    await dbConnection(process.env.dataBaseConnection);
    const transportUnits = await TransportUnit.aggregate([
      {
        $project: {
          plate: '$plate',
          owner: '$owner',
          drivers: '$drivers',
        },
      },
      {
        $match: {
          plate: new RegExp(plate, 'i'),
        },
      },
    ]);
    console.log('transportUnits====>>', transportUnits);

    const arrayTransportIds = [];
    for (const transportUnit of transportUnits) {
      let userId = transportUnit?.drivers[transportUnit.drivers.length - 1]?.userId || null;
      let user = await User.findById(userId);
      console.log('user=====>>>', user);
      let objectoNuevo = {};
      let comparation = JSON.stringify(user) === JSON.stringify(objectoNuevo);
      console.log('comparation=====>>>', comparation);
      let usersTransportUnit = {};
      if (comparation || user == null) {
        user = {};
      }

      let owner = transportUnit?.owner || [];
      let flag = true;

      if (owner.length > 0) {
        let lastOwner = owner.length - 1;
        let dataLastOwner = owner[lastOwner];
        let active = dataLastOwner?.active;
        if (active) {
          flag = false;
        }
      }
      if (flag) {
        let transportUnitIdUser = transportUnit?._id || '';
        let plate = transportUnit?.plate || '';
        let userId = user?._id || '';
        let userFirstName = user?.profile?.firstName || '';
        let userLastName = user?.profile?.lastName || '';
        let userDocumentId = user?.profile?.documentId || '';
        let fullName = userFirstName + ' ' + userLastName;

        usersTransportUnit = {
          user: {
            userId,
            fullName,
            documentId: userDocumentId,
          },
          transportUnitId: transportUnitIdUser,
          plate,
        };
        arrayTransportIds.push(usersTransportUnit);
      }
    }

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        users: arrayTransportIds,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

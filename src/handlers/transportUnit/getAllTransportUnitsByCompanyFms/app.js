const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const companyId = event?.pathParameters?.companyId;
    let active = true;
    const transportUnits = await TransportUnit.find({
      'owner.companyId': companyId,
      'owner.active': active,
    });

    let transportUnitsOwner = [];

    for (const transportUnitOwner of transportUnits) {
      let listOwner = transportUnitOwner?.owner || [];
      if (listOwner.length > 0) {
        let dataOwner = listOwner[listOwner.length - 1];
        let object = {
          companyId: dataOwner?.companyId || '',
          active: dataOwner?.active,
        };
        let comparation =
          JSON.stringify(object) === JSON.stringify({ companyId: companyId, active: active });

        if (comparation) {
          let listDrivers = transportUnitOwner?.drivers || [];
          let userId = null;
          if (listDrivers.length > 0) {
            let dataDrivers = listDrivers[listDrivers.length - 1];
            userId = dataDrivers?.userId || null;
          }
          const user = await User.findById(userId);
          let userTransportUnit = { transportUnit: transportUnitOwner, user };
          transportUnitsOwner.push(userTransportUnit);
        }
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        transportUnits: transportUnitsOwner,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

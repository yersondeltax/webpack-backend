const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;

    const transportUnit = await TransportUnit.findById(transportUnitId);
    let userId = transportUnit.drivers[transportUnit.drivers.length - 1].userId;
    let user = await User.findById(userId);
    let dataUser = {
      _id: user._id || '',
      userId: user.userId || '',
      type: user.type || '',
      auth: user.auth || {},
      profile: user.profile || {},
      address: user.address || {},
    };
    data = { ...transportUnit._doc, user: dataUser };

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        transportUnit: data,
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

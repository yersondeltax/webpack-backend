const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    let updateDate = moment().format();
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const transportUnitId = event.pathParameters.transportUnitId;
    const photoId = event.pathParameters.photoId || '';
    await dbConnection(process.env.dataBaseConnection);
    let transportUnitNew;

    let type = body.type || '';
    switch (type) {
      case 'photoRuat':
        console.log('entro a photoRuat');
        transportUnitNew = await TransportUnit.findOneAndUpdate(
          { _id: transportUnitId },
          {
            $set: { 'resources.photoRuat': '' },
          },
          { new: true }
        );
        break;

      case 'photo':
        console.log('entro a photo');
        transportUnitNew = await TransportUnit.findOneAndUpdate(
          { _id: transportUnitId },
          { $pull: { 'resources.photo': { _id: photoId } } },
          { new: true }
          // { $push: { 'resources.photo': dataPhotoTransport } }
        );
        break;

      default:
        break;
    }

    let percentage = 0;
    let data = '';
    let featuresTransportUnitsCount = await FeaturesTransportUnit.find({
      requireForCarrier: true,
    }).countDocuments();
    if (transportUnitNew.plate) percentage++;
    if (transportUnitNew.color) percentage++;
    if (transportUnitNew.year) percentage++;
    if (transportUnitNew.brand) percentage++;
    if (transportUnitNew.typeTransportUnit) percentage++;
    if (transportUnitNew.features) {
      for (const value of transportUnitNew.features) {
        const featuresTransportUnits = await FeaturesTransportUnit.find({
          requireForCarrier: true,
        });
        data = value.featuresTransportUnitId || '';
        for (const element of featuresTransportUnits) {
          let nuevo = {
            id: element?._id || '',
          };
          let nuevoElemento = {
            id: data,
          };
          let comparation = JSON.stringify(nuevo) === JSON.stringify(nuevoElemento);
          if (comparation) {
            percentage++;
          }
        }
      }
    }
    if (transportUnitNew.engine) {
      if (transportUnitNew.engine.engine) percentage++;
      if (transportUnitNew.engine.fuelType) percentage++;
    }
    if (transportUnitNew.resources) {
      if (transportUnitNew.resources.photo) {
        if (transportUnitNew.resources.photo.length > 0) percentage++;
      }
      if (transportUnitNew.resources.photoRuat) percentage++;
    }
    // if (transportUnitNew.owner) {
    //   if (transportUnitNew.owner.length > 0) percentage++;
    // }
    let div = 9 + featuresTransportUnitsCount;
    percentage = percentage / div;

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Recurso de la unidad de transporte eliminado.',
        transportUnit: { ...transportUnitNew._doc, advancePercentage: percentage },
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    let date = moment().format();
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const transportUnitId = event?.pathParameters?.transportUnitId || null;
    console.log('transportUnitId===>>', transportUnitId);

    const transportUnit = await TransportUnit.findById(transportUnitId);
    console.log('transportUnit===>>', transportUnit);
    let owner = transportUnit?.owner || [];
    console.log('owner===>>', owner);
    let desactive = false;

    if (owner.length > 0) {
      let latestOwner = owner.length - 1;
      let newObject = {};
      newObject[`owner.${latestOwner}.active`] = false;
      newObject[`owner.${latestOwner}.disengagement`] = date;

      console.log('newObject=====>>>', newObject);

      let misappropriateTransportUnit = await TransportUnit.updateOne(
        { _id: transportUnitId },
        { $set: newObject }
      );
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Error no tiene compañia asignada.',
        }),
      };
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'unidad de transporte desadquiridad.',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

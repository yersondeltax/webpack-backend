const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    let userId = body?.drivers[0]?.userId || null;

    let transportUnitAll = [];
    transportUnitAll = await TransportUnit.find({ 'drivers.userId': userId });

    if (transportUnitAll.length > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Ok',
        }),
      };
    }

    const newTransportUnit = new TransportUnit(body);
    const transportUnitSaved = await newTransportUnit.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        transportUnit: transportUnitSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;

    const transportUnit = await TransportUnit.findById(transportUnitId);
    // let advancePercentage;
    // transportUnit.plate ? (advancePercentage.plate = true) : (advancePercentage.plate = false);
    // transportUnit.country
    //   ? (advancePercentage.country = true)
    //   : (advancePercentage.country = false);

    // transportUnit.color ? (advancePercentage.color = true) : (advancePercentage.color = false);

    // transportUnit.year ? (advancePercentage.year = true) : (advancePercentage.year = false);

    // transportUnit.numberOfShafts
    //   ? (advancePercentage.numberOfShafts = true)
    //   : (advancePercentage.numberOfShafts = false);

    // transportUnit.storageCapacity
    //   ? (advancePercentage.storageCapacity = true)
    //   : (advancePercentage.storageCapacity = false);
    let percentage = 0;
    let featuresTransportUnitsCount = await FeaturesTransportUnit.find({
      requireForCarrier: true,
    }).countDocuments();
    let data = '';
    if (transportUnit.plate) percentage++;
    if (transportUnit.color) percentage++;
    if (transportUnit.year) percentage++;
    if (transportUnit.brand) percentage++;
    if (transportUnit.typeTransportUnit) percentage++;
    if (transportUnit.features) {
      for (const value of transportUnit.features) {
        const featuresTransportUnits = await FeaturesTransportUnit.find({
          requireForCarrier: true,
        });
        data = value.featuresTransportUnitId || '';
        for (const element of featuresTransportUnits) {
          let nuevo = {
            id: element?._id || '',
          };
          let nuevoElemento = {
            id: data,
          };
          let comparation = JSON.stringify(nuevo) === JSON.stringify(nuevoElemento);
          if (comparation) {
            percentage++;
          }
        }
      }
    }
    if (transportUnit.engine) {
      if (transportUnit.engine.engine) percentage++;
      if (transportUnit.engine.fuelType) percentage++;
    }
    if (transportUnit.resources) {
      if (transportUnit.resources.photo) {
        if (transportUnit.resources.photo.length > 0) percentage++;
      }
      if (transportUnit.resources.photoRuat) percentage++;
    }
    // if (transportUnit.owner) {
    //   if (transportUnit.owner.length > 0) percentage++;
    // }
    // percentage = percentage / 12;
    let div = 9 + featuresTransportUnitsCount;
    percentage = percentage / div;

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        transportUnit: { ...transportUnit._doc, advancePercentage: percentage },
      }),
    };
  } catch (err) {
    console.log(err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

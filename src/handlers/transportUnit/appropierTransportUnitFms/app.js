const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const TransportUnit = require('../../../models/transportUnit');
const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    let date = moment().format();
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const transportUnitId = event?.pathParameters?.transportUnitId || null;
    await dbConnection(process.env.dataBaseConnection);
    let companyId = body?.companyId || null;
    console.log('transportUnitId===>>', transportUnitId);
    let transportUnitAppropier = {};
    if (companyId) {
      let objectOwner = {
        companyId: companyId,
        assignationDate: date,
        active: true,
      };

      console.log('objectOwner===>>', objectOwner);

      transportUnitAppropier = await TransportUnit.findOneAndUpdate(
        { _id: transportUnitId },
        {
          $push: { owner: objectOwner },
        },
        { new: true }
      );
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Error al apropiarse de unidad.',
        }),
      };
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Unidad de transporte adquirida.',
        transportUnit: transportUnitAppropier,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const loadingOrderStatusId = event.pathParameters.loadingOrderStatusId;
    console.log('loadingOrderStatusId===>>>', loadingOrderStatusId);
    let loadingOrderStatusUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      loadingOrderStatusUpdate = await LoadingOrderStatus.updateOne(
        { _id: loadingOrderStatusId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      loadingOrderStatusUpdate = await LoadingOrderStatus.updateOne(
        { _id: loadingOrderStatusId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('loadingOrderStatusUpdate===>>>', loadingOrderStatusUpdate);
    if (loadingOrderStatusUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Estado de orden de carga ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

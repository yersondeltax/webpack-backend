const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const loadingOrderStatusId = event.pathParameters.loadingOrderStatusId;
    console.log('loadingOrderStatusId===>>>', loadingOrderStatusId);

    let updateDate = moment().format();
    let loadingOrderStatusUpdate = await LoadingOrderStatus.updateOne(
      { _id: loadingOrderStatusId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('loadingOrderStatusUpdate===>>>', loadingOrderStatusUpdate);
    if (loadingOrderStatusUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Estado de orden de carga actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const loadingOrderStatusId = event.pathParameters.loadingOrderStatusId;
    console.log('loadingOrderStatusId===>>>', loadingOrderStatusId);

    let loadingOrderStatus = await LoadingOrderStatus.findOne({ _id: loadingOrderStatusId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        loadingOrderStatus: loadingOrderStatus,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

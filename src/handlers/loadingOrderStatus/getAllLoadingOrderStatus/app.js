const { dbConnection } = require('../../../database/config');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const loadingOrderStatus = await LoadingOrderStatus.find().sort({ order: 1 });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        loadingOrderStatus,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

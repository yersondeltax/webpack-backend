const { dbConnection } = require('../../../database/config');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newLoadingOrderStatus = new LoadingOrderStatus(body);
    const loadingOrderStatusSaved = await newLoadingOrderStatus.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Estado de orden de carga creado exitosamente',
        loadingOrderStatus: loadingOrderStatusSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Modules = require('../../../models/modules');

let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;

    const modulesDB = new Modules(body);
    const modulesDBSave = await modulesDB.save();

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Modulo creado exitosamente',
        modules: modulesDBSave,
      }),
    };
  } catch (err) {
    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }

  return response;
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Modules = require('../../../models/modules');

let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    let idModules = event.pathParameters.idModules;
    const data = body;
    let updateDate = moment().format();
    const modulesDBSave = await Modules.updateOne(
      { _id: idModules },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      },
      { upsert: true }
    );

    if (modulesDBSave.nModified > 0) {
      response = {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Modulo actualizado.',
        }),
      };
    } else {
      response = {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }

  return response;
};

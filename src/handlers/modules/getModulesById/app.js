const { dbConnection } = require('../../../database/config');
const Modules = require('../../../models/modules');

let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    let idModules = event.pathParameters.idModules;
    await dbConnection(process.env.dataBaseConnection);

    let modules = await Modules.findById(idModules);
    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        modules: modules,
      }),
    };
  } catch (err) {
    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }

  return response;
};

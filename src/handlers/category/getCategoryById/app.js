const { dbConnection } = require('../../../database/config');
const Category = require('../../../models/category');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const categoryId = event.pathParameters.categoryId;
    console.log('categoryId===>>>', categoryId);

    let category = await Category.findOne({ _id: categoryId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        category: category,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

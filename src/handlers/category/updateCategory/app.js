const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Category = require('../../../models/category');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const categoryId = event.pathParameters.categoryId;
    console.log('categoryId===>>>', categoryId);
    let updateDate = moment().format();

    let categoryUpdate = await Category.updateOne(
      { _id: categoryId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('categoryUpdate===>>>', categoryUpdate);
    if (categoryUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Categoria actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

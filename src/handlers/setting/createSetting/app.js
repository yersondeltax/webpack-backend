const { dbConnection } = require('../../../database/config');
const Setting = require('../../../models/setting');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    const newSetting = new Setting(body);
    const settingSaved = await newSetting.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Configuración registrada',
        setting: settingSaved,
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Setting = require('../../../models/setting');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let setting = await Setting.find();
    console.log('setting====>>', setting);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Configuración registrada',
        setting: setting,
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

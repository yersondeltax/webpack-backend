const { dbConnection } = require('../../../database/config');
const Setting = require('../../../models/setting');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
const axios = require('axios');
const TOKEN_SERVER =
  'AAAA5uEYbrk:APA91bFBQALc5TEqcaqA4kaxYKbEAbqbB3RqCura575cXjdt9N1Y-vTKhN2ti2qt3ap_517f1grs_usCXUZdsE_oiZKzZa4U7KUVzGXWi0rXMEiHF4xiA1tcXIdEGCXmC85zs1gfpKFo';
const URL_FCM = 'https://fcm.googleapis.com/fcm/send';
const APP_NAME = 'DeltaX.la';

const sendNotification = async (content, data, token) => {
  const body = {
    click_action: 'FLUTTER_NOTIFICATION_CLICK',
  };
  const params = Object.assign(body, data);
  try {
    let resp = await axios({
      url: URL_FCM,
      method: 'POST',
      headers: {
        authorization: `key=${TOKEN_SERVER}`,
        'Content-Type': 'application/json',
      },
      data: {
        to: token,
        notification: {
          title: APP_NAME,
          body: content,
        },
        priority: 'high',
        sound: 'default',
        data: params,
      },
    });
    //console.log('======= REST =======');
    //console.log('DATA NOTIFICATION =====> ', resp.data);
    //return resp.data;
    return resp.success > 0 ? true : false;
  } catch (error) {
    //console.log('ERROR SENDNOTIFICATION ', error);
    return false;
  }
};

exports.lambdaHandler = async (event, context) => {
  try {
    let message = `prueba desde aws.`;
    let dataNotify = {
      id: 12356,
      key: 'GLOBAL',
      content: message,
    };
    await sendNotification(
      message,
      dataNotify,
      'd2Mc8zjySP66LVWaiy0W8r:APA91bGjr7LTIBOZwaaRBbu4DBuojnpUSEJRHBXg0suEh2a96DJ3E5kvgyvyNNYofLsxu3_BHt9AtkmWl-BNtWR4zl3L9mD-obyTCHK8t3XXcr89SyzmLLpCsbDNgArNAsBnm3_xQmg-'
    );

    let message1 = `prueba desde aws para front.`;
    let dataNotify1 = {
      id: 12356,
      key: 'GLOBAL',
      content: message1,
    };
    await sendNotification(
      message1,
      dataNotify1,
      'fpCoMlK7bvRZv5GlBh0KSP:APA91bFmpHgs79fasPr3xk-91UA6-w34Vg0GhkfwMlv_jqfM_-OJ2sI_ktJy-7T_Y5wvy4U8wgob8g5jJXXgljUFTiyTHX3fohAWCgZUWOGhLAwpcdL4fDiZn09AOz6992d2We5kCVsJ'
    );

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'notigicacion enviada',
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

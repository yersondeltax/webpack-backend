const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const TypeTransportUnit = require('../../../models/typeTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const typeTransportUnitId = event.pathParameters.typeTransportUnitId;
    console.log('typeTransportUnitId===>>>', typeTransportUnitId);

    let updateDate = moment().format();
    let typeTransportUnitUpdate = await TypeTransportUnit.updateOne(
      { _id: typeTransportUnitId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('typeTransportUnitUpdate===>>>', typeTransportUnitUpdate);
    if (typeTransportUnitUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Tipo de unidad de transporte actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const TypeTransportUnit = require('../../../models/typeTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const typeTransportUnitId = event.pathParameters.typeTransportUnitId;
    console.log('typeTransportUnitId===>>>', typeTransportUnitId);

    let typeTransportUnit = await TypeTransportUnit.findOne({ _id: typeTransportUnitId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        typeTransportUnit,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

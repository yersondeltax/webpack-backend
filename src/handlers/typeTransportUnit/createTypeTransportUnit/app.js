const { dbConnection } = require('../../../database/config');
const TypeTransportUnit = require('../../../models/typeTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    const typeTransportUnitDB = new TypeTransportUnit(data);
    const typeTransportUnitDBSave = await typeTransportUnitDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tipo de transporte creado exitosamente',
        typeTransportUnit: typeTransportUnitDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

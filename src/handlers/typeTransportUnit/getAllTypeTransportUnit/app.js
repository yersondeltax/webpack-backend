const { dbConnection } = require('../../../database/config');
const TypeTransportUnit = require('../../../models/typeTransportUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const typeTransportUnits = await TypeTransportUnit.find().sort({ 'account.createDate': 1 });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        typeTransportUnits,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const DispatchType = require('../../../models/dispatchType');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = { ...body, companyId: companyId };
    const dispatchTypeDB = new DispatchType(data);
    const dispatchTypeDBSave = await dispatchTypeDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tipo de despacho creado exitosamente',
        dispatchType: dispatchTypeDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

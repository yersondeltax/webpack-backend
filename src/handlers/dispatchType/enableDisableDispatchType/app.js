const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const DispatchType = require('../../../models/dispatchType');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const dispatchTypeId = event.pathParameters.dispatchTypeId;
    console.log('dispatchTypeId===>>>', dispatchTypeId);
    let dispatchTypeUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      dispatchTypeUpdate = await DispatchType.updateOne(
        { _id: dispatchTypeId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      dispatchTypeUpdate = await DispatchType.updateOne(
        { _id: dispatchTypeId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('dispatchTypeUpdate===>>>', dispatchTypeUpdate);
    if (dispatchTypeUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Tipo de despacho ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

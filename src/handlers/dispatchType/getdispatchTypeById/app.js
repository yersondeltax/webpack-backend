const { dbConnection } = require('../../../database/config');
const DispatchType = require('../../../models/dispatchType');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const dispatchTypeId = event.pathParameters.dispatchTypeId;
    console.log('dispatchTypeId===>>>', dispatchTypeId);

    let dispatchType = await DispatchType.findOne({ _id: dispatchTypeId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        dispatchType: dispatchType,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

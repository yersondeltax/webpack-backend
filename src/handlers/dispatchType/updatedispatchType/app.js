const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const DispatchType = require('../../../models/dispatchType');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const dispatchTypeId = event.pathParameters.dispatchTypeId;
    console.log('dispatchTypeId===>>>', dispatchTypeId);

    let updateDate = moment().format();
    let dispatchTypeUpdate = await DispatchType.updateOne(
      { _id: dispatchTypeId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('dispatchTypeUpdate===>>>', dispatchTypeUpdate);
    if (dispatchTypeUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Tipo de despacho actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

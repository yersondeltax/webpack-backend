const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const MeasurementUnit = require('../../../models/measurementUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const measurementUnitId = event.pathParameters.measurementUnitId;
    console.log('measurementUnitId===>>>', measurementUnitId);

    let updateDate = moment().format();
    let measurementUnitUpdate = await MeasurementUnit.updateOne(
      { _id: measurementUnitId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('measurementUnitUpdate===>>>', measurementUnitUpdate);
    if (measurementUnitUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Unidad de medida actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

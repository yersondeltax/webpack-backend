const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const MeasurementUnit = require('../../../models/measurementUnit');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const measurementUnitId = event.pathParameters.measurementUnitId;
    console.log('measurementUnitId===>>>', measurementUnitId);
    let measurementUnitUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      measurementUnitUpdate = await MeasurementUnit.updateOne(
        { _id: measurementUnitId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      measurementUnitUpdate = await MeasurementUnit.updateOne(
        { _id: measurementUnitId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('measurementUnitUpdate===>>>', measurementUnitUpdate);
    if (measurementUnitUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Unidad de medida ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

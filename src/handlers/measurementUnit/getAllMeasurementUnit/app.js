const { dbConnection } = require('../../../database/config');
const MeasurementUnit = require('../../../models/measurementUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let measurementUnits = [];

    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectOrder = {};
    if (order) {
      objectOrder[`${order}`] = 1;
    }
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }

    let objectSearch;
    if (search) {
      objectSearch = {
        name: new RegExp(search, 'i'),
      };
    }
    measurementUnits = await MeasurementUnit.find({
      ...objectFilter,
      ...objectSearch,
    })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort(objectOrder);
    let total;
    if (page || size) {
      total = await MeasurementUnit.find({
        companyId: companyId,
        ...objectFilter,
        ...objectSearch,
      }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    // const measurementUnits = await MeasurementUnit.find({ companyId: companyId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        measurementUnits,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const MeasurementUnit = require('../../../models/measurementUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const measurementUnitId = event.pathParameters.measurementUnitId;
    console.log('loadingOrderStatusId===>>>', measurementUnitId);

    let measurementUnit = await MeasurementUnit.findOne({ _id: measurementUnitId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        measurementUnit: measurementUnit,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

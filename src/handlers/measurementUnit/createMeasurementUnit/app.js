const { dbConnection } = require('../../../database/config');
const MeasurementUnit = require('../../../models/measurementUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    let data = { ...body, companyId: companyId };

    const newMeasurementUnit = new MeasurementUnit(data);
    const measurementUnitSaved = await newMeasurementUnit.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Unidad de medida creada exitosamente',
        measurementUnit: measurementUnitSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

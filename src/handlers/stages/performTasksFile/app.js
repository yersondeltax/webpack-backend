var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const Pusher = require('pusher');

const s3 = new AWS.S3();

const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

const { dbConnection } = require('../../../database/config');
const Stages = require('../../../models/stages');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Notification = require('../../../models/notification');
const Task = require('../../../models/task');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');

// const mongoose = require('mongoose');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let idPrueba = mongoose.Types.ObjectId();
    // console.log('idPrueba====>>>', idPrueba);
    await dbConnection(process.env.dataBaseConnection);
    let snsdss = new AWS.SNS({
      region: 'us-east-1',
    });
    let idNotification = uuidv4().toString();
    console.log('idNotification====>>>', idNotification);

    console.log('event ===>', event);
    const elemento = event.Records[0];
    console.log('s3 ===>', elemento.s3);
    console.log('key====>>', elemento.s3.object.key);
    let keyPhoto = elemento.s3.object.key;
    console.log('keyPhoto====>>', keyPhoto);
    let newKey = keyPhoto.replace('%3A', ':');
    console.log('newkey===>>', newKey);
    let bucketName = elemento.s3.bucket.name;
    console.log('bucketName======>', bucketName);
    const paramses = { Bucket: bucketName, Key: newKey };
    // const paramses = { Bucket: bucketName, Key: keyPhoto };
    console.log('parametros====>>', paramses);
    const metaData = await s3.headObject(paramses).promise();
    console.log('metaData====>>', metaData);
    let http = 'https://';
    // let namebucket = 'filestasksdelta-filestaskbucket-npwj0mzw291v';
    // let estatico = '.s3.us-east-2.amazonaws.com/';
    let estatico = '.s3.amazonaws.com/';
    // let path = http + namebucket + estatico + newKey;
    let path = http + bucketName + estatico + keyPhoto;
    console.log('path========>>', path);

    const stageId = metaData?.Metadata?.stagesid || '';
    const tasksId = metaData?.Metadata?.tasksid || '';
    let loadingOrderId = metaData?.Metadata?.loadingorderid || '';
    let type = metaData?.Metadata?.type || '';
    let dataPath = metaData?.Metadata?.data || '';
    let userId = metaData?.Metadata?.userid || '';
    let transportUnitId = metaData?.Metadata?.transportunitid || '';
    let order = metaData?.Metadata?.order || '';
    let comment = metaData?.Metadata?.comment || false;

    switch (type) {
      case 'task':
        let dateTime = moment().format();
        let data = {};
        if (comment) {
          data = {
            dateAction: dateTime,
            userId: userId,
            comment: comment,
          };
        } else {
          data = {
            dateAction: dateTime,
            userId: userId,
          };
        }

        let dataFile = {
          largePath: path,
          loadDate: dateTime,
        };
        console.log('dataFile=====>>>', dataFile);
        var orderFile = parseInt(order, 10);
        console.log('orderFile=====>>>', orderFile);

        let loadingOrder = await Travel.findOne({ 'loadingOrder.loadingOrderId': loadingOrderId });
        console.log('loadingOrder===>>', loadingOrder);
        let idOperation = loadingOrder?.operationId;
        console.log('loadingOrder===>>', loadingOrder);
        let stagesOneUpdate;
        if (orderFile == 1) {
          console.log('entrooo aca ');
          let array = [];

          array.push(dataFile);
          console.log('paso aca ');
          stagesOneUpdate = await Task.findOneAndUpdate(
            {
              _id: tasksId,
              stagesId: stageId,
            },
            { $set: { file: array }, $push: { action: data } }
          );
          console.log('realizar tarea 1');

          const stagesList = await Stages.find({
            loadingOrderId: loadingOrderId,
          }).select('fullStagesId-_id');

          console.log('stagesList ==> ', stagesList);

          const arrayStagesIds = [];
          for (const stagesIds of stagesList) {
            arrayStagesIds.push(stagesIds._id);
          }
          console.log('arrayStagesIds==>>', arrayStagesIds);
          console.log('realizar tarea 2');
          // primer tarea realizada
          const firstTask = await Task.find({
            stagesId: { $in: arrayStagesIds },
            'action.0': { $exists: true },
          });
          console.log('realizar tarea 3');
          console.log('firstTask===>>', firstTask);
          console.log('firstTask.length===>>', firstTask.length);

          let lengthStatusLoadingOrder = loadingOrder?.loadingOrder?.LoadingOrderStatus || [];
          // pasar en curso la orden de carga
          if (firstTask.length == 1 && lengthStatusLoadingOrder.length < 2) {
            let loadingOrderStatusProgress = await LoadingOrderStatus.findOne({
              order: 2,
            });
            let dataProgressLoadingOrder = {
              statusId: loadingOrderStatusProgress?._id || '',
              name: loadingOrderStatusProgress?.name || '',
              date: dateTime,
              order: loadingOrderStatusProgress?.order,
            };
            console.log('dataProgressLoadingOrder===>>>', dataProgressLoadingOrder);
            const loadingOrderStatus = await Travel.updateOne(
              { 'loadingOrder.loadingOrderId': loadingOrderId },
              { $push: { 'loadingOrder.LoadingOrderStatus': dataProgressLoadingOrder } }
            );
          }
          console.log('realizar tarea 4');

          let stagesOne = await Stages.findOne({ _id: stageId });
          let taskOne = await Task.findOne({ _id: tasksId });

          let order = stagesOne.order;
          let active = stagesOne.active;

          if (taskOne.changeStage && active == 1) {
            console.log('entro a al igual con estado');
            if (order < 4) {
              console.log('order====>>', order);
              const stagesUpadate = await Stages.updateOne(
                {
                  _id: stageId,
                },
                { $set: { active: 0 } }
              );
              const followingStagesUpadate = await Stages.findOneAndUpdate(
                {
                  loadingOrderId: loadingOrderId,
                  order: order + 1,
                },
                { $set: { active: 1 } },
                { new: true }
              );
            }
          }
          console.log('realizar tarea 5');
          // verificar todas las tareas si finalizaron
          const fullTask = await Task.find({
            stagesId: { $in: arrayStagesIds },
          });

          let flag = true;

          for (const tasksTemp of fullTask) {
            console.log('flag====>>>', flag);
            console.log('tasksTemp====>>>', tasksTemp);
            let validationOperator = tasksTemp.validation.operator || false;
            console.log('validationOperator====>>', validationOperator);
            let taskAction = tasksTemp.action || [];
            console.log('taskAction====>>', taskAction);
            if (taskAction.length < 1) {
              console.log('no tiene dato el action');
              flag = false;
              break;
            }
            let approve = taskAction[taskAction.length - 1]?.Validation?.approve || false;
            if (validationOperator && approve == false) {
              console.log('le falta validacion');
              flag = false;
              break;
            }
          }
          console.log('realizar tarea 6');
          console.log('flag=====>>>', flag);

          if (flag) {
            let loadingOrderStatusFinishStatus = await LoadingOrderStatus.findOne({
              order: 3,
            });
            let dataFinishLoadingOrder = {
              statusId: loadingOrderStatusFinishStatus?._id || '',
              name: loadingOrderStatusFinishStatus?.name || '',
              date: dateTime,
              order: loadingOrderStatusFinishStatus?.order,
            };
            const loadingOrderStatusFinish = await Travel.updateOne(
              { 'loadingOrder.loadingOrderId': loadingOrderId },
              {
                $push: { 'loadingOrder.LoadingOrderStatus': dataFinishLoadingOrder },
                'loadingOrder.assignment.assignmentStatus': 0,
              }
            );
          }
        } else {
          stagesOneUpdate = await Task.updateOne(
            {
              _id: tasksId,
              stagesId: stageId,
            },
            { $push: { file: dataFile } }
          );
        }
        let travelId = loadingOrder._id;
        if (orderFile == 1) {
          let listOperation = await Operation.findById(idOperation);
          console.log('listOperation===>>', listOperation);
          let userOperadorId = listOperation?.userOperatorId || null;

          const operatorFind = await User.findById(userOperadorId);
          console.log('operatorFind ==> ', operatorFind);

          let userIdOne = operatorFind?._id || null;
          console.log('userIdOne===>>', userIdOne);

          const token = operatorFind?.device?.pushToken || null;
          console.log('token ==> ', token);

          let messagePublish = 'Archivo subido, tarea realizada.';
          let typeNotification = 'tasks';
          let dataBody = {
            userId: userIdOne,
            tokenUser: token,
            message: messagePublish,
            type: typeNotification,
            travelId: travelId,
            operationId: idOperation,
            loadingOrderId: loadingOrderId,
            taskId: tasksId,
          };

          let newNotification = new Notification(dataBody);
          let notificationSaved = await newNotification.save();
          console.log('notificationSaved===>>', notificationSaved);

          //////////// notificacion pusher /////////

          const pusher = new Pusher({
            appId: '1266998',
            key: 'f8f1c4d725ab089521d6',
            secret: '491a6e8ca25b98ea5455',
            cluster: 'us2',
            useTLS: true,
          });

          await pusher.trigger(`loading-${travelId}`, 'perform-tasks', {
            message: 'Tarea realizada.',
          });

          /////////////////////////////////////////
        }
        break;

      case 'user':
        console.log('entro a actualizar user');
        switch (dataPath) {
          case 'pathPhoto':
            console.log('entro a pathPhoto');
            await User.updateOne(
              { _id: userId },
              {
                $set: { 'profile.pathPhoto': path },
              }
            );
            break;
          case 'photoDocumentIdFront':
            console.log('entro a photoDocumentIdFront');
            await User.updateOne(
              { _id: userId },
              {
                $set: { 'resources.photoDocumentIdFront': path },
              }
            );
            break;
          case 'photoDocumentIdReverse':
            console.log('entro a photoDocumentIdReverse');
            await User.updateOne(
              { _id: userId },
              {
                $set: { 'resources.photoDocumentIdReverse': path },
              }
            );
            break;
          case 'photoLicenseDrivers':
            console.log('entro a photoLicenseDrivers');
            await User.updateOne(
              { _id: userId },
              {
                $set: { 'resources.photoLicenseDrivers': path },
              }
            );
            break;
          default:
            break;
        }
        break;

      case 'transportUnit':
        console.log('entro a actualizar transportUnit');
        switch (dataPath) {
          case 'photoRuat':
            console.log('entro a photoRuat');
            await TransportUnit.updateOne(
              { _id: transportUnitId },
              {
                $set: { 'resources.photoRuat': path },
              }
            );
            break;

          case 'photo':
            console.log('entro a photo');
            let dataPhotoTransport = {
              path: path,
            };
            await TransportUnit.updateOne(
              { _id: transportUnitId },
              { $push: { 'resources.photo': dataPhotoTransport } }
            );
            break;

          default:
            break;
        }
        break;

      default:
        break;
    }

    return event;
  } catch (err) {
    return event;
  }
};

var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const Pusher = require('pusher');

const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

const { dbConnection } = require('../../../database/config');
const Stages = require('../../../models/stages');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Notification = require('../../../models/notification');
const Task = require('../../../models/task');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');

// const mongoose = require('mongoose');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let idPrueba = mongoose.Types.ObjectId();
    // console.log('idPrueba====>>>', idPrueba);
    await dbConnection(process.env.dataBaseConnection);
    let dateTime = moment().format();
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const stageId = event.pathParameters.stageId || '';
    const tasksId = event.pathParameters.tasksId || '';
    let userId = event?.queryStringParameters?.userId || '';
    let loadingOrderId = body.loadingOrderId;
    let approve = body?.approve;
    let comment = body?.comment || false;
    console.log('body==>>', body);
    console.log(stageId);
    console.log(tasksId);
    let data = {};
    if (comment) {
      data = {
        dateAction: dateTime,
        userId: userId,
        Validation: {
          ifValidation: true,
          approve: approve,
        },
        comment: comment,
      };
    } else {
      data = {
        dateAction: dateTime,
        userId: userId,
        Validation: {
          ifValidation: true,
          approve: approve,
        },
      };
    }
    const stagesOneUpdate = await Task.updateOne(
      {
        _id: tasksId,
        stagesId: stageId,
      },
      { $push: { action: data } }
    );

    // let stagesOne = await Stages.findOne({ _id: stageId });
    // console.log('antes de actualizar==>>>', stagesOne);

    // let loadingOrderId = stagesOne.loadingOrderId;

    let loadingOrder = await Travel.findOne({ 'loadingOrder.loadingOrderId': loadingOrderId });

    const stagesList = await Stages.find({
      loadingOrderId: loadingOrderId,
    }).select('fullStagesId-_id');

    console.log('stagesList ==> ', stagesList);

    const arrayStagesIds = [];
    for (const stagesIds of stagesList) {
      arrayStagesIds.push(stagesIds._id);
    }
    console.log('arrayStagesIds==>>', arrayStagesIds);

    const fullTask = await Task.find({
      stagesId: { $in: arrayStagesIds },
    });

    let flag = true;

    for (const tasksTemp of fullTask) {
      console.log('flag====>>>', flag);
      console.log('tasksTemp====>>>', tasksTemp);
      let validationOperator = tasksTemp.validation.operator || false;
      console.log('validationOperator====>>', validationOperator);
      let taskAction = tasksTemp.action || [];
      console.log('taskAction====>>', taskAction);
      if (taskAction.length < 1) {
        console.log('no tiene dato el action');
        flag = false;
        break;
      }
      let approve = taskAction[taskAction.length - 1]?.Validation?.approve || false;
      if (validationOperator && approve == false) {
        console.log('le falta validacion');
        flag = false;
        break;
      }
    }
    console.log('realizar tarea 6');
    console.log('flag=====>>>', flag);

    if (flag) {
      let loadingOrderStatusFinishStatus = await LoadingOrderStatus.findOne({
        order: 3,
      });
      let dataFinishLoadingOrder = {
        statusId: loadingOrderStatusFinishStatus?._id || '',
        name: loadingOrderStatusFinishStatus?.name || '',
        date: dateTime,
        order: loadingOrderStatusFinishStatus?.order,
      };
      console.log('dataProgressLoadingOrder===>>>', dataFinishLoadingOrder);
      const loadingOrderStatusFinish = await Travel.updateOne(
        { 'loadingOrder.loadingOrderId': loadingOrderId },
        {
          $push: { 'loadingOrder.LoadingOrderStatus': dataFinishLoadingOrder },
          'loadingOrder.assignment.assignmentStatus': 0,
        }
      );
    }

    let idTransportUnit = loadingOrder.loadingOrder.assignment.transportUnitId;

    let transportUnits = await TransportUnit.findById(idTransportUnit);

    let transportUnitDrivers = transportUnits.drivers;
    let transportUnitUser = transportUnitDrivers[transportUnitDrivers.length - 1]?.userId || null;
    console.log('transportUnitUser===>>', transportUnitUser);

    const operatorFind = await User.findById(transportUnitUser);
    console.log('operatorFind ==> ', operatorFind);

    let userIdOne = operatorFind?._id || null;
    console.log('userIdOne===>>', userIdOne);

    const token = operatorFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let messagePublish = 'Tarea validada.';
    let typeNotification = 'tasks';
    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      loadingOrderId: loadingOrderId,
      taskId: tasksId,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    //////////// notificacion pusher /////////

    const pusher = new Pusher({
      appId: '1266998',
      key: 'f8f1c4d725ab089521d6',
      secret: '491a6e8ca25b98ea5455',
      cluster: 'us2',
      useTLS: true,
    });

    await pusher.trigger(`loading-${loadingOrder._id}`, 'perform-tasks', {
      message: 'Tarea realizada.',
    });

    /////////////////////////////////////////

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tarea validada.',
      }),
    };
  } catch (err) {
    console.log('error', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

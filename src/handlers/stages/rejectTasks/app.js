var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const Pusher = require('pusher');

const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

const { dbConnection } = require('../../../database/config');
const Stages = require('../../../models/stages');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
const Notification = require('../../../models/notification');
const Task = require('../../../models/task');

// const mongoose = require('mongoose');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let idPrueba = mongoose.Types.ObjectId();
    // console.log('idPrueba====>>>', idPrueba);
    await dbConnection(process.env.dataBaseConnection);
    let dateTime = moment().format();
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const stageId = event.pathParameters.stageId || '';
    const tasksId = event.pathParameters.tasksId || '';
    console.log(stageId);
    console.log(tasksId);

    let userId = event?.queryStringParameters?.userId || '';
    let loadingOrderId = body.loadingOrderId;
    let approve = body?.approve || false;
    let comment = body?.comment || false;
    console.log('body==>>', body);
    console.log(stageId);
    console.log(tasksId);
    let data = {};
    if (comment) {
      data = {
        dateAction: dateTime,
        userId: userId,
        Validation: {
          ifValidation: true,
          approve: approve,
        },
        comment: comment,
      };
    } else {
      data = {
        dateAction: dateTime,
        userId: userId,
        Validation: {
          ifValidation: true,
          approve: approve,
        },
      };
    }

    let array = [];

    const stagesOneUpdate = await Task.findOneAndUpdate(
      {
        _id: tasksId,
        stagesId: stageId,
      },
      { $set: { file: array }, $push: { action: data } }
    );

    let loadingOrder = await Travel.findOne({ 'loadingOrder.loadingOrderId': loadingOrderId });

    // console.log('loadingOrder===', loadingOrder);
    // let idOperation = loadingOrder.operationId;

    let idTransportUnit = loadingOrder.loadingOrder.assignment.transportUnitId;

    let transportUnits = await TransportUnit.findById(idTransportUnit);

    let transportUnitDrivers = transportUnits.drivers;
    let transportUnitUser = transportUnitDrivers[transportUnitDrivers.length - 1]?.userId || null;
    console.log('transportUnitUser===>>', transportUnitUser);

    const operatorFind = await User.findById(transportUnitUser);
    console.log('operatorFind ==> ', operatorFind);

    let userIdOne = operatorFind?._id || null;
    console.log('userIdOne===>>', userIdOne);

    const token = operatorFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let messagePublish = 'Tarea rechazada.';
    let typeNotification = 'rejecttasks';
    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      loadingOrderId: loadingOrderId,
      taskId: tasksId,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    //////////// notificacion pusher /////////

    const pusher = new Pusher({
      appId: '1266998',
      key: 'f8f1c4d725ab089521d6',
      secret: '491a6e8ca25b98ea5455',
      cluster: 'us2',
      useTLS: true,
    });

    await pusher.trigger(`loading-${loadingOrder._id}`, 'perform-tasks', {
      message: 'Tarea realizada.',
    });

    /////////////////////////////////////////

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tarea rechazada.',
      }),
    };
  } catch (err) {
    console.log('error', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../.../../../models/stagesTemplate');
const stages = require('../../../models/stages');
const Task = require('../../../models/task');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    let loadingOrderId = body.loadingOrderId || '';
    await dbConnection(process.env.dataBaseConnection);
    const dispatchTypeId = event.pathParameters.dispatchTypeId;
    const typeServiceId = event.pathParameters.typeServiceId;
    const placeId = event.pathParameters.placeId;

    console.log('dispatchTypeId==>', dispatchTypeId);
    console.log('typeServiceId==>', typeServiceId);
    console.log('placeId==>', placeId);

    const stagesTemplates = await StagesTemplate.find({
      dispatchTypeId: dispatchTypeId,
      typeServiceId: typeServiceId,
      placeId: placeId,
    }).sort({ order: 1 });

    console.log('listas====>>', stagesTemplates);

    let arrayStages = [];

    for (let i = 0; i < stagesTemplates.length; i++) {
      const element = stagesTemplates[i];
      console.log('elemento===>>', element);
      let data;
      if (i == 0) {
        data = {
          ...element._doc,
          loadingOrderId,
          active: 1,
        };
      } else {
        data = {
          ...element._doc,
          loadingOrderId,
          active: 0,
        };
      }
      delete data._id;
      const newStages = new stages(data);
      const stagesSaved = await newStages.save();
      let idStages = stagesSaved?._id || '';
      let tasksStagesTemplate = data?.tasks || [];
      let arrayTasks = [];
      for (const dataTask of tasksStagesTemplate) {
        let objectTask = { ...dataTask._doc, stagesId: idStages };
        // objectTask = objectTask._doc;
        delete objectTask._id;
        const newTask = new Task(objectTask);
        const taskSaved = await newTask.save();
        arrayTasks.push(taskSaved);
      }
      let dataStagesTasks = {
        ...(stagesSaved._doc || ''),
        tasks: arrayTasks || [],
      };
      arrayStages.push(dataStagesTasks);
    }

    // for (let value of stagesTemplates) {
    //   let data = {
    //     ...value._doc,
    //     loadingOrderId,
    //   };
    //   const newStages = new stages(data);
    //   const stagesSaved = await newStages.save();
    //   arrayStages.push(stagesSaved);
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'esta bien',
        stages: arrayStages,
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

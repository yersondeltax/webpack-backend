var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });

const { dbConnection } = require('../../../database/config');
const Stages = require('../../../models/stages');
const Travel = require('../../../models/travel');

// const mongoose = require('mongoose');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let idPrueba = mongoose.Types.ObjectId();
    // console.log('idPrueba====>>>', idPrueba);
    await dbConnection(process.env.dataBaseConnection);

    console.log('entro aca');
    const stageId = event.pathParameters.stagesId || '';
    const tasksId = event.pathParameters.tasksId || '';
    console.log('stageId==>>', stageId);
    console.log('tasksId==>>', tasksId);
    const stagesList = await Stages.findOne({
      _id: stageId,
      tasks: { $elemMatch: { _id: tasksId } },
    });
    console.log('antes de actualizar==>', stagesList);

    let idStages = stagesList._id;
    let loadingOrderId = stagesList.loadingOrderId;
    let arrayTasks = stagesList.tasks;
    console.log('arrayTasks===>>', arrayTasks);
    let taskResponse = {};
    for (const task of arrayTasks) {
      console.log('elemento===>>', task);
      if (task._id == tasksId) {
        taskResponse = task._doc;
      }
    }

    const travelLoadingOrder = await Travel.findOne({
      'loadingOrder.loadingOrderId': loadingOrderId,
    });

    let travelId = travelLoadingOrder._id;

    taskResponse = { ...taskResponse, idStages, loadingOrderId, travelId };

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        task: taskResponse,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../.../../../models/stagesTemplate');
const Stages = require('../../../models/stages');
const Task = require('../../../models/task');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const travels = await Travel.find({
      'row.createDate': { $lt: new Date('2021-09-13') },
    });

    console.log('travels====>>', travels);

    for (const dataTravel of travels) {
      console.log('dataTravel===>>', dataTravel);
      let loadingOrderStatusStages = dataTravel?.loadingOrder?.LoadingOrderStatus || [];
      if (loadingOrderStatusStages.length > 0) {
        let loadingOrderIdStages = dataTravel?.loadingOrder?.loadingOrderId || null;
        let stagesDatas = [];
        stagesDatas = await Stages.find({ loadingOrderId: loadingOrderIdStages });
        for (const data of stagesDatas) {
          let idStages = data?._id || '';
          let tasksStagesTemplate = data?.tasks || [];
          for (const dataTask of tasksStagesTemplate) {
            let arrayAction = [];
            let objectTask = { ...dataTask._doc, stagesId: idStages };
            delete objectTask._id;
            console.log('objectTask===>>>', objectTask);
            let action = objectTask?.action || {};
            let objectoNuevo = {};
            let comparation = JSON.stringify(action) === JSON.stringify(objectoNuevo);
            console.log('comparation=====>>>', comparation);
            if (comparation == false) {
              let objetoTask = {
                dateAction: objectTask?.action?.dateAction || '',
              };
              arrayAction.push(objetoTask);
            }
            let isValidation = objectTask?.action?.dateValidation || false;
            if (isValidation) {
              console.log('entro en validacion');
              let objetoTaskValidation = {
                dateAction: isValidation || '',
                Validation: {
                  ifValidation: true,
                  approve: true,
                },
              };
              arrayAction.push(objetoTaskValidation);
            }
            objectTask = { ...objectTask, action: arrayAction };
            console.log('objectTask1111111===>>>', objectTask);
            const newTask = new Task(objectTask);
            const taskSaved = await newTask.save();
          }
        }
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'cambios en tareas',
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

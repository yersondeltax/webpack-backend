var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const Pusher = require('pusher');

const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

const { dbConnection } = require('../../../database/config');
const Stages = require('../../../models/stages');
const Travel = require('../../../models/travel');
const Operation = require('../../../models/operation');
const User = require('../../../models/user');
const Notification = require('../../../models/notification');
const Task = require('../../../models/task');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');

// const mongoose = require('mongoose');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let idPrueba = mongoose.Types.ObjectId();
    // console.log('idPrueba====>>>', idPrueba);
    await dbConnection(process.env.dataBaseConnection);

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const stageId = event?.pathParameters?.stagesId || '';
    const tasksId = event?.pathParameters?.tasksId || '';
    let userId = event?.queryStringParameters?.userId || '';

    let loadingOrderId = body.loadingOrderId;
    let comment = body?.comment || false;
    let dateTime = moment().format();
    let data = {};
    if (comment) {
      data = {
        dateAction: dateTime,
        userId: userId,
        comment: comment,
      };
    } else {
      data = {
        dateAction: dateTime,
        userId: userId,
      };
    }
    console.log('realizar tarea');
    // se realiza la tarea
    const stagesOneUpdate = await Task.updateOne(
      {
        _id: tasksId,
        stagesId: stageId,
      },
      { $push: { action: data } }
    );
    console.log('realizar tarea 1');
    // buscar todas las etapas de la orden de carga
    const stagesList = await Stages.find({
      loadingOrderId: loadingOrderId,
    }).select('fullStagesId-_id');

    console.log('stagesList ==> ', stagesList);

    const arrayStagesIds = [];
    for (const stagesIds of stagesList) {
      arrayStagesIds.push(stagesIds._id);
    }
    console.log('arrayStagesIds==>>', arrayStagesIds);
    console.log('realizar tarea 2');
    // primer tarea realizada
    const firstTask = await Task.find({
      stagesId: { $in: arrayStagesIds },
      'action.0': { $exists: true },
    });
    console.log('realizar tarea 3');
    console.log('firstTask===>>', firstTask);
    console.log('firstTask.length===>>', firstTask.length);
    let loadingOrder = await Travel.findOne({ 'loadingOrder.loadingOrderId': loadingOrderId });
    let lengthStatusLoadingOrder = loadingOrder?.loadingOrder?.LoadingOrderStatus || [];
    // pasar en curso la orden de carga
    if (firstTask.length == 1 && lengthStatusLoadingOrder.length < 2) {
      let loadingOrderStatusProgress = await LoadingOrderStatus.findOne({
        order: 2,
      });
      let dataProgressLoadingOrder = {
        statusId: loadingOrderStatusProgress?._id || '',
        name: loadingOrderStatusProgress?.name || '',
        date: dateTime,
        order: loadingOrderStatusProgress?.order,
      };
      console.log('dataProgressLoadingOrder===>>>', dataProgressLoadingOrder);
      const loadingOrderStatus = await Travel.updateOne(
        { 'loadingOrder.loadingOrderId': loadingOrderId },
        { $push: { 'loadingOrder.LoadingOrderStatus': dataProgressLoadingOrder } }
      );
    }
    console.log('realizar tarea 4');
    // cambiar de estado si requiere
    let stagesOne = await Stages.findOne({ _id: stageId });
    let taskOne = await Task.findOne({ _id: tasksId });

    let order = stagesOne.order;
    let active = stagesOne.active;

    if (taskOne.changeStage && active == 1) {
      console.log('entro a al igual con estado');
      if (order < 4) {
        console.log('order====>>', order);
        const stagesUpadate = await Stages.updateOne(
          {
            _id: stageId,
          },
          { $set: { active: 0 } }
        );
        const followingStagesUpadate = await Stages.findOneAndUpdate(
          {
            loadingOrderId: loadingOrderId,
            order: order + 1,
          },
          { $set: { active: 1 } },
          { new: true }
        );
      }
    }
    console.log('realizar tarea 5');
    // verificar todas las tareas si finalizaron
    const fullTask = await Task.find({
      stagesId: { $in: arrayStagesIds },
    });

    let flag = true;

    for (const tasksTemp of fullTask) {
      console.log('flag====>>>', flag);
      console.log('tasksTemp====>>>', tasksTemp);
      let validationOperator = tasksTemp.validation.operator || false;
      console.log('validationOperator====>>', validationOperator);
      let taskAction = tasksTemp.action || [];
      console.log('taskAction====>>', taskAction);
      if (taskAction.length < 1) {
        console.log('no tiene dato el action');
        flag = false;
        break;
      }
      let approve = taskAction[taskAction.length - 1]?.Validation?.approve || false;
      if (validationOperator && approve == false) {
        console.log('le falta validacion');
        flag = false;
        break;
      }
    }
    console.log('realizar tarea 6');
    console.log('flag=====>>>', flag);

    if (flag) {
      let loadingOrderStatusFinishStatus = await LoadingOrderStatus.findOne({
        order: 3,
      });
      let dataFinishLoadingOrder = {
        statusId: loadingOrderStatusFinishStatus?._id || '',
        name: loadingOrderStatusFinishStatus?.name || '',
        date: dateTime,
        order: loadingOrderStatusFinishStatus?.order,
      };
      const loadingOrderStatusFinish = await Travel.updateOne(
        { 'loadingOrder.loadingOrderId': loadingOrderId },
        {
          $push: { 'loadingOrder.LoadingOrderStatus': dataFinishLoadingOrder },
          'loadingOrder.assignment.assignmentStatus': 0,
        }
      );
    }
    console.log('realizar tarea 7');

    console.log('loadingOrder===>>', loadingOrder);
    let idOperation = loadingOrder.operationId;

    let travelId = loadingOrder._id;

    let listOperation = await Operation.findById(idOperation);
    console.log('listOperation===', listOperation);

    let userOperadorId = listOperation?.userOperatorId || null;

    const operatorFind = await User.findById(userOperadorId);
    console.log('operatorFind ==> ', operatorFind);

    let userIdOne = operatorFind?._id || null;
    console.log('userIdOne===>>', userIdOne);

    const token = operatorFind?.device?.pushToken || null;
    console.log('token ==> ', token);

    let messagePublish = 'Tarea Realizada.';
    let typeNotification = 'tasks';
    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      travelId: travelId,
      operationId: idOperation,
      loadingOrderId: loadingOrderId,
      taskId: tasksId,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    //////////// notificacion pusher /////////

    const pusher = new Pusher({
      appId: '1266998',
      key: 'f8f1c4d725ab089521d6',
      secret: '491a6e8ca25b98ea5455',
      cluster: 'us2',
      useTLS: true,
    });

    await pusher.trigger(`loading-${travelId}`, 'perform-tasks', {
      message: 'Tarea realizada.',
    });

    /////////////////////////////////////////

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tarea Realizada.',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

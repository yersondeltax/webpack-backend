const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Benefits = require('../../../models/benefits');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let updateDate = moment().format();
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const benefitsId = event.pathParameters.benefitsId;
    console.log('benefitsId===>>>', benefitsId);

    let benefitsUpdate = await Benefits.updateOne(
      { _id: benefitsId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('benefitsUpdate===>>>', benefitsUpdate);
    if (benefitsUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Beneficio actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

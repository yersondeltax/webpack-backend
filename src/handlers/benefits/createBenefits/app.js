const { dbConnection } = require('../../../database/config');
const Benefits = require('../../../models/benefits');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newBenefits = new Benefits(body);
    const benefitsSaved = await newBenefits.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Beneficios creado exitosamente',
        benefits: benefitsSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

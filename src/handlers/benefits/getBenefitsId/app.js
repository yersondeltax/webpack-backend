const { dbConnection } = require('../../../database/config');
const Benefits = require('../../../models/benefits');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const benefitsId = event.pathParameters.benefitsId;
    console.log('benefitsId===>>>', benefitsId);

    let benefitsOne = await Benefits.findOne({ _id: benefitsId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        benefit: benefitsOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

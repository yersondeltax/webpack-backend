const { dbConnection } = require('../../../database/config');
const Benefits = require('../../../models/benefits');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const benefitsId = event?.pathParameters?.benefitsId || '';
    console.log('benefitsId===>>>', benefitsId);

    let benefitsOne = await Benefits.deleteOne({ _id: benefitsId });

    console.log('benefitsOne====>>>', benefitsOne);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Beneficio eliminado.',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

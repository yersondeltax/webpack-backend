const { dbConnection } = require('../../../database/config');
const Benefits = require('../../../models/benefits');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let app = event?.queryStringParameters?.app;
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let benefits = [];
    if (app) {
      benefits = await Benefits.find({ 'account.enable': true }).sort({ order: 1 });
    } else {
      if (page || size) {
        console.log('entro por verdadero page');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
      }
      let objectOrder = {};
      if (order) {
        objectOrder[`${order}`] = 1;
      }
      let objectFilter = {};
      if (filter) {
        objectFilter = {
          'account.enable': true,
        };
      }

      let objectSearch;
      if (search) {
        objectSearch = {
          name: new RegExp(search, 'i'),
        };
      }
      benefits = await Benefits.find({
        ...objectFilter,
        ...objectSearch,
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort(objectOrder);
      let total;
      if (page || size) {
        total = await Benefits.find({
          ...objectFilter,
          ...objectSearch,
        }).countDocuments();
        total = parseInt(total, 10);
        totalPage = total / sizeNumber;
        totalPage = Math.ceil(totalPage);
      }
    }
    // let benefits = [];
    // if (app) {
    //   benefits = await Benefits.find({ 'account.enable': true }).sort({ order: 1 });
    // } else {
    //   benefits = await Benefits.find();
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        benefits,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

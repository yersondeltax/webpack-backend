const { dbConnection } = require('../../../database/config');
const BoardingMode = require('../../../models/boardingMode');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const boardingModeId = event.pathParameters.boardingmodeid;
    console.log('boardingModeId===>>>', boardingModeId);

    let boardingModeOne = await BoardingMode.findOne({ _id: boardingModeId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        boardingMode: boardingModeOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

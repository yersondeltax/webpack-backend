const { dbConnection } = require('../../../database/config');
const BoardingMode = require('../../../models/boardingMode');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    // let userId = event?.queryStringParameters?.userId;
    // console.log('userId====>>', userId);

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    // let useData = await User.findById(userId);
    // let roles = useData?.roles || [];
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.write);
    // // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.read);
    // console.log('module====>>', module);
    // if (!module) {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No tienes permiso para realizar esta acción',
    //     }),
    //   };
    // }

    let data = { ...body, companyId: companyId };
    const newBoardingMode = new BoardingMode(data);
    const boardingModeSaved = await newBoardingMode.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Modo de embarque creado exitosamente',
        boardingMode: boardingModeSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

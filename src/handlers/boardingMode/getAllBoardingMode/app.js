const { dbConnection } = require('../../../database/config');
const BoardingMode = require('../../../models/boardingMode');
const User = require('../../../models/user');
const jwt = require('jsonwebtoken');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // console.log('event=====>>>', event);
    // console.log('context=====>>>', context);
    // let token = event.headers.Authorization;
    // console.log('token=====>>', token);
    // let decodePayload = await jwt.decode(
    //   'eyJraWQiOiJqZ1NqMUpEeUYrUjRIdVZUWFdWak5pY3ZEamlEMVBSYU9pUXNIQitNbVFJPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiJiMjI5ZWUzNy00NDgwLTQyYzEtOWE5My02ODMyMDU3OGQ4YTciLCJhdWQiOiIyNDVoN2htZWY0b3FidThzbm9waGZzamQwdSIsImV2ZW50X2lkIjoiMzcxOTMwNjItMTlhOC00YmQyLThmZjQtOWE3ZWU1ZDgzZjEyIiwidG9rZW5fdXNlIjoiaWQiLCJhdXRoX3RpbWUiOjE2MjQ0NTg2MzEsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC51cy1lYXN0LTEuYW1hem9uYXdzLmNvbVwvdXMtZWFzdC0xX1JocTdad1UzdiIsImNvZ25pdG86dXNlcm5hbWUiOiJiMjI5ZWUzNy00NDgwLTQyYzEtOWE5My02ODMyMDU3OGQ4YTciLCJleHAiOjE2MjQ1ODAxOTMsImlhdCI6MTYyNDUzNjk5M30.iUlw07N1DvnXiGASEREo-J3TTd3iRBuZExVMg6xUTB3V6StGXHy5Zl6ND65iEtnmSlP_he1LsQAfGgS8QkeKqVB6eY5yoZUERQc_--BOUIATREKzgc8ACaBjc-wcc06hucEdrwDWWMOZkRKuSfh9wRnQBIx-irvQ_ggYZiW04jSDfeD9vk-B1G714-wph1JKHgD4kHqq5tfTInWBbvUCTSV4NG4bFNE-5OVnb33B6HLOfHb1vnt-PQu2slnjDEZS2S1U49fiTy9SR_T8dNzd09BfysEvJm0I677V6olerlzbUSrtuhyAxQx6lrkjQ1-wwZOckrR3LxmgEf3sNtV7uA',
    //   'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuEuVI5WGSrxdPdSw12BMOn5vaGNRi2IHX2fegulNKYwe/Ozwd6qC1ibwEyOO6r38FjAZPdtUAq1YmM+0mIAz    WbLMKkFtW1uRYyVYZQxE2PQWfmPf3OYOGuvaSZ0EjU97gmXJM95n2AQKWjcf2D8D    lGucoTPsQH+ScDgDKN7dNGb8AgHCz9LCz97rFjN4Car6pVyNYXDKwWgGvrmkyet/    mcSc5mY2wdXnmYY5Ud0Iuy1Nz/A/a82FkoQNXK5ntcaaIPd3vw8W1QrHqLfG/lVY    YL3Q9jGhkMFyWdfabajNT/A4Y4s2G8FEDq+Zn8F8U4xq3vDpsSNIYe+q0aBWrVrn    JwIDAQAB'
    // );
    // console.log('data=====>>', decodePayload);

    ///////// desde aca descomentar para el id de user
    // let userId = event?.queryStringParameters?.userId;
    // console.log('userId====>>', userId);

    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);

    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    var pageNumber = 0;
    var sizeNumber = 0;
    let totalPage = 1;
    let boardingModes = [];
    await BoardingMode.find({ companyId: companyId });

    if (page || size) {
      console.log('entro por verdadero page');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
    }
    let objectOrder = {};
    if (order) {
      objectOrder[`${order}`] = 1;
    }
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }

    let objectSearch;
    if (search) {
      objectSearch = {
        name: new RegExp(search, 'i'),
      };
    }
    boardingModes = await BoardingMode.find({
      companyId: companyId,
      ...objectFilter,
      ...objectSearch,
    })
      .limit(sizeNumber)
      .skip(pageNumber * sizeNumber)
      .sort({ 'account.createDate': 1 });
    let total;
    if (page || size) {
      total = await BoardingMode.find({
        companyId: companyId,
        ...objectFilter,
        ...objectSearch,
      }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    }

    // let useData = await User.findById(userId);
    // let roles = useData?.roles || [];
    // // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.write);
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.read);
    // console.log('module====>>', module);
    // if (!module) {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No tienes permiso para realizar esta acción',
    //     }),
    //   };
    // }
    // const boardingModes = await BoardingMode.find({ companyId: companyId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        boardingModes,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

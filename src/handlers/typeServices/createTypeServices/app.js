const { dbConnection } = require('../../../database/config');
const TypeServices = require('../../../models/typeServices');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    const typeServicesDB = new TypeServices(data);
    const typeServicesDBSave = await typeServicesDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tipo de servicio creado exitosamente',
        typeServices: typeServicesDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const TypeServices = require('../../../models/typeServices');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const typeServicesId = event.pathParameters.typeServicesId;
    console.log('typeServicesId===>>>', typeServicesId);

    let typeServices = await TypeServices.findOne({ _id: typeServicesId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        typeServices,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const TypeServices = require('../../../models/typeServices');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }
    await dbConnection(process.env.dataBaseConnection);
    const typeServices = await TypeServices.find({ ...objectFilter }).sort({
      'account.createDate': 1,
    });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        typeServices,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

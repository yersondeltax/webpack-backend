const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const TypeServices = require('../../../models/typeServices');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const typeServicesId = event.pathParameters.typeServicesId;
    console.log('typeServicesId===>>>', typeServicesId);

    let updateDate = moment().format();
    let typeServicesUpdate = await TypeServices.updateOne(
      { _id: typeServicesId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('typeServicesUpdate===>>>', typeServicesUpdate);
    if (typeServicesUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Tipo de servicio actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

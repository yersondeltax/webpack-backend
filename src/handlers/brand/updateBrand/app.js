const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const Brand = require('../../../models/brand');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const brandId = event.pathParameters.brandId;
    console.log('brandId===>>>', brandId);
    let updateDate = moment().format();

    let BrandUpdate = await Brand.updateOne(
      { _id: brandId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('BrandUpdate===>>>', BrandUpdate);
    if (BrandUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Marca actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

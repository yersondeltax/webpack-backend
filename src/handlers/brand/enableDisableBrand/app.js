const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Brand = require('../../../models/brand');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const brandId = event.pathParameters.brandId;
    console.log('brandId===>>>', brandId);
    let brandUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      brandUpdate = await Brand.updateOne(
        { _id: brandId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      brandUpdate = await Brand.updateOne(
        { _id: brandId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('brandUpdate===>>>', brandUpdate);
    if (brandUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Marca ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

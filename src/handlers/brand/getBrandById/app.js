const { dbConnection } = require('../../../database/config');
const Brand = require('../../../models/brand');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let idBrand = event.pathParameters.brandId;

    const brand = await Brand.findById(idBrand);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        brand: brand,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Brand = require('../../../models/brand');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newBrand = new Brand(body);
    const brandSaved = await newBrand.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Marca creada exitosamente',
        brand: brandSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

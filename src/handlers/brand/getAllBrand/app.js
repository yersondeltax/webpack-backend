const { dbConnection } = require('../../../database/config');
const Brand = require('../../../models/brand');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const brands = await Brand.find().sort({ 'account.createDate': 1 });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        brand: brands,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

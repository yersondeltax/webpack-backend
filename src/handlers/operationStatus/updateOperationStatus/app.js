const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const OperationStatus = require('../../../models/operationStatus');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const operationStatusId = event.pathParameters.operationStatusId;
    console.log('operationStatusId===>>>', operationStatusId);

    let updateDate = moment().format();
    let operationStatusUpdate = await OperationStatus.updateOne(
      { _id: operationStatusId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('operationStatusUpdate===>>>', operationStatusUpdate);
    if (operationStatusUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Estado de operación actualizado.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

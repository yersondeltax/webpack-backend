const { dbConnection } = require('../../../database/config');
const OperationStatus = require('../../../models/operationStatus');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const operationStatusId = event.pathParameters.operationStatusId;
    console.log('operationStatusId===>>>', operationStatusId);

    let operationStatus = await OperationStatus.findOne({ _id: operationStatusId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        operationStatus: operationStatus,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

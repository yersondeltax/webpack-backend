const { dbConnection } = require('../../../database/config');
const OperationStatus = require('../../../models/operationStatus');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    const operationStatusDB = new OperationStatus(data);
    const operationStatusDBSave = await operationStatusDB.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Estado creado exitosamente',
        operationStatus: operationStatusDBSave,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

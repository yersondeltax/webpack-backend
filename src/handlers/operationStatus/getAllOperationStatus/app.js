const { dbConnection } = require('../../../database/config');
const OperationStatus = require('../../../models/operationStatus');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    const operationStatu = await OperationStatus.find().sort({ order: 1 });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        OperationStatus: operationStatu,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const OperationStatus = require('../../../models/operationStatus');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const operationStatusId = event.pathParameters.operationStatusId;
    console.log('operationStatusId===>>>', operationStatusId);
    let operationStatusUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      operationStatusUpdate = await OperationStatus.updateOne(
        { _id: operationStatusId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      operationStatusUpdate = await OperationStatus.updateOne(
        { _id: operationStatusId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('operationStatusUpdate===>>>', operationStatusUpdate);
    if (operationStatusUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Estado de operación ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const Travel = require('../../../models/travel');
const TravelStatus = require('../../../models/travelStatus');
const LoadingOrderStatus = require('../../../models/loadingOrderStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entrooo');
    let userOperatorId = event?.queryStringParameters?.userId || '';
    console.log('userOperatorId====>>', userOperatorId);
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    let disableDate = moment().format();
    console.log('disableDate ==> ', disableDate);
    const operationId = event?.pathParameters?.operationId || '';
    console.log('operationId===>>>', operationId);
    let orderOperationStatus = 5;
    console.log('orderOperationStatus===>>>', orderOperationStatus);
    let operationStatusCancel = await OperationStatus.findOne({ order: orderOperationStatus });
    console.log('operationStatusCancel===>>>', operationStatusCancel);
    let operationCancel = await Operation.updateOne(
      { _id: operationId },
      {
        $set: {
          statusOperationId: operationStatusCancel,
          'row.disableDate': disableDate,
          'row.disableOperatorId': userOperatorId,
        },
      }
    );
    console.log('operationCancel===>>>', operationCancel);
    if (operationCancel.nModified > 0) {
      let travels = await Travel.find({ operationId });
      console.log('travels====>>>', travels);
      let travelStatusCancel = await TravelStatus.findOne({ order: orderOperationStatus });
      console.log('operationStatusCancel===>>>', operationStatusCancel);
      let dataCancel = {
        statusId: travelStatusCancel?._id || '',
        name: travelStatusCancel?.name || '',
        date: disableDate,
        order: travelStatusCancel?.order,
      };
      // const travelUpdate = travels.map((travel) => {
      let newArray = [];
      for (const travel of travels) {
        console.log('travel====>>', travel);
        let travelId = travel._id;
        let travelCancel = {};
        travelCancel = await Travel.findOneAndUpdate(
          {
            _id: travelId,
          },
          {
            $set: {
              'row.disableDate': disableDate,
              'row.disableOperatorId': userOperatorId,
              publish: false,
              $push: { travelstatus: dataCancel },
            },
          },
          { new: true }
        );
        console.log('travelCancel===>>', travelCancel);
        let objectLoadingOrder = travelCancel?.loadingOrder || {};
        let objectoNuevo = {};
        let nuevo = JSON.stringify(objectLoadingOrder) === JSON.stringify(objectoNuevo);
        if (nuevo == false) {
          let objectLoadingOrderAssignment = travelCancel?.loadingOrder?.assignment || {};
          let nuevo22 =
            JSON.stringify(objectLoadingOrderAssignment) === JSON.stringify(objectoNuevo);
          if (nuevo22 == false) {
            let loadingOrderId = travelCancel?.loadingOrder?.loadingOrderId || '';
            let assignment = 0;
            let loadingOrderStatusCancel = await LoadingOrderStatus.findOne({
              order: orderOperationStatus,
            });
            let dataCancelLoadingOrder = {
              statusId: loadingOrderStatusCancel?._id || '',
              name: loadingOrderStatusCancel?.name || '',
              date: disableDate,
              order: loadingOrderStatusCancel?.order,
            };
            console.log('dataCancelLoadingOrder===>>>', dataCancelLoadingOrder);
            travelCancel = await Travel.findOneAndUpdate(
              {
                _id: travelId,
                'loadingOrder.loadingOrderId': loadingOrderId,
              },
              {
                $set: {
                  'loadingOrder.assignment.assignmentStatus': 0,
                },
                $push: { 'loadingOrder.LoadingOrderStatus': dataCancelLoadingOrder },
              },
              { new: true }
            );
            console.log('dataLoadingOrder===Cancel>>>', travelCancel);
          }
        }
        newArray.push(travelCancel);
        // return travelCancel;
      }
      console.log('newArray====?>>>', newArray);
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Operación y viajes cancelados.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

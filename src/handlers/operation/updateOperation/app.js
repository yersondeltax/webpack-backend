const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let userOperatorId = event?.queryStringParameters?.userId || '';
    console.log('userOperatorId====>>', userOperatorId);
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    let updateDate = moment().format();
    console.log('updateDate ==> ', updateDate);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const operationId = event?.pathParameters?.operationId || '';
    console.log('operationId===>>>', operationId);

    let operationUpdate = await Operation.updateOne(
      { _id: operationId },
      {
        $set: { ...body, 'row.updateDate': updateDate, 'row.updateOperatorId': userOperatorId },
      }
    );
    console.log('operationUpdate===>>>', operationUpdate);
    if (operationUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Operación actualizada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

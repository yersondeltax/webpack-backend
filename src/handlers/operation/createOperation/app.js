const moment = require('moment');
const momentTime = require('moment-timezone');

const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const Company = require('../../../models/company');
const StagesTemplate = require('../../../models/stagesTemplate');
const OperationStatus = require('../../../models/operationStatus');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let userOperatorId = event?.queryStringParameters?.userId || '';
    console.log('userOperatorId====>>', userOperatorId);
    const date = moment().format();
    console.log('date======>>', date);
    const year = momentTime.utc(date, 'YYYY-MM-DDTHH:mm:ss').tz('America/La_Paz').format('YYYY');
    console.log('year======>>', year);
    var operationNumber = '000001';

    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const dispatchTypeId = body?.dispatchType?.dispatchTypeId || '';
    console.log('dispatchTypeId===>>>', dispatchTypeId);
    const typeServiceId = body?.typeService?.typeServiceId || '';
    console.log('typeServiceId===>>>', typeServiceId);
    const placeId = body?.placeRoute?.placeId || '';
    console.log('placeId===>>>', placeId);

    const stagesTemplate = await StagesTemplate.find({
      dispatchTypeId: dispatchTypeId,
      typeServiceId: typeServiceId,
      placeId: placeId,
    }).sort({ order: 1 });

    if (stagesTemplate.length == 0 || stagesTemplate == null) {
      return {
        statusCode: 500,
        headers,
        body: JSON.stringify({
          error: {
            error: 500,
          },
          message: 'Error al crear operación. No existe la plantilla de tareas seleccionada.',
        }),
      };
    }

    let companyOperatorId = body?.companyOperatorId || '';
    const companyFind = await Company.findById(companyOperatorId);
    console.log('companyFind=====>>>', companyFind);
    const prefixCompany = companyFind.prefix || '';
    console.log('prefixCompany=====>>>', prefixCompany);

    let partCode = prefixCompany + year;

    let lastOperation = await Operation.findOne({
      code: new RegExp(partCode, 'i'),
    })
      .sort({ code: -1 })
      .select('code');
    console.log('lastOperation=====>>', lastOperation);
    let code;
    if (!lastOperation) {
      code = partCode + operationNumber;
    } else {
      let codelatest = lastOperation.code;
      let number = codelatest.split(partCode);
      console.log('number====>>>', number);
      let numberCode = parseInt(number[1], 10) + 1;
      let maxDigitsNumber = 6;
      operationNumber = `${'0'.repeat(
        maxDigitsNumber - numberCode.toString().length
      )}${numberCode}`;
      console.log('Numero de Operacion', operationNumber);
      code = partCode + operationNumber;
    }
    console.log('code====>>', code);

    let row = {
      createDate: date,
      createOperatorId: userOperatorId,
    };

    let orderOperationStatus = 1;
    console.log('orderOperationStatus===>>>', orderOperationStatus);
    let operationStatus = await OperationStatus.findOne({ order: orderOperationStatus });
    console.log('operationStatus===>>>', operationStatus);
    let idOperationStatus = operationStatus?._id;
    console.log('idOperationStatus===>>>', idOperationStatus);
    let data = { ...body, code: code, row: row, statusOperationId: idOperationStatus };

    const newOperation = new Operation(data);
    const operationSaved = await newOperation.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Operación creada exitosamente.',
        operation: operationSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

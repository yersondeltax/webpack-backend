const mongoose = require('mongoose');
const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const Company = require('../../../models/company');
const CompanyClient = require('../../../models/companyClient');
const User = require('../../../models/user');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('event====>>', event);
    await dbConnection(process.env.dataBaseConnection);
    let userId = event?.queryStringParameters?.userId;
    let companyId = event?.queryStringParameters?.companyId;
    console.log('userId====>>', userId);
    console.log('companyId====>>', companyId);
    var page = event?.queryStringParameters?.page || 0;
    var size = event?.queryStringParameters?.size || 100;
    console.log('page===>>', page);
    console.log('size===>>', size);
    var pageNumber;
    var sizeNumber;
    pageNumber = parseInt(page, 10);
    sizeNumber = parseInt(size, 10);
    let totalPage = 1;

    let type = event?.queryStringParameters?.type || 'true';
    console.log('type====>>', type);

    let search = {};

    if (type == 'true') {
      search = {
        travelFms: true,
        companyFmsId: companyId,
        'loadingOrder.assignment': { $exists: true },
      };
    } else {
      search = {
        travelFms: true,
        companyFmsId: companyId,
        'loadingOrder.assignment': { $exists: false },
      };
    }

    let fullTravel = await Travel.find({
      'row.disableDate': { $exists: false },
      ...search,
    }).countDocuments();

    let operationArray = [];

    if (fullTravel > 0) {
      let searchTravel = {};

      if (type == 'true') {
        searchTravel = {
          'loadingOrder.assignment': { $exists: true },
        };
      } else {
        searchTravel = {
          'loadingOrder.assignment': { $exists: false },
        };
      }

      const travels = await Travel.aggregate([
        {
          $match: {
            travelFms: true,
            companyFmsId: mongoose.Types.ObjectId(`${companyId}`),
            'row.disableDate': { $exists: false },
            ...searchTravel,
          },
        },
        {
          $group: {
            _id: {
              travelFms: '$travelFms',
              operationId: '$operationId',
            },
            num_sesiones: { $sum: 1 },
          },
        },
      ]);
      console.log('travels====>>', travels);
      let listOperation = [];

      if (travels.length > 0) {
        for (const datoTravel of travels) {
          let operationId = datoTravel?._id?.operationId || null;
          console.log('operationId====>>', operationId);
          listOperation.push(operationId);
        }
      }
      console.log('listOperation====>>', listOperation);

      let operations = await Operation.find({
        _id: { $in: listOperation },
        'row.disableDate': { $exists: false },
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort({ 'row.createDate': -1 });

      console.log('operations====>>', operations);

      let total = await Operation.find({
        _id: { $in: listOperation },
        'row.disableDate': { $exists: false },
      }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);

      for (const operationElement of operations) {
        const operation = operationElement;
        console.log('operation======>>', operation);
        const operationStatus = await OperationStatus.findById(operation.statusOperationId);
        let companyclienteOperation;

        companyclienteOperation = await CompanyClient.findById(operation.companyClientId);
        let usersOperatorOperation = await User.findById(operation.userOperatorId);
        let data = {
          ...operation._doc,
          usersOperatorProfile: usersOperatorOperation.profile,
          operationStatusName: operationStatus.name,
          company: { name: companyclienteOperation.name },
        };
        operationArray.push(data);
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        operations: operationArray,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

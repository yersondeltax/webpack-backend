const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const Company = require('../../../models/company');
const CompanyClient = require('../../../models/companyClient');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const operationId = event.pathParameters.operationId;
    const operation = await Operation.findById(operationId).exec();

    const operationStatus = await OperationStatus.findById(operation.statusOperationId).exec();
    const usersOperator = await User.findById(operation.userOperatorId).exec();
    const company = await CompanyClient.findById(operation.companyClientId).exec();
    const newOperation = {
      ...operation._doc,
      usersOperatorProfile: usersOperator.profile,
      operationStatusName: operationStatus.name,
      company: { name: company.name },
    };

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        operation: newOperation,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const Company = require('../../../models/company');
const CompanyClient = require('../../../models/companyClient');
const User = require('../../../models/user');
const Travel = require('../../../models/travel');
const CheckPoints = require('../../../models/checkPoints');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let userId = event?.queryStringParameters?.userId;
    let companyId = event?.queryStringParameters?.companyId;
    console.log('userId====>>', userId);
    console.log('companyId====>>', companyId);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    let search = event?.queryStringParameters?.search;
    console.log('search===>>', search);
    let order = event?.queryStringParameters?.order;
    console.log('order===>>', order);
    let sequence = event?.queryStringParameters?.sequence;
    console.log('sequence===>>', sequence);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    var pageNumber;
    var sizeNumber;
    const usersOperator = await User.findById(userId);
    let typeUser = usersOperator.type;
    let operations = [];
    let totalPage = 1;
    const company = await CompanyClient.findById(companyId);
    let objectSearch;
    if (search) {
      objectSearch = {
        $or: [{ code: new RegExp(search, 'i') }, { description: new RegExp(search, 'i') }],
      };
    }
    let objectOrder = {};
    if (order && sequence) {
      if (sequence == 'asc') {
        objectOrder[`${order}`] = 1;
      } else {
        objectOrder[`${order}`] = -1;
      }
    }
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        statusOperationId: filter,
      };
    }
    if (typeUser == 'OPERATOR') {
      let isAdmin = usersOperator?.isAdmin || false;
      if (isAdmin) {
        if (page || size) {
          console.log('entro por verdadero');
          pageNumber = parseInt(page, 10);
          sizeNumber = parseInt(size, 10);
          operations = await Operation.find({
            companyOperatorId: companyId,
            ...objectSearch,
            ...objectFilter,
          })
            .limit(sizeNumber)
            .skip(pageNumber * sizeNumber)
            .sort(objectOrder);
          let total = await Operation.find({
            companyOperatorId: companyId,
            ...objectSearch,
            ...objectFilter,
          }).countDocuments();
          total = parseInt(total, 10);
          totalPage = total / sizeNumber;
          totalPage = Math.ceil(totalPage);
        } else {
          console.log('entro por falso');
          operations = await Operation.find({
            companyOperatorId: companyId,
            ...objectSearch,
            ...objectFilter,
          }).sort(objectOrder);
        }
      } else {
        if (page || size) {
          console.log('entro por verdadero');
          pageNumber = parseInt(page, 10);
          sizeNumber = parseInt(size, 10);
          operations = await Operation.find({
            $or: [
              {
                userOperatorId: userId,
              },
              {
                'userOperatorAccess.userOperatorId': userId,
              },
            ],
            ...objectSearch,
            ...objectFilter,
          })
            .limit(sizeNumber)
            .skip(pageNumber * sizeNumber)
            .sort(objectOrder);
          let total = await Operation.find({
            $or: [
              {
                userOperatorId: userId,
              },
              {
                'userOperatorAccess.userOperatorId': userId,
              },
            ],
            ...objectSearch,
            ...objectFilter,
          }).countDocuments();
          total = parseInt(total, 10);
          totalPage = total / sizeNumber;
          totalPage = Math.ceil(totalPage);
        } else {
          console.log('entro por falso');
          operations = await Operation.find({
            $or: [
              {
                userOperatorId: userId,
              },
              {
                'userOperatorAccess.userOperatorId': userId,
              },
            ],
            ...objectSearch,
            ...objectFilter,
          }).sort(objectOrder);
        }
      }
    } else {
      if (page || size) {
        console.log('entro por verdadero');
        pageNumber = parseInt(page, 10);
        sizeNumber = parseInt(size, 10);
        operations = await Operation.find({
          companyClientId: companyId,
          ...objectSearch,
          ...objectFilter,
        })
          .limit(sizeNumber)
          .skip(pageNumber * sizeNumber)
          .sort(objectOrder);
        let total = await Operation.find({
          companyClientId: companyId,
          ...objectSearch,
          ...objectFilter,
        }).countDocuments();
        total = parseInt(total, 10);
        totalPage = total / sizeNumber;
        totalPage = Math.ceil(totalPage);
      } else {
        console.log('entro por falso');
        operations = await Operation.find({
          companyClientId: companyId,
          ...objectSearch,
          ...objectFilter,
        }).sort(objectOrder);
      }
    }

    // const operations = await Operation.find().exec();

    let operationArray = [];

    for (const operationElement of operations) {
      const operation = operationElement;
      console.log('operation======>>', operation);
      let idOperation = operation?._id;
      let travelsOperation = [];
      travelsOperation = await Travel.find({
        operationId: idOperation,
        'loadingOrder.loadingOrderId': { $exists: true },
      });
      let idTravel;
      let loading = {};
      let discharge = {};
      let checkPoints = [];
      let travelsByOperation = [];
      let loadingOrderId;
      for (const travelData of travelsOperation) {
        idTravel = travelData?._id;
        loading = {
          lat: travelData?.route?.checkPoints[0]?.lat || '',
          lng: travelData?.route?.checkPoints[0]?.lng || '',
          name: travelData?.route?.checkPoints[0]?.name || '',
          km: travelData?.route?.checkPoints[0]?.km || '',
          type: travelData?.route?.checkPoints[0]?.type || 'A',
        };
        discharge = {
          lat: travelData?.route?.checkPoints[1]?.lat || '',
          lng: travelData?.route?.checkPoints[1]?.lng || '',
          name: travelData?.route?.checkPoints[1]?.name || '',
          km: travelData?.route?.checkPoints[1]?.km || '',
          type: travelData?.route?.checkPoints[1]?.type || 'B',
        };
        loadingOrderId = travelData?.loadingOrder?.loadingOrderId;
        checkPoints = await CheckPoints.find({
          loadingOrderId: loadingOrderId,
        });
        let objectTravel = {
          _id: idTravel,
          loadingOrderId: loadingOrderId,
          loading,
          discharge,
          checkPoints: checkPoints,
        };
        travelsByOperation.push(objectTravel);
      }
      data = {
        _id: idOperation,
        travels: travelsByOperation,
      };
      operationArray.push(data);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        operations: operationArray,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

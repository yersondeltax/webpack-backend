const { dbConnection } = require('../../../database/config');
const Operation = require('../../../models/operation');
const OperationStatus = require('../../../models/operationStatus');
const Company = require('../../../models/company');
const CompanyClient = require('../../../models/companyClient');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let userId = event?.queryStringParameters?.userId;
    let companyId = event?.queryStringParameters?.companyId;
    console.log('userId====>>', userId);
    console.log('companyId====>>', companyId);
    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    var pageNumber;
    var sizeNumber;
    await dbConnection(process.env.dataBaseConnection);
    const usersOperator = await User.findById(userId);
    let typeUser = usersOperator.type;
    let operations = [];
    // let totalPage = 1;
    const company = await CompanyClient.findById(companyId);
    if (typeUser == 'OPERATOR') {
      let isAdmin = usersOperator?.isAdmin || false;
      if (isAdmin) {
        if (page || size) {
          console.log('entro por verdadero');
          pageNumber = parseInt(page, 10);
          sizeNumber = parseInt(size, 10);
          let skipNumber = pageNumber * sizeNumber;
          let limitNumber = sizeNumber;
          // operations = await Operation.find({ companyOperatorId: companyId })
          //   .limit(sizeNumber)
          //   .skip(pageNumber * sizeNumber)
          //   .sort({ 'row.createDate': -1 });
          // let total = await Operation.find({ companyOperatorId: companyId }).count();
          // total = parseInt(total, 10);
          // totalPage = total / sizeNumber;
          // totalPage = Math.ceil(totalPage);
          operations = await Operation.aggregate([
            {
              $lookup: {
                from: 'users',
                localField: 'userOperatorId',
                foreignField: '_id',
                as: 'usersOperatorProfile',
              },
            },
            {
              $lookup: {
                from: 'companyclients',
                localField: 'companyClientId',
                foreignField: '_id',
                as: 'company',
              },
            },
            {
              $lookup: {
                from: 'operationstatuses',
                localField: 'statusOperationId',
                foreignField: '_id',
                as: 'operationStatusName',
              },
            },
            {
              $skip: skipNumber,
            },
            {
              $limit: limitNumber,
            },
          ]);
        } else {
          // console.log('entro por falso');
          // operations = await Operation.find({ companyOperatorId: companyId }).sort({
          //   'row.createDate': -1,
          // });
        }
      } else {
        // if (page || size) {
        //   console.log('entro por verdadero');
        //   pageNumber = parseInt(page, 10);
        //   sizeNumber = parseInt(size, 10);
        //   operations = await Operation.find({
        //     $or: [
        //       {
        //         userOperatorId: userId,
        //       },
        //       {
        //         'userOperatorAccess.userOperatorId': userId,
        //       },
        //     ],
        //   })
        //     .limit(sizeNumber)
        //     .skip(pageNumber * sizeNumber)
        //     .sort({ 'row.createDate': -1 });
        //   let total = await Operation.find({
        //     $or: [
        //       {
        //         userOperatorId: userId,
        //       },
        //       {
        //         'userOperatorAccess.userOperatorId': userId,
        //       },
        //     ],
        //   }).count();
        //   total = parseInt(total, 10);
        //   totalPage = total / sizeNumber;
        //   totalPage = Math.ceil(totalPage);
        // } else {
        //   console.log('entro por falso');
        //   operations = await Operation.find({
        //     $or: [
        //       {
        //         userOperatorId: userId,
        //       },
        //       {
        //         'userOperatorAccess.userOperatorId': userId,
        //       },
        //     ],
        //   }).sort({ 'row.createDate': -1 });
        // }
      }
    } else {
      // if (page || size) {
      //   console.log('entro por verdadero');
      //   pageNumber = parseInt(page, 10);
      //   sizeNumber = parseInt(size, 10);
      //   operations = await Operation.find({ companyClientId: companyId })
      //     .limit(sizeNumber)
      //     .skip(pageNumber * sizeNumber)
      //     .sort({ 'row.createDate': -1 });
      //   let total = await Operation.find({ companyClientId: companyId }).count();
      //   total = parseInt(total, 10);
      //   totalPage = total / sizeNumber;
      //   totalPage = Math.ceil(totalPage);
      // } else {
      //   console.log('entro por falso');
      //   operations = await Operation.find({ companyClientId: companyId }).sort({
      //     'row.createDate': -1,
      //   });
      // }
    }

    // const operations = await Operation.find().exec();

    // let operationArray = [];

    // for (const operationElement of operations) {
    //   const operation = operationElement;
    //   console.log('operation======>>', operation);
    //   const operationStatus = await OperationStatus.findById(operation.statusOperationId);
    //   let companyclienteOperation;
    //   let data = {};
    //   if (typeUser == 'OPERATOR') {
    //     companyclienteOperation = await CompanyClient.findById(operation.companyClientId);
    //     data = {
    //       ...operation._doc,
    //       usersOperatorProfile: usersOperator.profile,
    //       operationStatusName: operationStatus.name,
    //       company: { name: companyclienteOperation.name },
    //     };
    //   } else {
    //     let usersOperatorOperation = await User.findById(operation.userOperatorId);
    //     data = {
    //       ...operation._doc,
    //       usersOperatorProfile: usersOperatorOperation.profile,
    //       operationStatusName: operationStatus.name,
    //       company: { name: company.name },
    //     };
    //   }
    //   // let data = {
    //   //   ...operation._doc,
    //   //   usersOperatorProfile: usersOperator.profile,
    //   //   operationStatusName: operationStatus.name,
    //   //   company: { name: company.name },
    //   // };
    //   operationArray.push(data);
    //   // operations[index] = {
    //   //   ...operation._doc,
    //   //   usersOperatorProfile: usersOperator.profile,
    //   //   operationStatusName: operationStatus.name,
    //   //   company: { name: company.name },
    //   // };
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        // totalPage,
        operations: operations,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const AWS = require('aws-sdk');
const moment = require('moment');
const momentTime = require('moment-timezone');

const { dbConnection } = require('../../../database/config');
const Travel = require('../../.../../../models/travel');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});

const UserPoolId = process?.env?.ARNCognito || '';

exports.lambdaHandler = async (event, context) => {
  try {
    console.log('entro al llamado automatico');
    await dbConnection(process.env.dataBaseConnection);
    console.log('event=====>>>', event);
    const travel = await Travel.find({
      $and: [
        { linkClient: { $exists: true } },
        { linkClient: { $ne: null } },
        {
          $or: [
            { 'loadingOrder.LoadingOrderStatus.order': 3 },
            { 'loadingOrder.LoadingOrderStatus.order': 4 },
            { 'loadingOrder.LoadingOrderStatus.order': 5 },
          ],
        },
      ],
    });
    console.log('travel==>', travel);

    for (const dataTravel of travel) {
      let travelIdLink = dataTravel?._id || null;

      let email = `correo${travelIdLink}@gmail.com`;
      console.log('email==>', email);

      const userClient = await User.findOne({ 'auth.email': email });
      console.log('userClient==>', userClient);

      if (userClient) {
        const userCognitoId = userClient?.userId || '';
        let userIdData = userClient?._id || null;
        let params = {
          Username: userCognitoId,
          UserPoolId: UserPoolId,
        };
        let usersDelete = await User.deleteOne({ _id: userIdData });
        console.log('usersDelete====>>', usersDelete);
        const newUserCognito = await new Promise((resolve, reject) => {
          cognitoidentityserviceprovider.adminDeleteUser(params, function (err, data) {
            if (err) {
              // callback.callbackRegisterComplete(err);
              console.log('ERR ==>', err);
              reject({ ok: false, error: { err } });
            } else {
              console.log('DATA ==>', data);
              resolve({ ok: true, ...data });
              // callback.callbackRegisterComplete(null); //
            }
          });
        });
        console.log('newUserCognito====>>>', newUserCognito);

        let travelUpdate = await Travel.updateOne(
          { _id: travelIdLink },
          {
            $set: { linkClient: null },
          }
        );
        console.log('travelUpdate====>>>', travelUpdate);
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'todo salio Bien',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

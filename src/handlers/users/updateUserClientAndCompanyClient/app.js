const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body ==> ', body);
    const userId = event?.pathParameters?.userId || '';
    console.log('userId ==> ', userId);
    const companyClientId = event?.pathParameters?.companyClientId || '0';
    console.log('companyClientId ==> ', companyClientId);
    let companyId = event?.queryStringParameters?.companyId || null;
    console.log('companyId====>>', companyId);
    let companyClientName = event?.queryStringParameters?.companyClientName || '';
    console.log('companyClientName ==> ', companyClientName);
    let companyName = event?.queryStringParameters?.companyName || '';
    console.log('companyName ==> ', companyName);
    let idArrayClient = event?.queryStringParameters?.idArrayClient || null;
    console.log('idArrayClient ==> ', idArrayClient);

    let user = {};

    if (companyClientId == '0') {
      user = await User.findOneAndUpdate(
        { _id: userId },
        {
          $set: {
            ...body,
          },
        },
        { new: true }
      );
    } else {
      if (!idArrayClient) {
        return {
          statusCode: 400,
          headers,
          body: JSON.stringify({
            message: 'No se encontro ese cliente.',
          }),
        };
      }

      let dateTime = moment().format();

      let userData = await User.findOne({ _id: userId });

      let arrayClient = userData?.companiesClient || [];

      let flag = true;

      const result = arrayClient.find((element) => {
        console.log('element====>>', element);
        let objectClient = {
          id: element._id,
          client: element.companyId,
          company: element.companyMatchId,
        };
        console.log('objectClient====>>', objectClient);
        let objectClientNew = {
          id: idArrayClient,
          client: companyClientId,
          company: companyId,
        };
        console.log('objectClientNew====>>', objectClientNew);
        let comparation = JSON.stringify(objectClient) === JSON.stringify(objectClientNew);
        console.log('comparation=====>>>', comparation);
        return comparation;
      });

      if (result) {
        flag = true;
      } else {
        flag = false;
      }

      if (flag == false) {
        let objectCompany = {
          assignationDate: dateTime,
          active: true,
          companyId: companyClientId,
          companyName: companyClientName,
          companyMatchId: companyId,
          companyMatchName: companyName,
        };
        console.log('objectCompany====>>', objectCompany);

        let userPullClient = await User.findOneAndUpdate(
          { _id: userId },
          { $pull: { companiesClient: { _id: idArrayClient } } },
          { new: true }
        );
        console.log('userPullClient====>>', userPullClient);

        user = await User.findOneAndUpdate(
          { _id: userId },
          {
            $set: {
              ...body,
            },
            $push: { companiesClient: objectCompany },
          },
          { new: true }
        );
      } else {
        user = await User.findOneAndUpdate(
          { _id: userId },
          {
            $set: {
              ...body,
            },
          },
          { new: true }
        );
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Usuario actualizado.',
        user: user,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

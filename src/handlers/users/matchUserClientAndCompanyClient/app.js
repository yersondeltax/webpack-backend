const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body ==> ', body);
    const userId = event?.pathParameters?.userId || '';
    console.log('userId ==> ', userId);
    const companyClientId = event?.pathParameters?.companyClientId || null;
    console.log('companyClientId ==> ', companyClientId);
    let companyId = event?.queryStringParameters?.companyId || null;
    console.log('companyId====>>', companyId);
    let companyClientName = event?.queryStringParameters?.companyClientName || '';
    console.log('companyClientName ==> ', companyClientName);
    let companyName = event?.queryStringParameters?.companyName || '';
    console.log('companyName ==> ', companyName);

    let dateTime = moment().format();

    let objectCompany = {
      assignationDate: dateTime,
      active: true,
      companyId: companyClientId,
      companyName: companyClientName,
      companyMatchId: companyId,
      companyMatchName: companyName,
    };
    console.log('objectCompany====>>', objectCompany);

    let user = await User.findOneAndUpdate(
      { _id: userId },
      {
        $set: {
          ...body,
        },
        $push: { companiesClient: objectCompany },
      },
      { new: true }
    );

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Usuario actualizado.',
        user: user,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const CompanyClient = require('../../../models/companyClient');
const Company = require('../../../models/company');
const mongoose = require('mongoose');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let companyMatchId = event?.queryStringParameters?.companyId;
    console.log('companyMatchId====>>', companyMatchId);

    const users = await User.find({ type: 'CLIENT' });
    console.log('users===>>', users);

    for (const userData of users) {
      let userId = userData?._id;
      let companyId = userData?.profile?.companyId || null;
      let createDate = userData?.account?.createDate || '2021-10-28T04:00:00.773+00:00';
      let nameCompanyMatch = '';
      let nameCompany = '';

      let companyClientData = await CompanyClient.findOne({ _id: companyId });
      let companyMatchData2 = await Company.findOne({ _id: companyMatchId });
      nameCompany = companyClientData?.name || '';
      nameCompanyMatch = companyMatchData2?.name || '';

      let objectCompany = {
        assignationDate: createDate,
        active: true,
        companyId: companyId,
        companyName: nameCompany,
        companyMatchId: companyMatchId,
        companyMatchName: nameCompanyMatch,
      };
      console.log('objectCompany====>>', objectCompany);

      let arrayCompany = [];
      arrayCompany.push(objectCompany);

      await User.updateOne(
        {
          _id: userId,
        },
        { $set: { companiesClient: arrayCompany } }
      );
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'actualizaciones de todas las compañias',
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

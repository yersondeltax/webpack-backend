const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const TransportUnit = require('../../../models/transportUnit');
let response;
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    console.log('event====>>', event);
    let email = event?.queryStringParameters?.email;
    console.log('email====>>', email);
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);
    const users = await User.findOne({
      'auth.email': email,
      type: 'CLIENT',
    });
    console.log('users====>>', users);

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        users: users,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
  return response;
};

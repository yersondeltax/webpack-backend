const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const CompanyClient = require('../../../models/companyClient');
const Company = require('../../../models/company');
const mongoose = require('mongoose');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let companyMatchId = event?.queryStringParameters?.companyId;
    console.log('companyMatchId====>>', companyMatchId);

    const users = await User.find({ $or: [{ type: 'OPERATOR' }, { type: 'CLIENT' }] });
    console.log('users===>>', users);

    for (const userData of users) {
      let userId = userData?._id;

      const configurationsInitial = [
        {
          name: 'viajes',
          columns: [
            { name: 'Fecha Carga', active: true },
            { name: 'Fecha Entrega', active: true },
            { name: 'Origen', active: true },
            { name: 'Destino', active: true },
            { name: 'Peso', active: true },
            { name: 'Flete', active: false },
            { name: 'Costo', active: false },
            { name: 'Utilidad', active: false },
            { name: 'Placa', active: true },
            { name: 'Estado', active: true },
          ],
        },
        {
          name: 'oportunidades',
          columns: [
            { name: 'Fecha Carga', active: true },
            { name: 'Fecha Entrega', active: true },
            { name: 'Origen', active: true },
            { name: 'Destino', active: true },
            { name: 'Peso', active: true },
            { name: 'Categoría', active: true },
            { name: 'Cliente', active: true },
            { name: 'Tipo de Servicio', active: true },
            { name: 'Estado', active: true },
          ],
        },
      ];

      await User.updateOne(
        {
          _id: userId,
        },
        { $set: { config: configurationsInitial } }
      );
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'configuraciones actualizadas',
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

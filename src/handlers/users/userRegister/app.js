const AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const Company = require('../../../models/company');

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: '2017-05-30',
});
const UserPoolId = process.env.ARNCognito;
let response;

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    console.log('AWS===>>', AWS);
    console.log('cognitoidentityserviceprovider===>>', cognitoidentityserviceprovider);
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const data = body;
    let params = {
      Username: data.auth.email,
      UserPoolId: UserPoolId,
    };
    let companyId = data?.profile?.companyId || '';
    let emailUser = data?.auth?.email || '';
    // params['Username'] = data.email;
    // params['UserPoolId'] = UserPoolId;

    console.log('EVENT ==>', event);

    if (emailUser == '' || emailUser == null || emailUser == false) {
      return {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          message: 'Requiere email',
        }),
      };
    }

    const userSearch = await User.findOne({ 'auth.email': emailUser });

    if (userSearch) {
      return {
        statusCode: 404,
        headers,
        body: JSON.stringify({
          message: 'Email ya registrado',
        }),
      };
    }
    console.log('paso  1');
    const newUser = await new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.adminCreateUser(params, function (err, data) {
        if (err) {
          // callback.callbackRegisterComplete(err);
          console.log('ERR ==>', err);
          reject({ ok: false, error: { err } });
        } else {
          console.log('DATA ==>', data);
          resolve({ ok: true, ...data });
          // callback.callbackRegisterComplete(null); //
        }
      });
    });

    let userDBSave = {};
    if (newUser.ok) {
      const company = await Company.findOne({ _id: companyId }).select('access');
      console.log('company===>>', company);
      let accessArray = company?.access || [];
      let moduleArray = [];
      for (const moduleCompany of accessArray) {
        console.log('moduleCompany====>>', moduleCompany);
        let moduleId = moduleCompany.modulesId;
        let nameModule = moduleCompany.name;
        let codeModule = moduleCompany.code;
        let module = {
          modulesId: moduleId,
          name: nameModule,
          code: codeModule,
          read: false,
          write: false,
        };

        moduleArray.push(module);
      }
      const payload = {
        userId: newUser.User.Username,
        ...data,
        roles: moduleArray,
        config: [
          {
            name: 'viajes',
            columns: [
              { name: 'Fecha Carga', active: true },
              { name: 'Fecha Entrega', active: true },
              { name: 'Origen', active: true },
              { name: 'Destino', active: true },
              { name: 'Peso', active: true },
              { name: 'Flete', active: false },
              { name: 'Costo', active: false },
              { name: 'Utilidad', active: false },
              { name: 'Placa', active: true },
              { name: 'Estado', active: true },
            ],
          },
          {
            name: 'oportunidades',
            columns: [
              { name: 'Fecha Carga', active: true },
              { name: 'Fecha Entrega', active: true },
              { name: 'Origen', active: true },
              { name: 'Destino', active: true },
              { name: 'Peso', active: true },
              { name: 'Categoría', active: true },
              { name: 'Cliente', active: true },
              { name: 'Tipo de Servicio', active: true },
              { name: 'Estado', active: true },
            ],
          },
        ],
      };
      const userDB = new User(payload);
      userDBSave = await userDB.save();
    }

    response = {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Usuario creado exitosamente',
        user: userDBSave,
      }),
    };

    // event.response = { autoConfirmUser: true };
  } catch (err) {
    response = {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        error: err,
      }),
    };
    // return err;
  }
  return response;
};

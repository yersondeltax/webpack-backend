const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const moment = require('moment');
const { dbConnection } = require('../../../database/config');
const User = require('../../../models/user');
const Operation = require('../../../models/operation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};
exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body ==> ', body);
    const userId = event?.pathParameters?.userId || '';
    console.log('userId ==> ', userId);
    let companyClientId = event?.queryStringParameters?.companyClientId || null;
    console.log('companyClientId ==> ', companyClientId);
    let verify = false;

    let listOperation = await Operation.find({
      'usersClients.userClientId': userId,
      companyClientId: companyClientId,
    });

    if (listOperation.length > 0) {
      verify = true;
    } else {
      verify = false;
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        verify,
      }),
    };
  } catch (err) {
    console.log('err =>', err);
    return {
      statusCode: 404,
      headers,
      body: JSON.stringify({
        message: 'Error',
        // error: err,
      }),
    };
  }
};

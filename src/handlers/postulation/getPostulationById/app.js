const { dbConnection } = require('../../../database/config');
const Postulation = require('../../../models/postulation');
// const Setting = require('./models/setting');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const postulationId = event.pathParameters.postulationId;
    console.log('postulationId===>>>', postulationId);

    let postulation = await Postulation.findOne({ _id: postulationId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        postulation: postulation,
      }),
    };
  } catch (err) {
    console.log('err ===>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

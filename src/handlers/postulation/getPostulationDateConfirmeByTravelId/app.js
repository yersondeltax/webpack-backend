const { dbConnection } = require('../../../database/config');
const Postulation = require('../../../models/postulation');
const User = require('../../../models/user');
const Travel = require('../../../models/travel');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const travelId = event.pathParameters.travelId;
    const postulations = await Postulation.find({
      travelId,
      confirmedDate: { $exists: true },
      'account.enable': 1,
    }).sort({ confirmedDate: 1 });
    console.log('postulations====>>>', postulations);
    let transportUnitAssignment = null;
    for (const postulationConfirmed of postulations) {
      console.log('postulationConfirmed====>>>', postulationConfirmed);
      let transportUnitPostulation = postulationConfirmed?.transportUnitId || '';
      console.log('transportUnitPostulation====>>>', transportUnitPostulation);
      let travelAndTransportUnit = await Travel.findOne({
        'loadingOrder.assignment.transportUnitId': transportUnitPostulation,
        'loadingOrder.assignment.assignmentStatus': 1,
      });
      console.log('travelAndTransportUnit====>>', travelAndTransportUnit);
      let objectoNuevoTravel = {};
      let newComparationByTravel =
        JSON.stringify(travelAndTransportUnit) === JSON.stringify(objectoNuevoTravel);
      if (newComparationByTravel || travelAndTransportUnit == null) {
        console.log('entroooo en el if');
        transportUnitAssignment = postulationConfirmed;
        console.log('transportUnitAssignment===>>', transportUnitAssignment);
        break;
      }
    }
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        postulation: transportUnitAssignment,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

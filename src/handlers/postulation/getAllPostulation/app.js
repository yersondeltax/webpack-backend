const { dbConnection } = require('../../../database/config');
const Postulation = require('../../../models/postulation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const postulations = await Postulation.find();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        postulations,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

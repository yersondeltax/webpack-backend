var AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const Postulation = require('../../../models/postulation');
const TransportUnit = require('../../../models/transportUnit');
const Operation = require('../../../models/operation');
const Travel = require('../../../models/travel');
const User = require('../../../models/user');
const Notification = require('../../../models/notification');

const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    const postulationId = event.pathParameters.postulationId;
    let acceptedDate = moment().format();
    console.log('entreda update one');
    const postulationUpdated = await Postulation.findOneAndUpdate(
      { _id: postulationId },
      {
        $set: { acceptedDate, ...body },
      },
      { upsert: true }
    );
    console.log('salida update');
    // transportUnitId
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });
    const postulacionFind = await Postulation.findById(postulationId);
    const transportUnitFind = await TransportUnit.findById(postulacionFind.transportUnitId);
    console.log('transportUnitFind ==> ', transportUnitFind);
    const carrierId =
      transportUnitFind?.drivers[transportUnitFind.drivers.length - 1]?.userId || null;

    let messagePublish = 'Tu postulación ha sido aceptada, ingresa para confirmar.';
    let typeNotification = 'postulationAccepted';

    const carrierFind = await User.findById(carrierId);
    console.log('carrierFind ==> ', carrierFind);
    let userIdOne = carrierFind?._id || null;
    console.log('userIdOne===>>', userIdOne);
    const token = carrierFind?.device?.pushToken || null;

    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      travelId: postulationUpdated?.travelId || null,
      postulationId: postulationId,
    };
    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);

    // para los colaboradores se necesita una clausula IN es decir los usuarios que esten en un array
    // if (carrierFind.devices.length > 0) {
    // const token = carrierFind.devices[carrierFind.devices.length - 1].pushToken;
    //   console.log('token ==> ', token);
    //   let idNotification = uuidv4().toString();
    //   const messagePublish = `Tu postulación ha sido aceptada, ingresa para confirmar.`;
    //   let payloadNotification = {
    //     default: 'default',
    //     GCM: {
    //       notification: {
    //         title: 'DeltaX',
    //         body: messagePublish,
    //         sound: 'default',
    //       },
    //       data: {
    //         id: idNotification,
    //         travelId: postulationUpdated.travelId,
    //         postulationId: postulationId,
    //         type: 'postulationAccepted',
    //       },
    //     },
    //   };
    //   console.log('mensaje push====>>', payloadNotification);
    //   payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    //   payloadNotification = JSON.stringify(payloadNotification);
    //   const params_sns = {
    //     TargetArn: token,
    //     Message: payloadNotification,
    //     MessageStructure: 'json',
    //   };
    //   const publishText = await sns_new
    //     .publish(params_sns)
    //     .promise()
    //     .then(
    //       function (data) {
    //         /* process the data */
    //         console.log('dataaa', data);
    //         return data;
    //       },
    //       function (error) {
    //         console.log('errorrrr', error);
    //         return error;
    //         /* handle the error */
    //       }
    //     );
    //   console.log('publishText => ', publishText);
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Postulación aceptada exitosamente',
      }),
    };
  } catch (err) {
    console.log('err => ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

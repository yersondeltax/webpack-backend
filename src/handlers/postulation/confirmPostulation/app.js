var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
var lambda = new AWS.Lambda({ region: 'us-east-1' });

const { dbConnection } = require('../../../database/config');
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const Postulation = require('../../../models/postulation');
const TransportUnit = require('../../../models/transportUnit');
const Operation = require('../../../models/operation');
const Travel = require('../../../models/travel');
const User = require('../../../models/user');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const postulationId = event.pathParameters.postulationId;
    let confirmedDate = moment().format();
    console.log('entreda update one');
    const postulationUpdated = await Postulation.findOneAndUpdate(
      { _id: postulationId },
      { confirmedDate }
    );
    console.log('salida update');
    // transportUnitId
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });

    const travelFind = await Travel.findById(postulationUpdated.travelId).exec();
    console.log('travelFind ==> ', travelFind);

    const operationFind = await Operation.findById(travelFind.operationId).exec();
    console.log('operationFind ==> ', operationFind);

    let messagePublish = `Hay una nueva confirmación en ${travelFind.categoryLoad.name}`;
    let typeNotification = 'postulationConfirmed';

    const operatorFind = await User.findById(operationFind.userOperatorId);
    console.log('operatorFind ==> ', operatorFind);
    let userIdOne = operatorFind?._id || null;
    console.log('userIdOne===>>', userIdOne);
    const token = operatorFind?.device?.pushToken || null;
    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      travelId: travelFind?._id || null,
      operationId: operationFind?._id || null,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);
    // if (operatorFind.devices.length > 0) {
    //   const token = operatorFind.devices[operatorFind.devices.length - 1].pushToken;
    //   console.log('token ==> ', token);
    //   // para los colaboradores se necesita una clausula IN es decir los usuarios que esten en un array
    //   let idNotification = uuidv4().toString();
    //   const messagePublish = `Hay una nueva confirmación en ${travelFind.categoryLoad.name}`;
    //   let payloadNotification = {
    //     default: 'default',
    //     GCM: {
    //       notification: {
    //         title: 'DeltaX',
    //         body: messagePublish,
    //         sound: 'default',
    //       },
    //       data: {
    //         id: idNotification,
    //         operationId: operationFind._id,
    //         travelId: travelFind._id,
    //         type: 'postulationConfirmed',
    //       },
    //     },
    //   };
    //   payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    //   payloadNotification = JSON.stringify(payloadNotification);
    //   const params_sns = {
    //     TargetArn: token,
    //     Message: payloadNotification,
    //     MessageStructure: 'json',
    //   };
    //   const publishText = await sns_new
    //     .publish(params_sns)
    //     .promise()
    //     .then(
    //       function (data) {
    //         /* process the data */
    //         console.log('dataaa', data);
    //         return data;
    //       },
    //       function (error) {
    //         console.log('errorrrr', error);
    //         return error;
    //         /* handle the error */
    //       }
    //     );
    //   console.log('publishText => ', publishText);
    // }

    let travelId = travelFind._id;

    // let updateTravel = await Travel.updateOne(
    //   { _id: travelId },
    //   {
    //     $set: { publish: false },
    //   },
    //   { upsert: true }
    // );

    ////// invocar a otra lambda  ///////

    console.log('aca empieza');

    // let payload = JSON.stringify({
    //   carrierId: 'Pruebaaaaaaa',
    //   invoke: true,
    //   pathParameters: {
    //     travelId: travelId,
    //   },
    //   messageCarrier: 'Tarea validada.',
    //   body: {
    //     publish: false,
    //   },
    // });
    // console.log('payload', payload);

    // let params = {
    //   FunctionName: 'travel-publishTravel-MviwMIfvDlUm',
    //   InvocationType: 'RequestResponse',
    //   // InvocationType='Event',
    //   Payload: payload,
    // };

    // let dataResponse = await lambda.invoke(params).promise();

    // console.log('dataResponse=====>>', dataResponse);

    /////// termina de invocar /////////

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Postulación confirmada exitosamente y oportunidad cerrada.',
      }),
    };
  } catch (err) {
    console.log('err => ', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

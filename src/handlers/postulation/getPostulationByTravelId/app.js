const { dbConnection } = require('../../../database/config');
const Postulation = require('../../../models/postulation');
const User = require('../../../models/user');
const Travel = require('../../../models/travel');
const TransportUnit = require('../../../models/transportUnit');
// const Setting = require('./models/setting');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const travelId = event.pathParameters.travelId;

    var page = event?.queryStringParameters?.page;
    var size = event?.queryStringParameters?.size;
    console.log('page===>>', page);
    console.log('size===>>', size);
    var pageNumber;
    var sizeNumber;
    let postulations = [];
    let totalPage = 1;
    if (page || size) {
      console.log('entro por verdadero');
      pageNumber = parseInt(page, 10);
      sizeNumber = parseInt(size, 10);
      postulations = await Postulation.find({
        $and: [
          { travelId: travelId },
          { cancelledDate: { $exists: false } },
          { 'account.enable': 1 },
        ],
      })
        .limit(sizeNumber)
        .skip(pageNumber * sizeNumber)
        .sort({ postulateDate: -1 });
      let total = await Postulation.find({ travelId }).countDocuments();
      total = parseInt(total, 10);
      totalPage = total / sizeNumber;
      totalPage = Math.ceil(totalPage);
    } else {
      console.log('entro por falso');
      postulations = await Postulation.find({
        $and: [
          { travelId: travelId },
          { cancelledDate: { $exists: false } },
          { 'account.enable': 1 },
        ],
      }).sort({ postulateDate: -1 });
    }
    // const postulations = await Postulation.find({ travelId });
    const travel = await Travel.findById(travelId);
    const travelClear = travel._doc;
    let arrayPostulation = [];
    let data = {};

    for (const dataPostulation of postulations) {
      let transportUnitId = dataPostulation.transportUnitId;
      let travelAndTransportUnit = await Travel.findOne({
        'loadingOrder.assignment.transportUnitId': transportUnitId,
        'loadingOrder.assignment.assignmentStatus': 1,
      });
      let occupied = false;
      let objectoNuevoTravel = {};
      let newComparationByTravel =
        JSON.stringify(travelAndTransportUnit) === JSON.stringify(objectoNuevoTravel);
      if (newComparationByTravel || travelAndTransportUnit == null) {
        occupied = false;
      } else {
        occupied = true;
      }
      let transportUnit = await TransportUnit.findById(transportUnitId);
      let userId = transportUnit.drivers[transportUnit.drivers.length - 1].userId;
      let user = await User.findById(userId);
      // let userCarrier = user.devices[user.devices.length - 1];
      let dataUser = {
        _id: user._id || '',
        userId: user.userId || '',
        type: user.type || '',
        auth: user.auth || {},
        profile: user.profile || {},
        address: user.address || {},
      };
      ////////////////////////////////////////////////////////////////////////////////
      const transportUnitClear = transportUnit._doc;
      // const setting = await Setting.find();
      // if (setting[0].featureRestriction) {
      const featuresTravelNotFound = travelClear.features.map((featureTravel) => {
        const newFeatureTravel = {
          name: featureTravel.name,
          featuresTransportUnitId: featureTravel.featuresTransportUnitId,
          valueQualitative: featureTravel.valueQualitative || '',
          valueQuantitative: featureTravel.valueQuantitative || '',
        };
        // findIndex
        const featureFind = transportUnitClear.features.find((featureTU) => {
          const newFeatureTU = {
            name: featureTU.name,
            featuresTransportUnitId: featureTU.featuresTransportUnitId,
            valueQualitative: featureTU.valueQualitative || '',
            valueQuantitative: featureTU.valueQuantitative || '',
          };
          return JSON.stringify(newFeatureTravel) === JSON.stringify(newFeatureTU);
        });
        if (featureFind) {
          return null;
        } else {
          return { ...featureTravel._doc, hashtag: 'warning' };
        }
      });
      console.log('featuresTravelNotFound ===> ', featuresTravelNotFound);
      for (const featureTravelNotFound of featuresTravelNotFound) {
        if (featureTravelNotFound) {
          console.log('entro al if ==> ', featureTravelNotFound);
          transportUnitClear.features = [...transportUnitClear.features, featureTravelNotFound];
          console.log('transportUnitClear.features ==> ', transportUnitClear.features);
        }
      }
      // }
      ////////////////////////////////////////////////////////////////////////////////
      let dataTransportUnit = {
        plate: transportUnit.plate || '',
        company: transportUnit.company || '',
        country: transportUnit.country || '',
        color: transportUnit.color || '',
        year: transportUnit.year || '',
        numberOfShafts: transportUnit.numberOfShafts || '',
        storageCapacity: transportUnit.storageCapacity || '',
        volumeCapacity: transportUnit.volumeCapacity || '',
        typeTransportUnit: transportUnit.typeTransportUnit || '',
        features: transportUnitClear.features || [],
        resources: transportUnitClear.resources || [],
      };
      data = { dataPostulation, transportUnit: dataTransportUnit, user: dataUser, occupied };
      arrayPostulation.push(data);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        totalPage,
        postulations: arrayPostulation,
      }),
    };
  } catch (err) {
    console.log('err ===>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

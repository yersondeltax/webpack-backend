var AWS = require('aws-sdk');
const { dbConnection } = require('../../../database/config');
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');

const FeaturesTransportUnit = require('../../../models/featuresTransportUnit');
const Operation = require('../../../models/operation');
const Postulation = require('../../../models/postulation');
const TransportUnit = require('../../../models/transportUnit');
const Travel = require('../../../models/travel');
const User = require('../../../models/user');
const Notification = require('../../../models/notification');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    let postulateDate = moment().format();
    let transportUnitId = body.transportUnitId || '';
    if (transportUnitId == '') {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Se necesita la unidad de transporte.',
        }),
      };
    }

    let transportUnit = await TransportUnit.findById(transportUnitId);
    if (!transportUnit) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No existe unidad de transporte.',
        }),
      };
    }

    let featureTransportUnit = transportUnit._doc.features || [];
    let featureRequered = await FeaturesTransportUnit.find({
      requireForCarrier: true,
    });

    if (featureTransportUnit.length < 0) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Requiere registrar las caracteristicas de su unidad de transporte.',
        }),
      };
    }

    let requiered = true;
    console.log('featureTransportUnit===>>', featureTransportUnit);
    console.log('featureRequered===>>', featureRequered);
    // featureRequered = featureRequered._doc;
    for (let idRequered = 0; idRequered < featureRequered.length; idRequered++) {
      const elementRequere = featureRequered[idRequered];
      console.log('elemento requerido', elementRequere);
      for (let idFeature = 0; idFeature < featureTransportUnit.length; idFeature++) {
        const elementFeature = featureTransportUnit[idFeature];
        console.log('elemento feature', elementFeature);
        let idElementRequere = elementRequere._id;
        let idElementFeature = elementFeature.featuresTransportUnitId;
        let objetElement = {
          id: idElementRequere,
        };
        let objetFeature = {
          id: idElementFeature,
        };
        // let respuesta = JSON.stringify(objetElement) === JSON.stringify(objetFeature);
        // console.log('respuestas=====>>', respuesta);
        if (JSON.stringify(objetElement) === JSON.stringify(objetFeature)) {
          console.log('entro en el igual');
          break;
        }
        if (idFeature == featureTransportUnit.length - 1) {
          console.log('idFeature==>>', idFeature);
          requiered = false;
          break;
        }
      }
      if (requiered == false) {
        console.log('entro al if de false');
        break;
      }
    }

    if (requiered == false) {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'Requiere registrar las caracteristicas requeridas de su unidad de transporte.',
        }),
      };
    }

    const newPostulation = new Postulation({ ...body, postulateDate });
    const postulationSaved = await newPostulation.save();
    const sns_new = new AWS.SNS({ apiVersion: '2010-03-31', region: 'us-east-1' });
    const travelFind = await Travel.findById(body.travelId).exec();
    console.log('travelFind ==> ', travelFind);
    const operationFind = await Operation.findById(travelFind.operationId).exec();
    console.log('operationFind ==> ', operationFind);

    let messagePublish = `Hay una nueva postulación en ${travelFind.categoryLoad.name}`;
    let typeNotification = 'postulation';

    const operatorFind = await User.findById(operationFind.userOperatorId);
    console.log('operatorFind ==> ', operatorFind);
    let userIdOne = operatorFind?._id || null;
    console.log('userIdOne===>>', userIdOne);
    const token = operatorFind?.device?.pushToken || null;
    console.log('token ==> ', token);
    let dataBody = {
      userId: userIdOne,
      tokenUser: token,
      message: messagePublish,
      type: typeNotification,
      travelId: travelFind?._id || null,
      operationId: operationFind?._id || null,
    };

    let newNotification = new Notification(dataBody);
    let notificationSaved = await newNotification.save();
    console.log('notificationSaved===>>', notificationSaved);
    // // para los colaboradores se necesita una clausula IN es decir los usuarios que esten en un array
    // if (operatorFind.devices.length > 0) {
    //   const token = operatorFind.devices[operatorFind.devices.length - 1].pushToken;
    //   console.log('token ==> ', token);
    //   let idNotification = uuidv4().toString();
    //   const messagePublish = `Hay una nueva postulación en ${travelFind.categoryLoad.name}`;
    //   let payloadNotification = {
    //     default: 'default',
    //     GCM: {
    //       notification: {
    //         title: 'DeltaX',
    //         body: messagePublish,
    //         sound: 'default',
    //       },
    //       data: {
    //         id: idNotification,
    //         operationId: operationFind._id,
    //         travelId: travelFind._id,
    //         type: 'postulation',
    //       },
    //     },
    //   };
    //   payloadNotification.GCM = JSON.stringify(payloadNotification.GCM);
    //   payloadNotification = JSON.stringify(payloadNotification);
    //   const params_sns = {
    //     TargetArn: token,
    //     Message: payloadNotification,
    //     MessageStructure: 'json',
    //   };
    //   const publishText = await sns_new
    //     .publish(params_sns)
    //     .promise()
    //     .then(
    //       function (data) {
    //         /* process the data */
    //         console.log('dataaa', data);
    //         return data;
    //       },
    //       function (error) {
    //         console.log('errorrrr', error);
    //         return error;
    //         /* handle the error */
    //       }
    //     );
    //   console.log('publishText => ', publishText);
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Postulación creada exitosamente',
        postulation: postulationSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

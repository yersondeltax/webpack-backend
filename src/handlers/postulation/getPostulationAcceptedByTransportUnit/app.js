const { dbConnection } = require('../../../database/config');
const Postulation = require('../../../models/postulation');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    const transportUnitId = event.pathParameters.transportUnitId;
    console.log('transportUnitId===>>>', transportUnitId);
    const postulation = await Postulation.find({
      transportUnitId: transportUnitId,
      acceptedDate: {
        $exists: true,
      },
      'account.enable': 1,
    });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        postulations: postulation,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

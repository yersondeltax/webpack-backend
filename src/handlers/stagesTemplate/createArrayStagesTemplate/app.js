const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const arrayStages = body;
    let newArrayStages = [];

    let stageId = arrayStages[0];

    let dispatchTypeId = stageId.dispatchTypeId || '';
    console.log('dispatchTypeId=====>>>', dispatchTypeId);
    let typeServiceId = stageId.typeServiceId || '';
    console.log('typeServiceId=====>>>', typeServiceId);
    let placeId = stageId.placeId || '';
    console.log('placeId=====>>>', placeId);

    const stagesTemplates = await StagesTemplate.find({
      dispatchTypeId: dispatchTypeId,
      typeServiceId: typeServiceId,
      placeId: placeId,
    });

    console.log('stagesTemplates=====>>>', stagesTemplates);

    if (stagesTemplates.length > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Ya cuenta con etapas creadas',
        }),
      };
    }

    for (const stage of arrayStages) {
      let data = { ...stage, companyId: companyId };
      let newStagesTemplate = new StagesTemplate(data);
      let stagesTemplateSaved = await newStagesTemplate.save();
      newArrayStages.push(stagesTemplateSaved);
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Etapas creadas exitosamente',
        stagesTemplate: newArrayStages,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

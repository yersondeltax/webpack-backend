const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const Place = require('../../../models/place');
const DispatchType = require('../../../models/dispatchType');
const TypeServices = require('../../../models/typeServices');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const dispatchTypeId = event.queryStringParameters.dispatchTypeId || '';
    console.log('dispatchTypeId===>>>', dispatchTypeId);
    const placeId = event.queryStringParameters.placeId || '';
    console.log('placeId===>>>', placeId);
    const companyId = event.queryStringParameters.companyId || '';
    console.log('companyId===>>>', companyId);

    // // const stagesTemplate = await .find();
    // const stagesTemplate = await StagesTemplate.find({
    //   dispatchTypeId: dispatchTypeId,
    //   typeServiceId: typeServiceId,
    //   placeId: placeId,
    // }).sort({ order: 1 });

    let typeServices = [];
    typeServices = await TypeServices.find();

    for (const typeServi of typeServices) {
      let typeServicesId = typeServi._id;
      let stagesTemplates = [];
      stagesTemplates = await StagesTemplate.find({
        dispatchTypeId: dispatchTypeId,
        typeServiceId: typeServicesId,
        placeId: placeId,
      });
      if (stagesTemplates.length > 0) {
        let deleteStagesTemplate = await StagesTemplate.deleteMany({
          dispatchTypeId: dispatchTypeId,
          typeServiceId: typeServicesId,
          placeId: placeId,
        });
        console.log('deleteStagesTemplate===>>>', deleteStagesTemplate);
      }
      let dataObject = [
        {
          name: 'Asignada',
          dispatchTypeId: dispatchTypeId,
          typeServiceId: typeServicesId,
          placeId: placeId,
          order: 1,
          tasks: [],
        },
        {
          name: 'Carguío',
          dispatchTypeId: dispatchTypeId,
          typeServiceId: typeServicesId,
          placeId: placeId,
          order: 2,
          tasks: [
            {
              name: 'Fin de carguío',
              changeStage: true,
              order: 1,
              viewCarrier: true,
              viewClient: true,
              allowFiles: false,
              pushNotification: true,
              emailNotification: false,
              smsNotification: false,
              validation: {
                operator: false,
                carrier: false,
                client: false,
              },
              allow: {
                operator: true,
                carrier: true,
                client: true,
              },
            },
          ],
        },
        {
          name: 'Tránsito',
          dispatchTypeId: dispatchTypeId,
          typeServiceId: typeServicesId,
          placeId: placeId,
          order: 3,
          tasks: [],
        },
        {
          name: 'Descarguío',
          dispatchTypeId: dispatchTypeId,
          typeServiceId: typeServicesId,
          placeId: placeId,
          order: 4,
          tasks: [
            {
              name: 'Fin de descarguío',
              changeStage: true,
              order: 1,
              viewCarrier: true,
              viewClient: true,
              allowFiles: false,
              pushNotification: true,
              emailNotification: false,
              smsNotification: false,
              validation: {
                operator: false,
                carrier: false,
                client: false,
              },
              allow: {
                operator: true,
                carrier: true,
                client: true,
              },
            },
          ],
        },
      ];
      for (const newDataObject of dataObject) {
        let data = { ...newDataObject, companyId: companyId };
        let newStagesTemplate = new StagesTemplate(data);
        let stagesTemplateSaved = await newStagesTemplate.save();
      }
    }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Creacion de tareas exitosas',
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

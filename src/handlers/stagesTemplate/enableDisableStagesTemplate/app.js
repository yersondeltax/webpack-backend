const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const stagesTemplateId = event.pathParameters.stagesTemplateId;
    console.log('stagesTemplateId===>>>', stagesTemplateId);
    let stagesTemplateUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      stagesTemplateUpdate = await StagesTemplate.updateOne(
        { _id: stagesTemplateId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      stagesTemplateUpdate = await StagesTemplate.updateOne(
        { _id: stagesTemplateId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('stagesTemplateUpdate===>>>', stagesTemplateUpdate);
    if (stagesTemplateUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Etapa ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

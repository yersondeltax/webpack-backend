const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    let data = { ...body, companyId: companyId };
    const newStagesTemplate = new StagesTemplate(data);
    const stagesTemplateSaved = await newStagesTemplate.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Etapa creado exitosamente',
        stagesTemplate: stagesTemplateSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

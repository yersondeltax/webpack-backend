const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const stagesTemplateId = event.pathParameters.stagesTemplateId;
    console.log('stagesTemplateId===>>>', stagesTemplateId);
    const taskId = event.pathParameters.taskId;
    console.log('taskId===>>>', taskId);

    let updateDate = moment().format();
    let stagesTemplateUpdate = await StagesTemplate.findOneAndUpdate(
      { _id: stagesTemplateId },
      { $pull: { tasks: { _id: taskId } }, $set: { 'account.updateDate': updateDate } },
      { new: true }
    );

    console.log('stagesTemplateUpdate===>>>', stagesTemplateUpdate);
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tarea eliminada.',
        stagesTemplate: stagesTemplateUpdate,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

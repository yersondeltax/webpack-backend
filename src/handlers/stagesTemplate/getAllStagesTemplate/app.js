const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const Place = require('../../../models/place');
const DispatchType = require('../../../models/dispatchType');
const TypeServices = require('../../../models/typeServices');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    let companyId = event?.queryStringParameters?.companyId;
    console.log('companyId====>>', companyId);
    await dbConnection(process.env.dataBaseConnection);

    const stagesTemplate = await StagesTemplate.find({ companyId: companyId });

    console.log('stagesTemplate======>>>', stagesTemplate);
    console.log('longitud======>>>', stagesTemplate.length);
    console.log('llegooooo acaaaa ');

    const prueba = await StagesTemplate.aggregate([
      // {
      //   $match: {
      //     companyId: '60ad411f1e97db0008a2e606',
      //   },
      // },
      {
        $group: {
          _id: {
            dispatchTypeId: '$dispatchTypeId',
            typeServiceId: '$typeServiceId',
            placeId: '$placeId',
          },
          id: {
            $addToSet: '$_id',
          },
          field: {
            $sum: 1,
          },
        },
      },
    ]);
    // stagesTemplate = stagesTemplate._doc;
    console.log('llegooooo acaaaa222 ');
    console.log('prueba======>>>', prueba);
    let arrayFind = [];
    for (const element of prueba) {
      console.log('element====>>', element);
      let arrayId = element.id || [];
      let dispatchTypeId = element._id.dispatchTypeId || '';
      let typeServiceId = element._id.typeServiceId;
      let placeId = element._id.placeId;
      let newArray = [];
      for (const ids of arrayId) {
        console.log('ids====>>>>', ids);
        for (const stages of stagesTemplate) {
          console.log('stages=====>>', stages);
          let objetElement = {
            id: ids,
          };
          let objetFeature = {
            id: stages._id,
          };
          // let respuesta = JSON.stringify(objetElement) === JSON.stringify(objetFeature);
          // console.log('respuestas=====>>', respuesta);
          if (JSON.stringify(objetElement) === JSON.stringify(objetFeature)) {
            console.log('entro en el igual');
            newArray.push(stages);
            break;
          }
        }
      }
      let typeServices = await TypeServices.findOne({ _id: typeServiceId });
      let dispatchType = await DispatchType.findOne({ _id: dispatchTypeId });
      let place = await Place.findOne({ _id: placeId });
      let object = {
        stages: newArray,
        typeServices,
        dispatchType,
        place,
      };

      arrayFind.push(object);
    }
    console.log('arrayFind====>', arrayFind);
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        stagesTemplate: arrayFind,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

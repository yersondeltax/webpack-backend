const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const stagesTemplateId = event.pathParameters.stagesTemplateId;
    console.log('stagesTemplateId===>>>', stagesTemplateId);

    let stagesTemplate = await StagesTemplate.findOne({ _id: stagesTemplateId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        stagesTemplate: stagesTemplate,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

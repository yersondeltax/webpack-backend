const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const Place = require('../../../models/place');
const DispatchType = require('../../../models/dispatchType');
const TypeServices = require('../../../models/typeServices');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    const dispatchTypeId = event.pathParameters.dispatchTypeId || '';
    console.log('dispatchTypeId===>>>', dispatchTypeId);
    const typeServiceId = event.pathParameters.typeServiceId || '';
    console.log('typeServiceId===>>>', typeServiceId);
    const placeId = event.pathParameters.placeId || '';
    console.log('placeId===>>>', placeId);

    // const stagesTemplate = await .find();
    const stagesTemplate = await StagesTemplate.find({
      dispatchTypeId: dispatchTypeId,
      typeServiceId: typeServiceId,
      placeId: placeId,
    }).sort({ order: 1 });

    let typeServices = await TypeServices.findOne({ _id: typeServiceId });
    let dispatchType = await DispatchType.findOne({ _id: dispatchTypeId });
    let place = await Place.findOne({ _id: placeId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        stagesTemplate: stagesTemplate,
        typeServices,
        dispatchType,
        place,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

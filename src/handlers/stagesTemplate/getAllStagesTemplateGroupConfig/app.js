const { dbConnection } = require('../../../database/config');
const StagesTemplate = require('../../../models/stagesTemplate');
const Place = require('../../../models/place');
const DispatchType = require('../../../models/dispatchType');
const TypeServices = require('../../../models/typeServices');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);

    // const stagesTemplate = await StagesTemplate.find();

    const prueba = await StagesTemplate.aggregate([
      {
        $group: {
          _id: {
            dispatchTypeId: '$dispatchTypeId',
            typeServiceId: '$typeServiceId',
            placeId: '$placeId',
          },
          id: {
            $addToSet: '$_id',
          },
          field: {
            $sum: 1,
          },
        },
      },
    ]);
    // stagesTemplate = stagesTemplate._doc;
    console.log('llegooooo acaaaa222 ');
    console.log('prueba======>>>', prueba);
    let arrayFind = [];
    for (const element of prueba) {
      console.log('element====>>', element);

      let dispatchTypeId = element._id.dispatchTypeId || '';
      let typeServiceId = element._id.typeServiceId || '';
      let placeId = element._id.placeId || '';
      let typeServices = await TypeServices.findOne({ _id: typeServiceId });
      let dispatchType = await DispatchType.findOne({ _id: dispatchTypeId });
      let place = await Place.findOne({ _id: placeId });
      let object = {
        typeServices,
        dispatchType,
        place,
      };

      arrayFind.push(object);
    }
    console.log('arrayFind====>', arrayFind);
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        stagesTemplate: arrayFind,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

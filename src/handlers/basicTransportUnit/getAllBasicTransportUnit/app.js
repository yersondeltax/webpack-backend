const { dbConnection } = require('../../../database/config');
const BasicTransportUnit = require('../../../models/basicTransportUnit');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let userId = event?.queryStringParameters?.userId;
    // let enable = event?.queryStringParameters?.enable || 'false';

    // console.log('userId====>>', userId);
    // console.log('enable====>>', enable);
    await dbConnection(process.env.dataBaseConnection);
    let filter = event?.queryStringParameters?.filter;
    console.log('filter===>>', filter);
    let objectFilter = {};
    if (filter) {
      objectFilter = {
        'account.enable': true,
      };
    }
    // let useData = await User.findById(userId);
    // let roles = useData?.roles || [];
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.write);
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.read);
    // console.log('module====>>', module);
    // if (!module) {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No tienes permiso para realizar esta acción',
    //     }),
    //   };
    // }
    let basicTransportUnit = [];
    // if (enable == 'true') {
    // basicTransportUnit = await BasicTransportUnit.find({ 'account.enable': true });
    // } else {
    basicTransportUnit = await BasicTransportUnit.find({ ...objectFilter }).sort({
      'account.createDate': 1,
    });
    // }

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        basicTransportUnit,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const BasicTransportUnit = require('../../../models/basicTransportUnit');
const User = require('../../../models/user');

exports.lambdaHandler = async (event, context) => {
  const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  };
  try {
    // let userId = event?.queryStringParameters?.userId;
    // console.log('userId====>>', userId);

    await dbConnection(process.env.dataBaseConnection);

    // let useData = await User.findById(userId);
    // let roles = useData?.roles || [];
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.write);
    // // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.read);
    // console.log('module====>>', module);
    // if (!module) {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No tienes permiso para realizar esta acción',
    //     }),
    //   };
    // }
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);
    let enable = body.enable || false;
    let date = moment().format();
    const basicTransportUnitId = event.pathParameters.basicTransportUnitId;
    console.log('basicTransportUnitId===>>>', basicTransportUnitId);
    let basicTransportUnitUpdate;
    let mensaje = 'desabilitado';
    if (enable) {
      mensaje = 'habilitado';
      basicTransportUnitUpdate = await BasicTransportUnit.updateOne(
        { _id: basicTransportUnitId },
        {
          $set: { 'account.enable': enable, 'account.enableDate': date },
        }
      );
    } else {
      mensaje = 'desabilitado';
      basicTransportUnitUpdate = await BasicTransportUnit.updateOne(
        { _id: basicTransportUnitId },
        {
          $set: { 'account.enable': enable, 'account.disableDate': date },
        }
      );
    }

    console.log('basicTransportUnitUpdate===>>>', basicTransportUnitUpdate);
    if (basicTransportUnitUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: `Tipo de transporte ${mensaje}.`,
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

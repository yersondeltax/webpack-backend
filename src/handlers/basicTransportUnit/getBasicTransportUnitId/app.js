const { dbConnection } = require('../../../database/config');
const BasicTransportUnit = require('../../../models/basicTransportUnit');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const basicTransportUnitId = event.pathParameters.basicTransportUnitId;
    console.log('basicTransportUnitId===>>>', basicTransportUnitId);

    let basicTransportUnitOne = await BasicTransportUnit.findOne({ _id: basicTransportUnitId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        basicTransportUnit: basicTransportUnitOne,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

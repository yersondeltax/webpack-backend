const { dbConnection } = require('../../../database/config');
const BasicTransportUnit = require('../../../models/basicTransportUnit');
const User = require('../../../models/user');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    // let userId = event?.queryStringParameters?.userId;
    console.log('entro aca');
    console.log('entro aca');
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    // let useData = await User.findById(userId);
    // let roles = useData?.roles || [];
    // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.write);
    // // let module = roles.find((module) => module.code === 'basicTypeTransportUnit' && module.read);
    // console.log('module====>>', module);
    // if (!module) {
    //   return {
    //     statusCode: 400,
    //     headers,
    //     body: JSON.stringify({
    //       message: 'No tienes permiso para realizar esta acción',
    //     }),
    //   };
    // }

    const newBasicTransportUnit = new BasicTransportUnit(body);
    const basicTransportUnitSaved = await newBasicTransportUnit.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tipo de transporte creado exitosamente',
        basicTransportUnit: basicTransportUnitSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

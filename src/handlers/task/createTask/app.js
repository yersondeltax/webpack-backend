const { dbConnection } = require('../../../database/config');
const Task = require('../../../models/task');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);

    const newTask = new Task(body);
    const taskSaved = await newTask.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Tarea creada exitosamente',
        task: taskSaved,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

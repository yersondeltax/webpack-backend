const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Task = require('../../../models/task');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const taskId = event.pathParameters.taskId;
    console.log('taskId===>>>', taskId);

    let updateDate = moment().format();
    let taskUpdate = await Task.updateOne(
      { _id: taskId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('taskUpdate===>>>', taskUpdate);
    if (taskUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Tarea actualizada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actualizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

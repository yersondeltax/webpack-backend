const { dbConnection } = require('../../../database/config');
const Task = require('../../../models/task');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);

    const taskId = event.pathParameters.taskId;
    console.log('taskId===>>>', taskId);

    let task = await Task.findOne({ _id: taskId });

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        task,
      }),
    };
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

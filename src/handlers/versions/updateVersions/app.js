const moment = require('moment');

const { dbConnection } = require('../../../database/config');
const Versions = require('../../../models/versions');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    console.log('event ==> ', event);
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    console.log('body===>>>', body);

    const versionsId = event.pathParameters.versionsId;
    console.log('versionsId===>>>', versionsId);
    let updateDate = moment().format();

    let versionsUpdate = await Versions.updateOne(
      { _id: versionsId },
      {
        $set: { ...body, 'account.updateDate': updateDate },
      }
    );
    console.log('versionsUpdate===>>>', versionsUpdate);
    if (versionsUpdate.nModified > 0) {
      return {
        statusCode: 200,
        headers,
        body: JSON.stringify({
          message: 'Version actualizada.',
        }),
      };
    } else {
      return {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          message: 'No se pudo actulizar.',
        }),
      };
    }
  } catch (err) {
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

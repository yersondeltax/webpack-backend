const { dbConnection } = require('../../../database/config');
const Versions = require('../../../models/versions');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    const body = typeof event.body !== 'undefined' ? JSON.parse(event.body) : event;
    await dbConnection(process.env.dataBaseConnection);
    const newVersions = new Versions(body);
    const versionsSaved = await newVersions.save();

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        message: 'Configuración registrada',
        versions: versionsSaved,
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

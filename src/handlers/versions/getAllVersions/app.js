const { dbConnection } = require('../../../database/config');
const Versions = require('../../../models/versions');
const headers = {
  'Access-Control-Allow-Origin': '*',
  'Content-Type': 'application/json',
};

exports.lambdaHandler = async (event, context) => {
  try {
    await dbConnection(process.env.dataBaseConnection);
    let versions = await Versions.find();
    console.log('versions====>>', versions);

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        versions: versions,
      }),
    };
  } catch (err) {
    console.log('error===>>', err);
    return {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        error: err,
        message: 'Algo salio mal.',
      }),
    };
  }
};

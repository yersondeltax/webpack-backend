
var mongoose = require('mongoose');

const dbConnection = async (dataBaseConnection:string) => {
  console.log('cadena de conexion : ',dataBaseConnection)
  try {
    await mongoose.connect(
      dataBaseConnection,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );

    console.log('Base de datos online');
  } catch (error) {
    throw new Error('Error a la hora de iniciar la base de datos');
  }
};

module.exports = {
  dbConnection,
};

const mongoose = require('mongoose');

const dbConnection = async (dataBaseConnection) => {
  try {
    console.log('cadena de conexion : ', dataBaseConnection);
    await mongoose.connect(
      dataBaseConnection,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      }
    );

    console.log('Base de datos online');
  } catch (error) {
    throw new Error('Error a la hora de iniciar la base de datos');
  }
};

module.exports = {
  dbConnection,
};

const { Schema, model } = require('mongoose');

const notificationSchema = Schema({
  operationId: { type: Schema.ObjectId },
  travelId: { type: Schema.ObjectId },
  loadingOrderId: { type: Schema.ObjectId },
  taskId: { type: Schema.ObjectId },
  checkPointId: { type: Schema.ObjectId },
  postulationId: { type: Schema.ObjectId },
  userId: { type: Schema.ObjectId },
  tokenUser: {
    type: String,
  },
  message: {
    type: String,
  },
  type: {
    type: String,
  },
  sent: {
    type: Boolean,
    default: false,
  },
  read: {
    type: Boolean,
    default: false,
  },
  createDate: {
    type: Date,
    default: Date.now,
  },
});

notificationSchema.methods.toJSON = function () {
  const { __v, ...notification } = this.toObject();
  return notification;
};

module.exports = model('notification', notificationSchema);

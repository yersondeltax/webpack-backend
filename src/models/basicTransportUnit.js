const { Schema, model } = require('mongoose');

const basicTransportUnitSchema = Schema({
  name: {
    type: String,
  },
  path: {
    type: String,
  },
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

basicTransportUnitSchema.methods.toJSON = function () {
  const { __v, ...basicTransportUnit } = this.toObject();
  return basicTransportUnit;
};

module.exports = model('basicTransportUnit', basicTransportUnitSchema);

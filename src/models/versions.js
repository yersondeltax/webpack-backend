const { Schema, model } = require('mongoose');

const versionsSchema = Schema({
  versionApp: {
    type: String,
  },
  row: {
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
  },
});

versionsSchema.methods.toJSON = function () {
  const { __v, ...versions } = this.toObject();
  return versions;
};

module.exports = model('versions', versionsSchema);

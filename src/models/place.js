const { Schema, model } = require('mongoose');

const placeSchema = Schema({
  countryName: {
    type: String,
  },
  states: [
    {
      name: {
        type: String,
      },
      province: [
        {
          provinceName: {
            type: String,
          },
          municipality: [
            {
              municipalityName: {
                type: String,
              },
            },
          ],
        },
      ],
    },
  ],
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

placeSchema.methods.toJSON = function () {
  const { __v, ...place } = this.toObject();
  return place;
};

module.exports = model('place', placeSchema);

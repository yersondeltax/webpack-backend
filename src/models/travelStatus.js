const { Schema, model } = require('mongoose');

const travelStatusSchema = Schema({
  name: {
    type: String,
  },
  order: {
    type: Number,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

travelStatusSchema.methods.toJSON = function () {
  const { __v, ...travelStatus } = this.toObject();
  return travelStatus;
};

module.exports = model('travelStatus', travelStatusSchema);

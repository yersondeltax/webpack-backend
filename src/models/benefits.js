const { Schema, model } = require('mongoose');

const benefitsSchema = Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  path: {
    type: String,
  },
  order: {
    type: Number,
  },
  price: {
    type: Number,
  },
  percentage: {
    type: Number,
  },
  phone: {
    type: String,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

benefitsSchema.methods.toJSON = function () {
  const { __v, ...benefits } = this.toObject();
  return benefits;
};

module.exports = model('benefits', benefitsSchema);

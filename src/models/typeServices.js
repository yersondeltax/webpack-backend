const { Schema, model } = require('mongoose');

const typeServicesSchema = Schema({
  name: {
    type: String,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

typeServicesSchema.methods.toJSON = function () {
  const { __v, ...typeServices } = this.toObject();
  return typeServices;
};

module.exports = model('typeServices', typeServicesSchema);

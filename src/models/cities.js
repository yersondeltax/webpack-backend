const { Schema, model } = require('mongoose');

const citiesSchema = Schema({
  name: {
    type: String,
  },
  countryName: {
    type: String,
  },
  statesName: {
    type: String,
  },
  provinceName: {
    type: String,
  },
  municipalityName: {
    type: String,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

citiesSchema.methods.toJSON = function () {
  const { __v, ...cities } = this.toObject();
  return cities;
};

module.exports = model('cities', citiesSchema);

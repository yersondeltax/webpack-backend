const { Schema, model } = require('mongoose');

const loadingOrderStatusSchema = Schema({
  name: {
    type: String,
  },
  order: {
    type: Number,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

loadingOrderStatusSchema.methods.toJSON = function () {
  const { __v, ...loadingOrderStatus } = this.toObject();
  return loadingOrderStatus;
};

module.exports = model('loadingOrderStatus', loadingOrderStatusSchema);

const { Schema, model } = require('mongoose');

const routeSchema = Schema({
  origin: {
    cityOriginId: { type: Schema.ObjectId },
    cityOrigin: {
      type: String,
    },
  },
  destination: {
    cityDestinationId: { type: Schema.ObjectId },
    cityDestination: {
      type: String,
    },
  },
  checkPoints: [
    {
      lat: {
        type: Number,
      },
      lng: {
        type: Number,
      },
      name: {
        type: String,
      },
      km: {
        type: Number,
        default: 0,
      },
      type: {
        type: String,
        default: 'A-B',
      },
    },
  ],
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

routeSchema.methods.toJSON = function () {
  const { __v, ...route } = this.toObject();
  return route;
};

module.exports = model('route', routeSchema);

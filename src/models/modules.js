const { Schema, model } = require('mongoose');

const modulesSchema = Schema({
  name: {
    type: String,
  },
  code: {
    type: String,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

modulesSchema.methods.toJSON = function () {
  const { __v, ...modules } = this.toObject();
  return modules;
};

module.exports = model('modules', modulesSchema);

const { Schema, model } = require('mongoose');

const postulationSchema = Schema({
  travelId: { type: Schema.ObjectId },
  freightValue: {
    type: Number,
  },
  typeCurrencyFreightId: { type: Schema.ObjectId },
  transportUnitId: { type: Schema.ObjectId },
  invoice: {
    type: Boolean,
  },
  freightValueOperator: {
    type: Number,
  },
  typeCurrencyFreightOperatorId: { type: Schema.ObjectId },
  invoiceOperator: {
    type: Boolean,
  },
  postulateDate: {
    type: Date,
  },
  acceptedDate: {
    type: Date,
  },
  confirmedDate: {
    type: Date,
  },
  cancelledDate: {
    type: Date,
  },
  rejectDate: {
    type: Date,
  },
  commentReject: {
    type: String,
  },
  declineDate: {
    type: Date,
  },
  account: {
    enable: {
      type: Number,
      default: 1,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

postulationSchema.methods.toJSON = function () {
  const { __v, ...postulation } = this.toObject();
  return postulation;
};

module.exports = model('postulation', postulationSchema);

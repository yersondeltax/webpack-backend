const { Schema, model } = require('mongoose');

const companyClientSchema = Schema({
  name: {
    type: String,
  },
  taxId: {
    type: String,
  },
  address: {
    type: String,
  },
  country: {
    type: String,
  },
  city: {
    type: String,
  },
  postalCode: {
    type: String,
  },
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

companyClientSchema.methods.toJSON = function () {
  const { __v, ...companyClient } = this.toObject();
  return companyClient;
};

module.exports = model('companyClient', companyClientSchema);

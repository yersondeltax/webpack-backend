const { Schema, model } = require('mongoose');

const typeTransportUnitSchema = Schema({
  description: {
    type: String,
  },
  features: [
    {
      featuresTransportUnitId: { type: Schema.ObjectId },
      name: {
        type: String,
      },
      valueId: { type: Schema.ObjectId },
      valueQuantitative: {
        type: Number,
      },
      valueQualitative: {
        type: String,
      },
    },
  ],
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

typeTransportUnitSchema.methods.toJSON = function () {
  const { __v, ...typeTransportUnit } = this.toObject();
  return typeTransportUnit;
};

module.exports = model('typeTransportUnit', typeTransportUnitSchema);

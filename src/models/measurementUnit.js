const { Schema, model } = require('mongoose');

const measurementUnitSchema = Schema({
  name: {
    type: String,
  },
  abbreviation: {
    type: String,
  },
  type: {
    type: String,
  },
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

measurementUnitSchema.methods.toJSON = function () {
  const { __v, ...measurementUnit } = this.toObject();
  return measurementUnit;
};

module.exports = model('measurementUnit', measurementUnitSchema);

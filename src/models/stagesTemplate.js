const { Schema, model } = require('mongoose');

const stagesTemplateSchema = Schema({
  name: {
    type: String,
  },
  dispatchTypeId: { type: Schema.ObjectId },
  typeServiceId: { type: Schema.ObjectId },
  placeId: { type: Schema.ObjectId },
  order: {
    type: Number,
  },
  tasks: [
    {
      taskId: { type: Schema.ObjectId },
      name: {
        type: String,
      },
      changeStage: {
        type: Boolean,
      },
      order: {
        type: Number,
      },
      viewCarrier: {
        type: Boolean,
      },
      viewClient: {
        type: Boolean,
      },
      allowFiles: {
        type: Boolean,
      },
      pushNotification: {
        type: Boolean,
      },
      emailNotification: {
        type: Boolean,
      },
      smsNotification: {
        type: Boolean,
      },
      validation: {
        operator: {
          type: Boolean,
        },
        carrier: {
          type: Boolean,
        },
        client: {
          type: Boolean,
        },
      },
      allow: {
        operator: {
          type: Boolean,
        },
        carrier: {
          type: Boolean,
        },
        client: {
          type: Boolean,
        },
      },
    },
  ],
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

stagesTemplateSchema.methods.toJSON = function () {
  const { __v, ...stagesTemplate } = this.toObject();
  return stagesTemplate;
};

module.exports = model('stagesTemplate', stagesTemplateSchema);

const { Schema, model } = require('mongoose');

const brandSchema = Schema({
  brand: {
    type: String,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

brandSchema.methods.toJSON = function () {
  const { __v, ...brand } = this.toObject();
  return brand;
};

module.exports = model('brand', brandSchema);

const { Schema, model } = require('mongoose');

const boardingModeSchema = Schema({
  name: {
    type: String,
  },
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

boardingModeSchema.methods.toJSON = function () {
  const { __v, ...boardingMode } = this.toObject();
  return boardingMode;
};

module.exports = model('boardingMode', boardingModeSchema);

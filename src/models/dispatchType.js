const { Schema, model } = require('mongoose');

const dispatchTypeSchema = Schema({
  name: {
    type: String,
  },
  companyId: { type: Schema.ObjectId },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

dispatchTypeSchema.methods.toJSON = function () {
  const { __v, ...dispatchType } = this.toObject();
  return dispatchType;
};

module.exports = model('dispatchType', dispatchTypeSchema);

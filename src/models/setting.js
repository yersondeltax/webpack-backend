const { Schema, model } = require('mongoose');

const settingSchema = Schema({
  featureRestriction: {
    type: Boolean,
  },
  row: {
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
  },
});

settingSchema.methods.toJSON = function () {
  const { __v, ...setting } = this.toObject();
  return setting;
};

module.exports = model('setting', settingSchema);

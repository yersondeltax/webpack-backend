const { Schema, model } = require('mongoose');

const operationStatusSchema = Schema({
  name: {
    type: String,
  },
  order: {
    type: Number,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

operationStatusSchema.methods.toJSON = function () {
  const { __v, ...operationStatus } = this.toObject();
  return operationStatus;
};

module.exports = model('operationStatus', operationStatusSchema);

const { Schema, model } = require('mongoose');

const checkPointsSchema = Schema({
  checkPointId: { type: Schema.ObjectId },
  dateTime: {
    type: Date,
  },
  exit: {
    type: Boolean,
  },
  lat: {
    type: Number,
  },
  lng: {
    type: Number,
  },
  loadingOrderId: { type: Schema.ObjectId },
  transportUnitId: { type: Schema.ObjectId },
  driver: {
    type: Boolean,
    default: true,
  },
  taskId: { type: Schema.ObjectId },
});

checkPointsSchema.methods.toJSON = function () {
  const { __v, ...checkPoints } = this.toObject();
  return checkPoints;
};

module.exports = model('checkPoints', checkPointsSchema);

const { Schema, model } = require('mongoose');

const newsSchema = Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  path: {
    type: String,
  },
  link: {
    type: String,
  },
  order: {
    type: Number,
  },
  account: {
    enable: {
      type: Boolean,
      default: true,
    },
    enableDate: {
      type: Date,
    },
    disableDate: {
      type: Date,
    },
    createDate: {
      type: Date,
      default: Date.now,
    },
    updateDate: {
      type: Date,
    },
  },
});

newsSchema.methods.toJSON = function () {
  const { __v, ...news } = this.toObject();
  return news;
};

module.exports = model('news', newsSchema);

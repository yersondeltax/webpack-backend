
basicTransportUnit:
	NODE_DEV=development NODE_BUILD_TYPE=BASICTRANSPORTUNIT npm run-script build

benefits:
	NODE_DEV=development NODE_BUILD_TYPE=BENEFITS npm run-script build

boardingMode:
	NODE_DEV=development NODE_BUILD_TYPE=BOARDINGMODE npm run-script build

brand:
	NODE_DEV=development NODE_BUILD_TYPE=BRAND npm run-script build

category:
	NODE_DEV=development NODE_BUILD_TYPE=CATEGORY npm run-script build

checkPoints:
	NODE_DEV=development NODE_BUILD_TYPE=CHECKPOINTS npm run-script build

cities:
	NODE_DEV=development NODE_BUILD_TYPE=CITIES npm run-script build

client:
	NODE_DEV=development NODE_BUILD_TYPE=CLIENT npm run-script build

company:
	NODE_DEV=development NODE_BUILD_TYPE=COMPANY npm run-script build

companyClient:
	NODE_DEV=development NODE_BUILD_TYPE=COMPANYCLIENT npm run-script build

dashboard:
	NODE_DEV=development NODE_BUILD_TYPE=DASHBOARD npm run-script build

dispatchType:
	NODE_BUILD_TYPE=DISPATCHTYPE npm run-script build

featuresTransportUnit:
	NODE_BUILD_TYPE=FEATURESTRANSPORTUNIT npm run-script build

loadingOrderStatus:
	NODE_BUILD_TYPE=LOADINGORDERSTATUS npm run-script build

measurementUnit:
	NODE_BUILD_TYPE=MEASUREMENTUNIT npm run-script build

modules:
	NODE_BUILD_TYPE=MODULES npm run-script build

news:
	NODE_BUILD_TYPE=NEWS npm run-script build

travel:
	NODE_BUILD_TYPE=TRAVEL npm run-script build

notification:
	NODE_BUILD_TYPE=NOTIFICATION npm run-script build

operation:
	NODE_BUILD_TYPE=OPERATION npm run-script build

operationStatus:
	NODE_BUILD_TYPE=OPERATIONSTATUS npm run-script build

Operator:
	NODE_BUILD_TYPE=OPERATOR npm run-script build

place:
	NODE_BUILD_TYPE=PLACE npm run-script build

postulation:
	NODE_BUILD_TYPE=POSTULATION npm run-script build

rating:
	NODE_BUILD_TYPE=RATING npm run-script build

report:
	NODE_BUILD_TYPE=REPORT npm run-script build

route:
	NODE_BUILD_TYPE=ROUTE npm run-script build

setting:
	NODE_BUILD_TYPE=SETTING npm run-script build

stages:
	NODE_BUILD_TYPE=STAGES npm run-script build

stagesTemplate:
	NODE_BUILD_TYPE=STAGESTEMPLATE npm run-script build

task:
	NODE_BUILD_TYPE=TASK npm run-script build

transportUnit:
	NODE_BUILD_TYPE=TRANSPORTUNIT npm run-script build

travelStatus:
	NODE_BUILD_TYPE=TRAVELSTATUS npm run-script build

typeServices:
	NODE_BUILD_TYPE=TYPESERVICES npm run-script build

TypeTransporUnit:
	NODE_BUILD_TYPE=TYPETRANSPORTUNIT npm run-script build

versions:
	NODE_BUILD_TYPE=VERSIONS npm run-script build

vipDrivers:
	NODE_BUILD_TYPE=VIPDRIVERS npm run-script build

users:
	NODE_BUILD_TYPE=USERS npm run-script build

carrier:
	NODE_BUILD_TYPE=CARRIER npm run-script build



const outputsBasicTransportUnit = "src/handlers/basicTransportUnit/.aws-sam/build"
const outputsBenefits = "src/handlers/benefits/.aws-sam/build"
const outputsBoardingMode = "src/handlers/boardingMode/.aws-sam/build"
const outputsBrand = "src/handlers/brand/.aws-sam/build"
const outputsCategory = "src/handlers/category/.aws-sam/build"
const outputsCheckPoints = "src/handlers/checkPoints/.aws-sam/build"
const outputsCities = "src/handlers/cities/.aws-sam/build"
const outputsClient = "src/handlers/client/.aws-sam/build"
const outputsCompany = "src/handlers/company/.aws-sam/build"
const outputsCompanyClient = "src/handlers/companyClient/.aws-sam/build"
const outputsDashboard = "src/handlers/dashboard/.aws-sam/build"
const outputsDispatchType = "src/handlers/dispatchType/.aws-sam/build"
const outputsFeaturesTransportUnit = "src/handlers/featuresTransportUnit/.aws-sam/build"
const outputsLoadingOrderStatus = "src/handlers/loadingOrderStatus/.aws-sam/build"
const outputsMeasurementUnit = "src/handlers/measurementUnit/.aws-sam/build"
const outputsModules = "src/handlers/modules/.aws-sam/build"
const outputsNews = "src/handlers/news/.aws-sam/build"
const outputsTravel = "src/handlers/travel/.aws-sam/build"
const outputsNotification = "src/handlers/notification/.aws-sam/build"
const outputsOperation = "src/handlers/operation/.aws-sam/build"
const outputsOperationStatus = "src/handlers/operationStatus/.aws-sam/build"
const outputsOperator = "src/handlers/Operator/.aws-sam/build"
const outputsPlace = "src/handlers/place/.aws-sam/build"
const outputsPostulation = "src/handlers/postulation/.aws-sam/build"
const outputsRating = "src/handlers/rating/.aws-sam/build"
const outputsReport = "src/handlers/report/.aws-sam/build"
const outputsRoute = "src/handlers/route/.aws-sam/build"
const outputsSetting = "src/handlers/setting/.aws-sam/build"
const outputsStages = "src/handlers/stages/.aws-sam/build"
const outputsStagesTemplate = "src/handlers/stagesTemplate/.aws-sam/build"
const outputsTask = "src/handlers/task/.aws-sam/build"
const outputsTransportUnit = "src/handlers/transportUnit/.aws-sam/build"
const outputsTravelStatus = "src/handlers/travelStatus/.aws-sam/build"
const outputsTypeServices = "src/handlers/typeServices/.aws-sam/build"
const outputsTypeTransportUnit = "src/handlers/typeTransportUnit/.aws-sam/build"
const outputsVersions = "src/handlers/versions/.aws-sam/build"
const outputsVipDrivers = "src/handlers/vipDrivers/.aws-sam/build"
const outputsUsers = "src/handlers/users/.aws-sam/build"
const outputsCarrier = "src/handlers/carrier/.aws-sam/build"

export {
  outputsBasicTransportUnit,
  outputsBenefits,
  outputsBoardingMode,
  outputsBrand,
  outputsCategory,
  outputsCheckPoints,
  outputsCities,
  outputsClient,
  outputsCompany,
  outputsCompanyClient,
  outputsDashboard,
  outputsDispatchType,
  outputsFeaturesTransportUnit,
  outputsLoadingOrderStatus,
  outputsMeasurementUnit,
  outputsModules,
  outputsNews,
  outputsTravel,
  outputsNotification,
  outputsOperation,
  outputsOperationStatus,
  outputsOperator,
  outputsPlace,
  outputsPostulation,
  outputsRating,
  outputsReport,
  outputsRoute,
  outputsSetting,
  outputsStages,
  outputsStagesTemplate,
  outputsTask,
  outputsTransportUnit,
  outputsTravelStatus,
  outputsTypeServices,
  outputsTypeTransportUnit,
  outputsVersions,
  outputsVipDrivers,
  outputsUsers, 
  outputsCarrier,
};
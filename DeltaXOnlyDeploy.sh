#!/bin/bash

##Parameters##
# $1 -- AWS Access Key
# $2 -- AWS Secret Key
# $3 -- Process: deploy or destroy
# To deploy the project
# Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY deploy 
# To destroy the project
# Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY destroy 

#Check if parameters are input
if [ $# -gt 3 ] ; then
  cat << EOF
Bad Arguments
To deploy the project
Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY deploy
To destroy the project
Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY destroy
EOF
  exit 1
fi

#####---VARIABLES---#####
accessKey=$1
secretKey=$2
process=$3
awsRegion="us-east-1"
stackNameFront="DeltaxAPP"
templateFile="template.yaml"
echo    ####################################
echo    ##### Despligue Sistema DeltaX #####
echo    ####################################
firstPath=$(pwd)
dataBaseConnection="mongodb+srv://userDeltaxDev:6hjunZbBbiWJpSQB@deltaxdb-develop.airpm.mongodb.net/deltadev"
cp parameters_template.properties parameters.properties
accessKeyReplace=REACT_APP_S3_IDENTITY_ACCESS_KEY_ID_VALUE
secretKeyReplace=REACT_APP_S3_IDENTITY_SECRET_ACCESS_KEY_VALUE
s3NewsRegionReplace=REACT_APP_S3_IDENTITY_REGION_VALUE
sed -i "s#${accessKeyReplace}#${accessKey}#g" ./parameters.properties
sed -i "s#${secretKeyReplace}#${secretKey}#g" ./parameters.properties
sed -i "s#${s3NewsRegionReplace}#${awsRegion}#g" ./parameters.properties
########################
# Repository Variables #

key="jT8ZRnRTCswVTgE5Cr"
secret="sXmSncuZfeRCdtqZR8dL4QPPFyYCgQAx"
##############################
# users Variables to Replace #

envUsersAPIURL=REACT_APP_URL_USER_REGISTER_VALUE
envCongnitoUserPool=REACT_APP_COGNITO_POOLID_VALUE
envCognitoUserClient=REACT_APP_COGNITO_CLIENTID_VALUE
envUsersUrlNotification=REACT_APP_URL_NOTIFICATION_SERVICE_VALUE
emailCognito="info@telectronix.click"
arnEmail="arn:aws:ses:us-east-1:018374070137:identity/info@telectronix.click"
arnSns="arn:aws:sns:us-east-1:018374070137:app/GCM/snsApi"
keyFirstUser="373968026DXTI"

##########################################################
# operator Variables to Replace in parameters.properties #

envOperatorAPIURL=REACT_APP_URL_OPERATOR_SERVICE_VALUE

#################################################################
# operationStatus Variables to Replace in parameters.properties #

envOperationStatusAPIURL=REACT_APP_URL_OPERATION_STATUS_SERVICE_VALUE

###########################################################
# dashboard Variables to Replace in parameters.properties #

envDashboardAPIURL=REACT_APP_URL_GET_DASHBOARD_VALUE

##############################################################
# notification Variables to Replace in parameters.properties #

envNotificationAPIURL=REACT_APP_URL_NOTIFICATION_VALUE

###############################
# travel Variables to Replace #

envTravelAPIURL=REACT_APP_URL_MAPS_TRAVEL_VALUE
keyFireBaseLink="AIzaSyBumW34isWTAzG8iddvlMBjlm_9WzBV3mI"
domainUriPrefix="https://deltaxbeta.page.link"
dynamicLink="https://deltaxbeta.page.link"
nameAppInfo="com.deltaxlat.appdriver.dev"

####################################################################
# basicTransportUnit Variables to Replace in parameters.properties #

envBasicTransUnitAPIURL=REACT_APP_URL_BASIC_TYPE_TRANSPORT_UNIT_SERVICE_VALUE

##########################################################
# benefits Variables to Replace in parameters.properties #

envBenefitsAPIURL=REACT_APP_URL_BENEFIT_SERVICE_VALUE

##############################################################
# boardingMode Variables to Replace in parameters.properties #

envBoardingModeAPIURL=REACT_APP_URL_BOARDING_MODE_SERVICE_VALUE

#######################################################
# brand Variables to Replace in parameters.properties #

envBrandAPIURL=REACT_APP_URL_BRAND_SERVICE_VALUE

##########################################################
# category Variables to Replace in parameters.properties #

envCategoryAPIURL=REACT_APP_URL_CATEGORY_SERVICE_VALUE

#############################################################
# checkPoints Variables to Replace in parameters.properties #

envCheckPointsAPIURL=REACT_APP_URL_MAPS_CHECKPOINTS_VALUE
envCheckPointsLoadOrderService=REACT_APP_URL_LOAD_ORDER_SERVICE_CHECK_POINTS_VALUE

########################################################
# cities Variables to Replace in parameters.properties #

envCitiesAPIURL=REACT_APP_URL_CITY_SERVICE_VALUE

########################################################
# client Variables to Replace in parameters.properties #

envClientAPIURL=REACT_APP_URL_CLIENT_SERVICE_VALUE

####################################################################
# loadingOrderStatus Variables to Replace in parameters.properties #

envLoadingOrderAPIURL=REACT_APP_URL_STATUS_SERVICE_VALUE

#################################################################
# measurementUnit Variables to Replace in parameters.properties #

envMeasurementUnitAPIURL=REACT_APP_URL_MEASUREMENT_UNIT_SERVICE_VALUE

#######################################################
# place Variables to Replace in parameters.properties #

envPlaceAPIURL=REACT_APP_URL_PLACE_SERVICE_VALUE

########################################################
# rating Variables to Replace in parameters.properties #

envRatingAPIURL=REACT_APP_URL_RATING_VALUE

######################################################
# task Variables to Replace in parameters.properties #

envTaskAPIURL=REACT_APP_URL_TASK_SERVICE_VALUE

###################################################################
# typeTransportUnit Variables to Replace in parameters.properties #

envTypeTransportUnitAPIURL=REACT_APP_URL_TYPE_TRANSPORT_UNIT_SERVICE_VALUE

#########################################################
# company Variables to Replace in parameters.properties #

envCompanyAPIURL=REACT_APP_URL_COMPANY_VALUE

###############################################################
# companyClient Variables to Replace in parameters.properties #

envCompanyClientAPIURL=REACT_APP_URL_COMPANY_SERVICE_VALUE

##############################################################
# dispatchType Variables to Replace in parameters.properties #

envDispatchTypeAPIURL=REACT_APP_URL_DISPATCH_TYPE_SERVICE_VALUE

#######################################################################
# featuresTransportUnit Variables to Replace in parameters.properties #

envFeaturesTransportUnitAPIURL=REACT_APP_URL_FEATURE_SERVICE_VALUE

#########################################################
# modules Variables to Replace in parameters.properties #

envModulesAPIURL=REACT_APP_URL_MODULE_SERVICE_VALUE

######################################################
# news Variables to Replace in parameters.properties #

envNewsAPIURL=REACT_APP_URL_NEW_SERVICE_VALUE
envS3Identity=REACT_APP_S3_IDENTITY_BUCKET_NAME_VALUE

###########################################################
# operation Variables to Replace in parameters.properties #

envOperationAPIURL=REACT_APP_URL_OPERATION_SERVICE_VALUE

#############################################################
# postulation Variables to Replace in parameters.properties #

envPostulationAPIURL=REACT_APP_URL_POSTULATION_SERVICE_VALUE

########################################################
# report Variables to Replace in parameters.properties #

envReportAPIURL=REACT_APP_URL_REPORT_VALUE

#######################################################
# route Variables to Replace in parameters.properties #

envRouteAPIURL=REACT_APP_URL_ROUTES_SERVICE_VALUE

########################################################
# stages Variables to Replace in parameters.properties #

envStagesTaskApprove=REACT_APP_URL_TASK_APPROVE_VALUE
envStagesTaskByIdStage=REACT_APP_URL_TASK_GET_BY_ID_STAGE_VALUE
envStagesStageService=REACT_APP_URL_STAGE_SERVICE_VALUE
envStagesValidateStages=REACT_APP_URL_VALIDATE_STAGES_VALUE

################################################################
# stagesTemplate Variables to Replace in parameters.properties #

envStagesTemplateAPIURL=REACT_APP_URL_STAGES_TEMPLATE_SERVICE_VALUE

###############################################################
# transportUnit Variables to Replace in parameters.properties #

envTransportUnitAPIURL=REACT_APP_URL_TRANSPORT_UNIT_SERVICE_VALUE

##############################################################
# typeServices Variables to Replace in parameters.properties #

envTypeServicesAPIURL=REACT_APP_URL_SERVICE_SERVICE_VALUE

case $process in
    deploy)

        ##############################
        ##### Microservice users #####
        ##############################
        directory="src/handlers/users"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection emailCognito=$emailCognito ARNEmail=$arnEmail ARNSns=$arnSns keyFirstUser=$keyFirstUser \
                --stack-name users --resolve-s3 --capabilities CAPABILITY_NAMED_IAM CAPABILITY_IAM | tee deploymentLogs.log

        ApiUrl=$(grep "execute-api.us-east-1.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | head -n 1)
        sed -i "s#${envUsersAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        ApiUrlInv=$(grep "execute-api.us-east-1.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | tail -n 1)

        CognitoUserPoolId=$(grep "${awsRegion}_" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | head -n 2 | tail -n 1)
        sed -i "s#${envCongnitoUserPool}#${CognitoUserPoolId}#g" ../../../parameters.properties

        CognitoUserPoolClientId=$(AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey aws cognito-idp list-user-pool-clients --user-pool-id $CognitoUserPoolId | grep ClientId | cut -d ":" -f2- | sed 's/"//g' | sed 's/ //g' | sed 's/,//g')
        sed -i "s#${envCognitoUserClient}#${CognitoUserPoolClientId}#g" ../../../parameters.properties

        CognitoUserPoolArn=$(grep "arn:aws:cognito" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | tail -n 1)
        CognitoUserCarrierPruebaPoolArn=$(grep "arn:aws:cognito" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | head -n 1)
        CognitoUserCarrierPruebaPoolID=$(grep "${awsRegion}_" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | tail -n 1)

        urlNotification=$ApiUrl"/pushTokenNotification"
        sed -i "s#${envUsersUrlNotification}#${urlNotification}#g" ../../../parameters.properties
        
        curl --location --request POST $ApiUrlInv \
            --data-raw '{

                "type": "OPERATOR",

                "auth": {

                    "phone": "75349488",

                    "countryCode": "+591",

                    "email": "d.gamon.p@gmail.com"

                },

                "isAdmin": true,

                "profile": {

                    "firstName": "David",

                    "lastName": "Gamon",

                    "timeZone": "America/La_Paz"

                }

            }'

        cd ${firstPath}
        
        ###############################
        ### Microservice Operator #####
        ###############################
        directory="src/handlers/Operator"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn IdGrupoCognitoCarrier=$CognitoUserCarrierPruebaPoolID IdGrupoCognitoOperator=$CognitoUserPoolId \
                --stack-name Operator --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.us-east-1.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | head -n 1)
        sed -i "s#${envOperatorAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        cd ${firstPath}

        ##########dashboard##############################
        ##### Microservice operationStatus #####
        ########################################
        directory="src/handlers/operationStatus"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name operationStatus --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envOperationStatusAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ##################################
        ##### Microservice dashboard #####
        ##################################
        directory="src/handlers/dashboard"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name dashboard --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.us-east-1.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envDashboardAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice travel #####
        ###############################
        directory="src/handlers/travel"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn KeyFireBaseLink=$keyFireBaseLink DomainUriPrefix=$domainUriPrefix DynamicLink=$dynamicLink NameAppInfo=$nameAppInfo \
                --stack-name travel --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.us-east-1.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g' | head -1)
        sed -i "s#${envTravelAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        firstArnPart=$(grep "arn:aws:states" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondArnPart=$(grep ":stateMachine:" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        arnStateMachine=$firstArnPart$secondArnPart
        
        cd ${firstPath}

        #####################################
        ##### Microservice notification #####
        #####################################
        directory="src/handlers/notification"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn ARNStateMachine=$arnStateMachine \
                --stack-name notification --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envNotificationAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        cd ${firstPath}

        ###########################################
        ##### Microservice basicTransportUnit #####
        ###########################################
        directory="src/handlers/basicTransportUnit"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name basicTransportUnit --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envBasicTransUnitAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #################################
        ##### Microservice benefits #####
        #################################
        directory="src/handlers/benefits"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name benefits --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envBenefitsAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #####################################
        ##### Microservice boardingMode #####
        #####################################
        directory="src/handlers/boardingMode"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name boardingMode --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envBoardingModeAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ##############################
        ##### Microservice brand #####
        ##############################
        directory="src/handlers/brand"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name brand --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envBrandAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #################################
        ##### Microservice category #####
        #################################
        directory="src/handlers/category"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection \
                --stack-name category --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envCategoryAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ####################################
        ##### Microservice checkPoints #####
        ####################################
        directory="src/handlers/checkPoints"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name checkPoints --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envCheckPointsAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        loadingOrder=$ApiUrl"/loadingOrder"
        sed -i "s#${envCheckPointsLoadOrderService}#${loadingOrder}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice cities #####
        ###############################
        directory="src/handlers/cities"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name cities --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envCitiesAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice client #####
        ###############################
        directory="src/handlers/client"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name client --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envClientAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###########################################
        ##### Microservice loadingOrderStatus #####
        ###########################################
        directory="src/handlers/loadingOrderStatus"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name loadingOrderStatus --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envLoadingOrderAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ########################################
        ##### Microservice measurementUnit #####
        ########################################
        directory="src/handlers/measurementUnit"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name measurementUnit --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envMeasurementUnitAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ##############################
        ##### Microservice place #####
        ##############################
        directory="src/handlers/place"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name place --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envPlaceAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice rating #####
        ###############################
        directory="src/handlers/rating"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name rating --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envRatingAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ################################
        ##### Microservice setting #####
        ################################
        directory="src/handlers/setting"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name setting --resolve-s3 | tee deploymentLogs.log
        
        cd ${firstPath}

        #############################
        ##### Microservice task #####
        #############################
        directory="src/handlers/task"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name task --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envTaskAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #####################################
        ##### Microservice travelStatus #####
        #####################################
        directory="src/handlers/travelStatus"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name travelStatus --resolve-s3 | tee deploymentLogs.log
        
        cd ${firstPath}

        ##########################################
        ##### Microservice typeTransportUnit #####
        ##########################################
        directory="src/handlers/typeTransportUnit"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name typeTransportUnit --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envTypeTransportUnitAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ################################
        ##### Microservice company #####
        ################################
        directory="src/handlers/company"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name company --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envCompanyAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ######################################
        ##### Microservice companyClient #####
        ######################################
        directory="src/handlers/companyClient"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name companyClient --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envCompanyClientAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #####################################
        ##### Microservice dispatchType #####
        #####################################
        directory="src/handlers/dispatchType"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name dispatchType --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envDispatchTypeAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ##############################################
        ##### Microservice featuresTransportUnit #####
        ##############################################
        directory="src/handlers/featuresTransportUnit"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name featuresTransportUnit --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envFeaturesTransportUnitAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ################################
        ##### Microservice modules #####
        ################################
        directory="src/handlers/modules"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name modules --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envModulesAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #############################
        ##### Microservice news #####
        #############################
        directory="src/handlers/news"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn ARNStateMachine=$arnStateMachine \
                --stack-name news --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envNewsAPIURL}#${ApiUrl}#g" ../../../parameters.properties

        s3BucketName=$(grep "news-new" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envS3Identity}#${s3BucketName}#g" ../../../parameters.properties

        cd ${firstPath}

        ##################################
        ##### Microservice operation #####
        ##################################
        directory="src/handlers/operation"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name operation --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envOperationAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ####################################
        ##### Microservice postulation #####
        ####################################
        directory="src/handlers/postulation"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name postulation --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envPostulationAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice report #####
        ###############################
        directory="src/handlers/report"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name report --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envReportAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ##############################
        ##### Microservice route #####
        ##############################
        directory="src/handlers/route"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name route --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        sed -i "s#${envRouteAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###############################
        ##### Microservice stages #####
        ###############################
        directory="src/handlers/stages"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name stages --resolve-s3 | tee deploymentLogs.log
        
        ApiUrl=$(grep "execute-api.$awsRegion.amazonaws.com" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        taskApprove=$ApiUrl"/validate/stages"
        sed -i "s#${envStagesTaskApprove}#${taskApprove}#g" ../../../parameters.properties
        taskGetId=$ApiUrl"/task/stages"
        sed -i "s#${envStagesTaskByIdStage}#${taskGetId}#g" ../../../parameters.properties
        
        sed -i "s#${envStagesStageService}#${ApiUrl}#g" ../../../parameters.properties
        sed -i "s#${envStagesValidateStages}#${taskApprove}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #######################################
        ##### Microservice stagesTemplate #####
        #######################################
        directory="src/handlers/stagesTemplate"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name stagesTemplate --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envStagesTemplateAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ######################################
        ##### Microservice transportUnit #####
        ######################################
        directory="src/handlers/transportUnit"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name transportUnit --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envTransportUnitAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        #####################################
        ##### Microservice typeServices #####
        #####################################
        directory="src/handlers/typeServices"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn \
                --stack-name typeServices --resolve-s3 | tee deploymentLogs.log
        
        firstUrl=$(grep "execute-api." deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        secondUrl=$(grep "amazonaws.com/" deploymentLogs.log | sed -e 's/Value\(.*\)/\1/' | sed 's/ //g')
        ApiUrl=$firstUrl$secondUrl
        sed -i "s#${envTypeServicesAPIURL}#${ApiUrl}#g" ../../../parameters.properties
        
        cd ${firstPath}

        ###################################
        ##### Microservice vipDrivers #####
        ###################################
        directory="src/handlers/vipDrivers"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection ARNCognitoUsersOperator=$CognitoUserPoolArn ARNCognitoUsersCarrier=$CognitoUserCarrierPruebaPoolArn \
                --stack-name vipDrivers --resolve-s3 | tee deploymentLogs.log
        
        cd ${firstPath}

        #################################
        ##### Microservice versions #####
        #################################
        directory="src/handlers/versions"
        
        cd $directory && AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey sam deploy \
                --region $awsRegion --parameter-overrides dataBaseConnection=$dataBaseConnection \
                --stack-name versions --resolve-s3 | tee deploymentLogs.log
        
        cd ${firstPath}

        #############################
        ##### Amplify React APP #####
        #############################

        curl -X POST -u "$key:$secret" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials > token.json
        sed -i 's|,|\n|g' ./token.json
        tokenV=$(grep access_token ./token.json | cut -d ":" -f2- | sed 's/"//g' | sed 's/ //g')

        sed -i "s#Token_VALUE#${tokenV}#g" ./parameters.properties

        AWS_ACCESS_KEY_ID=$accessKey AWS_SECRET_ACCESS_KEY=$secretKey aws cloudformation deploy \
            --stack-name $stackNameFront --parameter-overrides $(cat parameters.properties) --region $awsRegion \
            --capabilities CAPABILITY_IAM --template-file $templateFile 2>&1 | tee deploymentLogs.log

        exit 0
        ;;
    destroy)
        printf "Do you wan to perform destroy the entire APP?\n"
        printf "Only 'yes' will be accepted to approve.\n"
        printf "\nEnter a value: "
        read input
        case $input in
            yes)
                echo > destroyLogs.log
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name $stackNameFront >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name users >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name Operator >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name operationStatus >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name dashboard >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name travel >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name notification >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name basicTransportUnit >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name benefits >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name boardingMode >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name brand >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name category >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name checkPoints >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name cities >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name client >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name loadingOrderStatus >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name measurementUnit >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name place >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name rating >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name setting >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name task >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name travelStatus >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name typeTransportUnit >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name company >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name companyClient >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name dispatchType >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name featuresTransportUnit >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name modules >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name news >> destroyLogs.log &

                

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name postulation >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name report >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name route >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name stages >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name stagesTemplate >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name transportUnit >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name typeServices >> destroyLogs.log &
                
                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name vipDrivers >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name versions >> destroyLogs.log &

                AWS_ACCESS_KEY_ID=$1 AWS_SECRET_ACCESS_KEY=$2 aws --region $awsRegion cloudformation delete-stack \
                    --stack-name operation >> destroyLogs.log &
                
                exit 0
                ;;
            *)
                printf "No actions performed\n"
                exit 0
                ;;
        esac
        ;;
    *)
        cat << EOF
Bad Arguments
To deploy the project
Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY deploy
To destroy the project
Ex: ./iacFront.sh AWS_ACCESS_KEY AWS_SECRET_KEY destroy
EOF
  exit 1
esac

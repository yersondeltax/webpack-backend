import { resolve } from 'path';
import { Configuration } from 'webpack';
import {
  entriesDashboard,
  entriesTravel,
  entriesBasicTransportUnit,
  entriesBenefits,
  entriesBrand,
  entriesCategory,
  entriesCheckPoints,
  entriesCities,
  entriesClient,
  entriesCompany,
  entriesCompanyClient,
  entriesDispatchType,
  entriesFeaturesTransportUnit,
  entriesLoadingOrderStatus,
  entriesMeasurementUnit,
  entriesModules,
  entriesNews,
  entriesNotification,
  entriesOperation,
  entriesOperationStatus,
  entriesOperator,
  entriesPlace,
  entriesPostulation,
  entriesRating,
  entriesReport,
  entriesRoute,
  entriesSetting,
  entriesStages,
  entriesStagesTemplate,
  entriesTask,
  entriesTransportUnit,
  entriesTravelStatus,
  entriesTypeServices,
  entriesTypeTransportUnit,
  entriesVersions,
  entriesVipDrivers,
  entriesUsers,
  entriesCarrier,
  entriesBoardingMode,
} from "./entries";
import {
  outputsTravel,
  outputsBasicTransportUnit,
  outputsBenefits,
  outputsBrand,
  outputsCategory,
  outputsCheckPoints,
  outputsCities,
  outputsClient,
  outputsCompany,
  outputsCompanyClient,
  outputsDashboard,
  outputsDispatchType,
  outputsFeaturesTransportUnit,
  outputsLoadingOrderStatus,
  outputsMeasurementUnit,
  outputsModules,
  outputsNews,
  outputsNotification,
  outputsOperation,
  outputsOperationStatus,
  outputsOperator,
  outputsPlace,
  outputsPostulation,
  outputsRating,
  outputsReport,
  outputsRoute,
  outputsSetting,
  outputsStages,
  outputsStagesTemplate,
  outputsTask,
  outputsTransportUnit,
  outputsTravelStatus,
  outputsTypeServices,
  outputsTypeTransportUnit,
  outputsVersions,
  outputsVipDrivers,
  outputsUsers,
  outputsCarrier,
  outputsBoardingMode,
} from "./outputs";

let entries = {}
let outputs = ""


if (process.env.NODE_BUILD_TYPE === "TRAVEL"){
  entries = entriesTravel
  outputs = outputsTravel
}

if (process.env.NODE_BUILD_TYPE === "BASICTRANSPORTUNIT"){
  entries = entriesBasicTransportUnit
  outputs = outputsBasicTransportUnit
}

if (process.env.NODE_BUILD_TYPE === "BENEFITS"){
  entries = entriesBenefits
  outputs = outputsBenefits
}

if (process.env.NODE_BUILD_TYPE === "BOARDINGMODE"){
  entries = entriesBoardingMode
  outputs = outputsBoardingMode
}

if (process.env.NODE_BUILD_TYPE === "BRAND"){
  entries = entriesBrand
  outputs = outputsBrand
}

if (process.env.NODE_BUILD_TYPE === "CATEGORY"){
  entries = entriesCategory
  outputs = outputsCategory
}

if (process.env.NODE_BUILD_TYPE === "CHECKPOINTS"){
  entries = entriesCheckPoints
  outputs = outputsCheckPoints
}

if (process.env.NODE_BUILD_TYPE === "CITIES"){
  entries = entriesCities
  outputs = outputsCities
}

if (process.env.NODE_BUILD_TYPE === "CLIENT"){
  entries = entriesClient
  outputs = outputsClient
}

if (process.env.NODE_BUILD_TYPE === "COMPANY"){
  entries = entriesCompany
  outputs = outputsCompany
}

if (process.env.NODE_BUILD_TYPE === "COMPANYCLIENT"){
  entries = entriesCompanyClient
  outputs = outputsCompanyClient
}

if (process.env.NODE_BUILD_TYPE === "DASHBOARD"){
  entries = entriesDashboard
  outputs = outputsDashboard
}

if (process.env.NODE_BUILD_TYPE === "DISPATCHTYPE"){
  entries = entriesDispatchType
  outputs = outputsDispatchType
}

if (process.env.NODE_BUILD_TYPE === "FEATURESTRANSPORTUNIT"){
  entries = entriesFeaturesTransportUnit
  outputs = outputsFeaturesTransportUnit
}

if (process.env.NODE_BUILD_TYPE === "LOADINGORDERSTATUS"){
  entries = entriesLoadingOrderStatus
  outputs = outputsLoadingOrderStatus
}

if (process.env.NODE_BUILD_TYPE === "MEASUREMENTUNIT"){
  entries = entriesMeasurementUnit
  outputs = outputsMeasurementUnit
}

if (process.env.NODE_BUILD_TYPE === "MODULES"){
  entries = entriesModules
  outputs = outputsModules
}

if (process.env.NODE_BUILD_TYPE === "NEWS"){
  entries = entriesNews
  outputs = outputsNews
}

if (process.env.NODE_BUILD_TYPE === "TRAVEL"){
  entries = entriesTravel
  outputs = outputsTravel
}

if (process.env.NODE_BUILD_TYPE === "NOTIFICATION"){
  entries = entriesNotification
  outputs = outputsNotification
}

if (process.env.NODE_BUILD_TYPE === "OPERATION"){
  entries = entriesOperation
  outputs = outputsOperation
}

if (process.env.NODE_BUILD_TYPE === "OPERATIONSTATUS"){
  entries = entriesOperationStatus
  outputs = outputsOperationStatus
}

if (process.env.NODE_BUILD_TYPE === "OPERATOR"){
  entries = entriesOperator
  outputs = outputsOperator
}

if (process.env.NODE_BUILD_TYPE === "PLACE"){
  entries = entriesPlace
  outputs = outputsPlace
}

if (process.env.NODE_BUILD_TYPE === "POSTULATION"){
  entries = entriesPostulation
  outputs = outputsPostulation
}

if (process.env.NODE_BUILD_TYPE === "RATING"){
  entries = entriesRating
  outputs = outputsRating
}

if (process.env.NODE_BUILD_TYPE === "REPORT"){
  entries = entriesReport
  outputs = outputsReport
}

if (process.env.NODE_BUILD_TYPE === "ROUTE"){
  entries = entriesRoute
  outputs = outputsRoute
}

if (process.env.NODE_BUILD_TYPE === "SETTING"){
  entries = entriesSetting
  outputs = outputsSetting
}

if (process.env.NODE_BUILD_TYPE === "STAGES"){
  entries = entriesStages
  outputs = outputsStages
}

if (process.env.NODE_BUILD_TYPE === "STAGESTEMPLATE"){
  entries = entriesStagesTemplate
  outputs = outputsStagesTemplate
}

if (process.env.NODE_BUILD_TYPE === "TASK"){
  entries = entriesTask
  outputs = outputsTask
}

if (process.env.NODE_BUILD_TYPE === "TRANSPORTUNIT"){
  entries = entriesTransportUnit
  outputs = outputsTransportUnit
}

if (process.env.NODE_BUILD_TYPE === "TRAVELSTATUS"){
  entries = entriesTravelStatus
  outputs = outputsTravelStatus
}

if (process.env.NODE_BUILD_TYPE === "TYPETRANSPORTUNIT"){
  entries = entriesTypeTransportUnit
  outputs = outputsTypeTransportUnit
}

if (process.env.NODE_BUILD_TYPE === "TYPESERVICES"){
  entries = entriesTypeServices
  outputs = outputsTypeServices
}

if (process.env.NODE_BUILD_TYPE === "VERSIONS"){
  entries = entriesVersions
  outputs = outputsVersions
}

if (process.env.NODE_BUILD_TYPE === "VIPDRIVERS"){
  entries = entriesVipDrivers
  outputs = outputsVipDrivers
}

if (process.env.NODE_BUILD_TYPE === "USERS"){
  entries = entriesUsers
  outputs = outputsUsers
}

if (process.env.NODE_BUILD_TYPE === "CARRIER"){
  entries = entriesCarrier
  outputs = outputsCarrier
}

const config: Configuration = {
   /*  entry: { getuser: './src/handlers/users/getuser.ts', 
             getall:  './src/handlers/users/getall.ts',
             register:  './src/handlers/users/register.ts'}, */

    entry: entries,

    output: {
      filename: '[name].js',
      libraryTarget: 'commonjs2',
      /* path: resolve(__dirname, 'build'), */
      path: resolve(__dirname, outputs)
      },
    // more
    module: {
        rules: [{ test: /\.ts$/, loader: 'ts-loader' }],
      },
      resolve: {
        extensions: ['.js', '.ts'],
      },
      target: 'node',
    // more
    mode: process.env.NODE_ENV === 'dev' ? 'development' : 'production',
  // more
  };

export default config;


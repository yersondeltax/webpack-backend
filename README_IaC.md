# DELTAX IaC

## Introduction

_This document provides you how to deploy the infrastructure that emcompases microservices (backend) and React App (frontend)on AWS account._

### Pre-requisites 🚀

To get started, you need to:

* [AWS CLI 2.2.2](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html)

------------
## Table of Contents

1. **Project Structure**
2. **Deploy Infrastructure**
3. **Destroy Infrastructure**

# Project Structure 📁


This project has the following files inside the root folder:

* `parameters_template.properties`: This is a template for all ENV variables used by frontend application. Its content will be automatically copied to the **parameters.properties** file. There are some variables that you need to provide its values manually.
* `parameters.properties`: In this file, all ENV variables values will be mapped automatically during the execution and creation of the resources, like API Gateway URLs. This file will be used by AWS Amplify Application.

    >**NOTE**
    >
    >DON'T edit this file. Just modify the `parameters_template.properties` to insert manual values where corresponds.

* `template.yaml`: Contains all resources needed to create the Amplify Frontend Application.

    In order to create a new _frontend environment_ you must create a new `AWS::Amplify::Branch` inside this file. Here there is an example:

    ```
    [ResourceName]:
        Type: AWS::Amplify::Branch
        Properties:
        BranchName: [branchName]
        AppId: !GetAtt AmplifyApp.AppId
        Description: [description]
        EnableAutoBuild: true
    ```

    Where,

    - __ResourceName__: is name for the branch resource. For example, if you want to create a development environment you can name it **AmplifyBranchDevelop**.
    - __branchName__: is the name of the branch where the code resides on the repository and you want to deploy it. For example: _develop_, _staging_, etc.
    - __description__ (Optional): a small description about the environment.

    Aditionally, you must to provide a `Prefix` under the `SubDomainSettings` that reference the URL to access the new environment. For example, the next section creates a subdomain for our development environmentm where you must reference the `AWS::Amplify::Branch` that we created in the previous step, note the `!GetAtt AmplifyBranchDevelop.BranchName` attribute:
    
    ```
    AmplifyDomain:
    Type: AWS::Amplify::Domain
    Properties:
      DomainName: !Ref Domain
      AppId: !GetAtt AmplifyApp.AppId
      SubDomainSettings:
        - Prefix: develop
          BranchName: !GetAtt AmplifyBranchDevelop.BranchName
    ```

* `DeltaX.sh`: this is the main script to deploy or destroy the infrastructure. Inside this file there are some values that you must provide manually when corresponds.

------------
## Deploy Infrastructure 📦

In order to deploy infrastructure, on the command line enter:

```bash
$ ./DeltaX.sh AWS_ACCESS_KEY AWS_SECRET_KEY deploy
```

Where:

**AWS_ACCESS_KEY** y **AWS_SECRET_KEY** are your AWS credentials with all required permissions in order to create all resources needed, like: AWS Lambda Functions, AWS Cognito Pools, AWS API Gateway, AWS Amplify, IAM Roles, IAM Policies, etc.

>**NOTE**
>
>After deploy the infrastructure, the script will create a `deploymentLogs.log` file inside each microservice folder that contains all information and outputs about the resources creation.
------------
# Destroy Infrastructure 💥

In order to destroy the infrastructure, on the command line enter:

```bash
$ ./DeltaX.sh AWS_ACCESS_KEY AWS_SECRET_KEY destroy
```

>**NOTE**
>
>After destroy the infrastructure, the script will create a `destroyLogs.log` file inside the root folder that contains all information about the destruction of resources.

# Change Parameters on DeltaX.sh 💥


```bash
$ awsRegion="us-east-1"
$ dataBaseConnection="mongodb+srv://userDeltaxDev:6hjunZbBbiWJpSQB@deltaxdb-develop.airpm.mongodb.net/deltadev"
$ emailCognito="info@telectronix.click"
$ arnEmail="arn:aws:ses:us-east-1:018374070137:identity/info@telectronix.click"
$ arnSns="arn:aws:sns:us-east-1:018374070137:app/GCM/snsApi"

```
# domain to send link travel ...

```bash
$ keyFireBaseLink="AIzaSyBumW34isWTAzG8iddvlMBjlm_9WzBV3mI"
$ domainUriPrefix="https://deltaxbeta.page.link"
$ dynamicLink="https://deltaxbeta.page.link"
$ nameAppInfo="com.deltaxlat.appdriver.dev"
```

# Change Parameters on parameters_template.properties 💥

```bash
$ Repository=https://bitbucket.org/deltaxnew/deltax-frontend-react
$ Domain=telectronix.click
$ Stage=prod
$ ReactAppDomain=https://staging.telectronix.click
$ ReactAppFirebaseApiKey=AIzaSyCKWxm4vHogEvoLzdAH-eQKORQ-13YNn_A
$ ReactAppFirebaseAuthDomain=deltax-staging.firebaseapp.com
$ ReactAppFirebaseProjectID=deltax-staging
$ ReactAppFirebaseStoreBucket=deltax-staging.appspot.com
$ ReactAppFirebaseMenssaging=59752073518
$ ReactAppFirebaseAppID=1:59752073518:web:67802648be406fcaf012bf
$ ReactAppFirebaseMeasurementID=G-GGLQGK6845
$ ReactAppStaticApiKey=AIzaSyAE8QygNNc0lhFs5oY0KtIJZoR17LDSJWM
$ ReactAppTokenVapidKey=BHxqot2eNy1-f-7bOUMUH0yzywxsKsYiLMQvG0HpwaytZy_otuNuNwoeCIcCtoOeroCBSF4phIq40SMlNfgZjUI
$ ReactAppEnvironmen=STAGING

```
### License

© All rights reserved of the Company **[DeltaX - 2020](https://www.deltax.la/)**
